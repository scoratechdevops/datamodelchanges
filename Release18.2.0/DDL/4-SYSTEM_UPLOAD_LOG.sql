CREATE TABLE SYSTEM_UPLOAD_LOG
(
system_upload_log_id INT(15) NOT NULL AUTO_INCREMENT,
system_upload_type_value_id INT(15) NOT NULL ,
campus_id INT(15)  ,
company_id INT(15)  ,
create_user_id INT(15) NOT NULL,
create_datetime DATETIME NOT NULL, 
csv_file_location VARCHAR(200),
err_file_location VARCHAR(200),
total_no_of_recs INT(10),
no_success_recs INT(10),
no_fail_recs INT(10),
PRIMARY KEY ( system_upload_log_id ),
FOREIGN KEY (campus_id) REFERENCES CAMPUS(campus_id),
FOREIGN KEY (company_id) REFERENCES COMPANY(company_id),
FOREIGN KEY (system_upload_type_value_id) REFERENCES LOOKUP_VALUE(lookup_value_id)
);