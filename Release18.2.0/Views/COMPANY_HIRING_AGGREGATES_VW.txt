CREATE 
    ALGORITHM = UNDEFINED 
    DEFINER = `root`@`%` 
    SQL SECURITY DEFINER
VIEW `COMPANY_HIRING_AGGREGATES_VW` AS
    SELECT 
        `COMPANY_HIRING_AGGREGATES`.`company_id` AS `company_id`,
        `COMPANY_HIRING_AGGREGATES`.`calendar_year` AS `calendar_year`,
        (SUM(`COMPANY_HIRING_AGGREGATES`.`sum_of_offers`) / SUM(`COMPANY_HIRING_AGGREGATES`.`no_of_offers`)) AS `avg_salary`,
        SUM(`COMPANY_HIRING_AGGREGATES`.`no_of_offers`) AS `total_no_of_offers`
    FROM
        `COMPANY_HIRING_AGGREGATES`
    GROUP BY `COMPANY_HIRING_AGGREGATES`.`company_id` , `COMPANY_HIRING_AGGREGATES`.`calendar_year`