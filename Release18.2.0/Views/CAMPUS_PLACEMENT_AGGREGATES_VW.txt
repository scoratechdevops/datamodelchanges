CREATE 
    ALGORITHM = UNDEFINED 
    DEFINER = `root`@`%` 
    SQL SECURITY DEFINER
VIEW `CAMPUS_PLACEMENT_AGGREGATES_VW` AS
    (SELECT 
        `CAMPUS_PLACEMENT_AGGREGATES`.`university_id` AS `university_id`,
        `CAMPUS_PLACEMENT_AGGREGATES`.`campus_id` AS `campus_id`,
        `CAMPUS_PLACEMENT_AGGREGATES`.`academic_year` AS `academic_year`,
        SUM(`CAMPUS_PLACEMENT_AGGREGATES`.`no_of_offers`) AS `no_of_offers`,
        SUM(`CAMPUS_PLACEMENT_AGGREGATES`.`total_offers`) AS `total_offers`,
        (SUM(`CAMPUS_PLACEMENT_AGGREGATES`.`total_offers`) / SUM(`CAMPUS_PLACEMENT_AGGREGATES`.`no_of_offers`)) AS `average_salary`
    FROM
        `CAMPUS_PLACEMENT_AGGREGATES`
    GROUP BY `CAMPUS_PLACEMENT_AGGREGATES`.`university_id` , `CAMPUS_PLACEMENT_AGGREGATES`.`campus_id` , `CAMPUS_PLACEMENT_AGGREGATES`.`academic_year`)