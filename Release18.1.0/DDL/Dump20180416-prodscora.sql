-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: localhost    Database: prodscora
-- ------------------------------------------------------
-- Server version	5.6.39-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `ADDRESS`
--

DROP TABLE IF EXISTS `ADDRESS`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ADDRESS` (
  `address_id` int(15) NOT NULL AUTO_INCREMENT,
  `address_type_value_id` int(15) NOT NULL,
  `address_line_1` varchar(100) DEFAULT NULL,
  `address_line_2` varchar(100) DEFAULT NULL,
  `address_line_3` varchar(100) DEFAULT NULL,
  `postal_id` int(15) DEFAULT NULL,
  `city_id` int(15) NOT NULL,
  `state_code` varchar(16) DEFAULT NULL,
  `country_code` varchar(16) NOT NULL,
  `create_datetime` datetime NOT NULL,
  `update_datetime` datetime NOT NULL,
  `create_user_id` int(15) NOT NULL,
  `update_user_id` int(15) NOT NULL,
  PRIMARY KEY (`address_id`),
  KEY `state_code` (`state_code`),
  KEY `city_id` (`city_id`),
  KEY `country_code` (`country_code`),
  KEY `address_type_value_id` (`address_type_value_id`),
  CONSTRAINT `ADDRESS_ibfk_2` FOREIGN KEY (`state_code`) REFERENCES `STATE` (`state_code`),
  CONSTRAINT `ADDRESS_ibfk_3` FOREIGN KEY (`city_id`) REFERENCES `CITY` (`city_id`),
  CONSTRAINT `ADDRESS_ibfk_4` FOREIGN KEY (`country_code`) REFERENCES `STATE` (`country_code`),
  CONSTRAINT `ADDRESS_ibfk_5` FOREIGN KEY (`address_type_value_id`) REFERENCES `LOOKUP_VALUE` (`lookup_value_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ADDRESS`
--

LOCK TABLES `ADDRESS` WRITE;
/*!40000 ALTER TABLE `ADDRESS` DISABLE KEYS */;
INSERT INTO `ADDRESS` VALUES (1,20,'S4, 2nd Floor Destination Center','Magarpatta City','Hadapsar',411008,164,'MH','IN','2018-04-04 10:35:14','2018-04-04 10:35:14',13163,13163),(2,20,'Plot No:14 Aeren Building','Rajeev Gandhi Technology Park',NULL,160101,40,'CH','IN','2018-04-04 10:37:03','2018-04-04 10:37:03',13163,13163),(3,20,'Level 9','SLN Terminus','Gachibowli',500032,626,'TS','IN','2018-04-04 10:47:51','2018-04-04 10:47:51',13162,13162),(4,20,'9th Floor, SLN Terminious','Gachibowli',NULL,500032,626,'TS','IN','2018-04-04 11:26:16','2018-04-04 11:26:16',13163,13163);
/*!40000 ALTER TABLE `ADDRESS` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ASSESSMENT`
--

DROP TABLE IF EXISTS `ASSESSMENT`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ASSESSMENT` (
  `assessment_id` int(15) NOT NULL AUTO_INCREMENT,
  `enrollment_id` int(15) NOT NULL,
  `program_unit_type_value_id` int(15) DEFAULT NULL,
  `subject_details` varchar(2000) DEFAULT NULL,
  `score` int(3) DEFAULT NULL,
  `highlights` varchar(2000) DEFAULT NULL,
  `data_verified_ind` varchar(1) DEFAULT NULL,
  `create_datetime` datetime NOT NULL,
  `update_datetime` datetime NOT NULL,
  `create_user_id` int(15) NOT NULL,
  `update_user_id` int(15) NOT NULL,
  `campus_id` int(15) NOT NULL,
  PRIMARY KEY (`assessment_id`),
  KEY `enrollment_id` (`enrollment_id`),
  KEY `program_unit_type_value_id` (`program_unit_type_value_id`),
  KEY `campus_id` (`campus_id`),
  CONSTRAINT `ASSESSMENT_ibfk_2` FOREIGN KEY (`enrollment_id`) REFERENCES `ENROLLMENT` (`enrollment_id`),
  CONSTRAINT `ASSESSMENT_ibfk_3` FOREIGN KEY (`program_unit_type_value_id`) REFERENCES `LOOKUP_VALUE` (`lookup_value_id`),
  CONSTRAINT `ASSESSMENT_ibfk_4` FOREIGN KEY (`campus_id`) REFERENCES `CAMPUS` (`campus_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ASSESSMENT`
--

LOCK TABLES `ASSESSMENT` WRITE;
/*!40000 ALTER TABLE `ASSESSMENT` DISABLE KEYS */;
/*!40000 ALTER TABLE `ASSESSMENT` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `CAMPUS`
--

DROP TABLE IF EXISTS `CAMPUS`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `CAMPUS` (
  `campus_id` int(15) NOT NULL AUTO_INCREMENT,
  `university_id` int(15) NOT NULL,
  `short_name` varchar(50) DEFAULT NULL,
  `name` varchar(100) DEFAULT NULL,
  `description` varchar(2000) DEFAULT NULL,
  `rating` int(2) DEFAULT NULL,
  `rank` int(5) DEFAULT NULL,
  `number_of_students` int(15) DEFAULT NULL,
  `logo` varchar(200) DEFAULT NULL,
  `branding_image` varchar(200) DEFAULT NULL,
  `mission_statement` varchar(2000) DEFAULT NULL,
  `create_datetime` datetime NOT NULL,
  `update_datetime` datetime NOT NULL,
  `create_user_id` int(15) NOT NULL,
  `update_user_id` int(15) NOT NULL,
  `established_date` date DEFAULT NULL,
  `website` varchar(200) DEFAULT NULL,
  `facebook` varchar(200) DEFAULT NULL,
  `twitter` varchar(200) DEFAULT NULL,
  `youtube` varchar(200) DEFAULT NULL,
  `linkedin` varchar(200) DEFAULT NULL,
  `campus_status_value_id` int(15) DEFAULT NULL,
  `search_name` varchar(100) DEFAULT NULL,
  `search_short_name` varchar(50) DEFAULT NULL,
  `tier_value_id` int(15) DEFAULT NULL,
  PRIMARY KEY (`campus_id`),
  KEY `university_id` (`university_id`),
  KEY `IDX_CAM_SN` (`search_name`),
  KEY `IDX_CAM_SSN` (`search_short_name`),
  CONSTRAINT `CAMPUS_ibfk_1` FOREIGN KEY (`university_id`) REFERENCES `UNIVERSITY` (`university_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `CAMPUS`
--

LOCK TABLES `CAMPUS` WRITE;
/*!40000 ALTER TABLE `CAMPUS` DISABLE KEYS */;
INSERT INTO `CAMPUS` VALUES (1,68,NULL,'Bharati Vidyapeeth College of Engineering, Pune',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2018-04-05 13:07:18','2018-04-05 13:07:18',1,1,NULL,NULL,NULL,NULL,NULL,NULL,367,'BHARATI VIDYAPEETH COLLEGE OF ENGINEERING, PUNE',NULL,NULL);
/*!40000 ALTER TABLE `CAMPUS` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `CAMPUS_ADDRESS`
--

DROP TABLE IF EXISTS `CAMPUS_ADDRESS`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `CAMPUS_ADDRESS` (
  `campus_id` int(15) NOT NULL,
  `address_id` int(15) NOT NULL,
  `primary_ind` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`campus_id`,`address_id`),
  KEY `address_id` (`address_id`),
  CONSTRAINT `CAMPUS_ADDRESS_ibfk_1` FOREIGN KEY (`campus_id`) REFERENCES `CAMPUS` (`campus_id`),
  CONSTRAINT `CAMPUS_ADDRESS_ibfk_2` FOREIGN KEY (`address_id`) REFERENCES `ADDRESS` (`address_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `CAMPUS_ADDRESS`
--

LOCK TABLES `CAMPUS_ADDRESS` WRITE;
/*!40000 ALTER TABLE `CAMPUS_ADDRESS` DISABLE KEYS */;
/*!40000 ALTER TABLE `CAMPUS_ADDRESS` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `CAMPUS_CONTACT`
--

DROP TABLE IF EXISTS `CAMPUS_CONTACT`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `CAMPUS_CONTACT` (
  `campus_id` int(15) NOT NULL,
  `contact_id` int(15) NOT NULL,
  `primary_ind` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`campus_id`,`contact_id`),
  KEY `contact_id` (`contact_id`),
  CONSTRAINT `CAMPUS_CONTACT_ibfk_1` FOREIGN KEY (`campus_id`) REFERENCES `CAMPUS` (`campus_id`),
  CONSTRAINT `CAMPUS_CONTACT_ibfk_2` FOREIGN KEY (`contact_id`) REFERENCES `CONTACT` (`contact_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `CAMPUS_CONTACT`
--

LOCK TABLES `CAMPUS_CONTACT` WRITE;
/*!40000 ALTER TABLE `CAMPUS_CONTACT` DISABLE KEYS */;
/*!40000 ALTER TABLE `CAMPUS_CONTACT` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `CAMPUS_DRIVE`
--

DROP TABLE IF EXISTS `CAMPUS_DRIVE`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `CAMPUS_DRIVE` (
  `campus_drive_id` int(15) NOT NULL AUTO_INCREMENT,
  `campus_id` int(15) NOT NULL,
  `drive_name` varchar(50) NOT NULL,
  `description` varchar(100) DEFAULT NULL,
  `drive_type_value_id` int(15) NOT NULL,
  `start_date` date NOT NULL,
  `end_date` date DEFAULT NULL,
  `drive_status_value_id` int(15) NOT NULL,
  `create_datetime` datetime NOT NULL,
  `create_user_id` int(15) NOT NULL,
  `update_datetime` datetime NOT NULL,
  `update_user_id` int(15) NOT NULL,
  `department_id` int(15) DEFAULT NULL,
  PRIMARY KEY (`campus_drive_id`),
  KEY `IDX_FK_CAMP_DRV` (`campus_id`),
  KEY `drive_type_value_id` (`drive_type_value_id`),
  KEY `drive_status_value_id` (`drive_status_value_id`),
  KEY `CAMPUS_DRIVE_ibfk_4` (`department_id`),
  CONSTRAINT `CAMPUS_DRIVE_ibfk_1` FOREIGN KEY (`campus_id`) REFERENCES `CAMPUS` (`campus_id`),
  CONSTRAINT `CAMPUS_DRIVE_ibfk_2` FOREIGN KEY (`drive_type_value_id`) REFERENCES `LOOKUP_VALUE` (`lookup_value_id`),
  CONSTRAINT `CAMPUS_DRIVE_ibfk_3` FOREIGN KEY (`drive_status_value_id`) REFERENCES `LOOKUP_VALUE` (`lookup_value_id`),
  CONSTRAINT `CAMPUS_DRIVE_ibfk_4` FOREIGN KEY (`department_id`) REFERENCES `DEPARTMENT` (`department_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `CAMPUS_DRIVE`
--

LOCK TABLES `CAMPUS_DRIVE` WRITE;
/*!40000 ALTER TABLE `CAMPUS_DRIVE` DISABLE KEYS */;
/*!40000 ALTER TABLE `CAMPUS_DRIVE` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `CAMPUS_EVENT`
--

DROP TABLE IF EXISTS `CAMPUS_EVENT`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `CAMPUS_EVENT` (
  `campus_event_id` int(15) NOT NULL AUTO_INCREMENT,
  `campus_drive_id` int(15) NOT NULL,
  `campus_id` int(15) NOT NULL,
  `employer_event_id` int(15) DEFAULT NULL,
  `company_id` int(15) DEFAULT NULL,
  `event_name` varchar(50) NOT NULL,
  `event_type_value_id` int(15) NOT NULL,
  `event_status_value_id` int(15) NOT NULL,
  `scheduled_date` date DEFAULT NULL,
  `scheduled_start_time` time DEFAULT NULL,
  `duration` int(15) DEFAULT NULL,
  `create_datetime` datetime NOT NULL,
  `create_user_id` int(15) NOT NULL,
  `update_datetime` datetime NOT NULL,
  `update_user_id` int(15) NOT NULL,
  `event_requirement` varchar(2000) DEFAULT NULL,
  PRIMARY KEY (`campus_event_id`),
  KEY `employer_event_id` (`employer_event_id`),
  KEY `campus_id` (`campus_id`),
  KEY `IDX_FK_CAMP_EVNT_CAM` (`campus_drive_id`),
  KEY `IDX_FK_CAMP_EVNT_COM` (`company_id`),
  KEY `event_type_value_id` (`event_type_value_id`),
  KEY `event_status_value_id` (`event_status_value_id`),
  CONSTRAINT `CAMPUS_EVENT_ibfk_1` FOREIGN KEY (`campus_drive_id`) REFERENCES `CAMPUS_DRIVE` (`campus_drive_id`),
  CONSTRAINT `CAMPUS_EVENT_ibfk_2` FOREIGN KEY (`employer_event_id`) REFERENCES `EMPLOYER_EVENT` (`emp_event_id`),
  CONSTRAINT `CAMPUS_EVENT_ibfk_3` FOREIGN KEY (`campus_id`) REFERENCES `CAMPUS` (`campus_id`),
  CONSTRAINT `CAMPUS_EVENT_ibfk_4` FOREIGN KEY (`company_id`) REFERENCES `COMPANY` (`company_id`),
  CONSTRAINT `CAMPUS_EVENT_ibfk_5` FOREIGN KEY (`event_type_value_id`) REFERENCES `LOOKUP_VALUE` (`lookup_value_id`),
  CONSTRAINT `CAMPUS_EVENT_ibfk_6` FOREIGN KEY (`event_status_value_id`) REFERENCES `LOOKUP_VALUE` (`lookup_value_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `CAMPUS_EVENT`
--

LOCK TABLES `CAMPUS_EVENT` WRITE;
/*!40000 ALTER TABLE `CAMPUS_EVENT` DISABLE KEYS */;
/*!40000 ALTER TABLE `CAMPUS_EVENT` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Temporary view structure for view `CAMPUS_EVENT_STUDENT_SEARCH_VW`
--

DROP TABLE IF EXISTS `CAMPUS_EVENT_STUDENT_SEARCH_VW`;
/*!50001 DROP VIEW IF EXISTS `CAMPUS_EVENT_STUDENT_SEARCH_VW`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `CAMPUS_EVENT_STUDENT_SEARCH_VW` AS SELECT 
 1 AS `first_name`,
 1 AS `last_name`,
 1 AS `name`,
 1 AS `campus_id`,
 1 AS `campus_name`,
 1 AS `program_name`,
 1 AS `highlights`,
 1 AS `student_id`,
 1 AS `off_campus_ind`,
 1 AS `profile_activation_ind`,
 1 AS `student_status_value_id`,
 1 AS `department_id`,
 1 AS `program_id`,
 1 AS `program_type_value_id`,
 1 AS `program_class_value_id`,
 1 AS `program_cat_value_id`,
 1 AS `program_major_value_id`,
 1 AS `cgpa_score`,
 1 AS `planed_completion_date`*/;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `CAMPUS_OFFER_AGGREGATES`
--

DROP TABLE IF EXISTS `CAMPUS_OFFER_AGGREGATES`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `CAMPUS_OFFER_AGGREGATES` (
  `campus_offer_aggregate_id` int(15) NOT NULL AUTO_INCREMENT,
  `university_id` int(15) DEFAULT NULL,
  `campus_id` int(15) DEFAULT NULL,
  `department_id` int(15) DEFAULT NULL,
  `program_id` int(15) DEFAULT NULL,
  `no_of_students` int(5) DEFAULT NULL,
  `academic_year` int(4) DEFAULT NULL,
  `top_5` decimal(18,6) DEFAULT NULL,
  `6_to_10` decimal(18,6) DEFAULT NULL,
  `11_to_20` decimal(18,6) DEFAULT NULL,
  `21_to_50` decimal(18,6) DEFAULT NULL,
  `above_50` decimal(18,6) DEFAULT NULL,
  `no_offers` int(5) DEFAULT NULL,
  PRIMARY KEY (`campus_offer_aggregate_id`),
  KEY `university_id` (`university_id`),
  KEY `department_id` (`department_id`),
  KEY `program_id` (`program_id`),
  KEY `IDX_CAMPUS_OFFER_AGGREGATES_1` (`campus_id`,`academic_year`),
  CONSTRAINT `CAMPUS_OFFER_AGGREGATES_ibfk_1` FOREIGN KEY (`campus_id`) REFERENCES `CAMPUS` (`campus_id`),
  CONSTRAINT `CAMPUS_OFFER_AGGREGATES_ibfk_2` FOREIGN KEY (`university_id`) REFERENCES `UNIVERSITY` (`university_id`),
  CONSTRAINT `CAMPUS_OFFER_AGGREGATES_ibfk_3` FOREIGN KEY (`department_id`) REFERENCES `DEPARTMENT` (`department_id`),
  CONSTRAINT `CAMPUS_OFFER_AGGREGATES_ibfk_4` FOREIGN KEY (`program_id`) REFERENCES `PROGRAM` (`program_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `CAMPUS_OFFER_AGGREGATES`
--

LOCK TABLES `CAMPUS_OFFER_AGGREGATES` WRITE;
/*!40000 ALTER TABLE `CAMPUS_OFFER_AGGREGATES` DISABLE KEYS */;
/*!40000 ALTER TABLE `CAMPUS_OFFER_AGGREGATES` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `CAMPUS_PLACEMENT_AGGREGATES`
--

DROP TABLE IF EXISTS `CAMPUS_PLACEMENT_AGGREGATES`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `CAMPUS_PLACEMENT_AGGREGATES` (
  `campus_aggregate_id` int(15) NOT NULL AUTO_INCREMENT,
  `university_id` int(15) DEFAULT NULL,
  `campus_id` int(15) DEFAULT NULL,
  `department_id` int(15) DEFAULT NULL,
  `program_id` int(15) DEFAULT NULL,
  `no_of_offers` int(5) DEFAULT NULL,
  `total_offers` decimal(18,6) DEFAULT NULL,
  `minimum_offer` decimal(18,6) DEFAULT NULL,
  `maximum_offer` decimal(18,6) DEFAULT NULL,
  `academic_year` int(4) DEFAULT NULL,
  `company_id` int(15) DEFAULT NULL,
  PRIMARY KEY (`campus_aggregate_id`),
  KEY `campus_id` (`campus_id`),
  KEY `university_id` (`university_id`),
  KEY `department_id` (`department_id`),
  KEY `program_id` (`program_id`),
  KEY `company_id` (`company_id`),
  KEY `IDX_CAMPUS_PLACEMENT_AGGREGATES_1` (`campus_id`,`academic_year`,`department_id`),
  CONSTRAINT `CAMPUS_PLACEMENT_AGGREGATES_ibfk_1` FOREIGN KEY (`campus_id`) REFERENCES `CAMPUS` (`campus_id`),
  CONSTRAINT `CAMPUS_PLACEMENT_AGGREGATES_ibfk_2` FOREIGN KEY (`university_id`) REFERENCES `UNIVERSITY` (`university_id`),
  CONSTRAINT `CAMPUS_PLACEMENT_AGGREGATES_ibfk_3` FOREIGN KEY (`department_id`) REFERENCES `DEPARTMENT` (`department_id`),
  CONSTRAINT `CAMPUS_PLACEMENT_AGGREGATES_ibfk_4` FOREIGN KEY (`program_id`) REFERENCES `PROGRAM` (`program_id`),
  CONSTRAINT `CAMPUS_PLACEMENT_AGGREGATES_ibfk_5` FOREIGN KEY (`company_id`) REFERENCES `COMPANY` (`company_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `CAMPUS_PLACEMENT_AGGREGATES`
--

LOCK TABLES `CAMPUS_PLACEMENT_AGGREGATES` WRITE;
/*!40000 ALTER TABLE `CAMPUS_PLACEMENT_AGGREGATES` DISABLE KEYS */;
/*!40000 ALTER TABLE `CAMPUS_PLACEMENT_AGGREGATES` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Temporary view structure for view `CAMPUS_PLACEMENT_AGGREGATES_VW`
--

DROP TABLE IF EXISTS `CAMPUS_PLACEMENT_AGGREGATES_VW`;
/*!50001 DROP VIEW IF EXISTS `CAMPUS_PLACEMENT_AGGREGATES_VW`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `CAMPUS_PLACEMENT_AGGREGATES_VW` AS SELECT 
 1 AS `university_id`,
 1 AS `campus_id`,
 1 AS `academic_year`,
 1 AS `no_of_offers`,
 1 AS `total_offers`,
 1 AS `average_salary`*/;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `CAMPUS_PLACEMENT_AGGREGATES_WORK`
--

DROP TABLE IF EXISTS `CAMPUS_PLACEMENT_AGGREGATES_WORK`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `CAMPUS_PLACEMENT_AGGREGATES_WORK` (
  `row_number` int(5) DEFAULT NULL,
  `department_name` varchar(100) DEFAULT NULL,
  `program_name` varchar(50) DEFAULT NULL,
  `no_of_offers` int(5) DEFAULT NULL,
  `academic_year` int(4) DEFAULT NULL,
  `no_of_students` int(5) DEFAULT NULL,
  `total_offers` decimal(18,6) DEFAULT NULL,
  `minimum_offer` decimal(18,6) DEFAULT NULL,
  `maximum_offer` decimal(18,6) DEFAULT NULL,
  `top_5` decimal(18,6) DEFAULT NULL,
  `6_to_10` decimal(18,6) DEFAULT NULL,
  `11_to_20` decimal(18,6) DEFAULT NULL,
  `21_to_50` decimal(18,6) DEFAULT NULL,
  `above_50` decimal(18,6) DEFAULT NULL,
  `error` varchar(500) DEFAULT NULL,
  `department_id` int(15) DEFAULT NULL,
  `program_major_value_id` int(15) DEFAULT NULL,
  `campus_upload_log_id` int(15) DEFAULT NULL,
  KEY `campus_upload_log_id` (`campus_upload_log_id`),
  CONSTRAINT `CAMPUS_PLACEMENT_AGGREGATES_WORK_ibfk_1` FOREIGN KEY (`campus_upload_log_id`) REFERENCES `CAMPUS_UPLOAD_LOG` (`campus_upload_log_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `CAMPUS_PLACEMENT_AGGREGATES_WORK`
--

LOCK TABLES `CAMPUS_PLACEMENT_AGGREGATES_WORK` WRITE;
/*!40000 ALTER TABLE `CAMPUS_PLACEMENT_AGGREGATES_WORK` DISABLE KEYS */;
/*!40000 ALTER TABLE `CAMPUS_PLACEMENT_AGGREGATES_WORK` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Temporary view structure for view `CAMPUS_SEARCH_VW`
--

DROP TABLE IF EXISTS `CAMPUS_SEARCH_VW`;
/*!50001 DROP VIEW IF EXISTS `CAMPUS_SEARCH_VW`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `CAMPUS_SEARCH_VW` AS SELECT 
 1 AS `University_Name`,
 1 AS `Campus_Name`,
 1 AS `city_id`,
 1 AS `state_code`,
 1 AS `university_id`,
 1 AS `campus_id`,
 1 AS `city_name`,
 1 AS `rank`,
 1 AS `established_date`,
 1 AS `region_flag`,
 1 AS `tier_value_id`,
 1 AS `tier`,
 1 AS `search_name`,
 1 AS `search_short_name`,
 1 AS `campus_status_value_id`,
 1 AS `campus_status`,
 1 AS `average_salary`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `CAMPUS_SRCH_PROG_SKILL_INT_VW`
--

DROP TABLE IF EXISTS `CAMPUS_SRCH_PROG_SKILL_INT_VW`;
/*!50001 DROP VIEW IF EXISTS `CAMPUS_SRCH_PROG_SKILL_INT_VW`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `CAMPUS_SRCH_PROG_SKILL_INT_VW` AS SELECT 
 1 AS `campus_id`,
 1 AS `program_id`,
 1 AS `program_name`,
 1 AS `program_type_value_id`,
 1 AS `program_class_value_id`,
 1 AS `program_cat_value_id`,
 1 AS `program_major_value_id`,
 1 AS `student_id`,
 1 AS `skill_type_value_id`,
 1 AS `interest_type_value_id`*/;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `CAMPUS_UPLOAD_LOG`
--

DROP TABLE IF EXISTS `CAMPUS_UPLOAD_LOG`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `CAMPUS_UPLOAD_LOG` (
  `campus_upload_log_id` int(15) NOT NULL AUTO_INCREMENT,
  `campus_upload_type_value_id` int(15) NOT NULL,
  `campus_id` int(15) NOT NULL,
  `input_parameters` varchar(2000) DEFAULT NULL,
  `create_user_id` int(15) DEFAULT NULL,
  `create_datetime` datetime DEFAULT NULL,
  `csv_file_location` varchar(200) DEFAULT NULL,
  `error_file_location` varchar(200) DEFAULT NULL,
  `total_no_recs` int(10) DEFAULT NULL,
  `no_success_recs` int(10) DEFAULT NULL,
  `no_fail_recs` int(10) DEFAULT NULL,
  `program_id` int(15) DEFAULT NULL,
  PRIMARY KEY (`campus_upload_log_id`),
  KEY `campus_id` (`campus_id`),
  KEY `campus_upload_type_value_id` (`campus_upload_type_value_id`),
  KEY `CAMPUS_UPLOAD_LOG_ibfk_3_idx` (`program_id`),
  CONSTRAINT `CAMPUS_UPLOAD_LOG_ibfk_1` FOREIGN KEY (`campus_id`) REFERENCES `CAMPUS` (`campus_id`),
  CONSTRAINT `CAMPUS_UPLOAD_LOG_ibfk_2` FOREIGN KEY (`campus_upload_type_value_id`) REFERENCES `LOOKUP_VALUE` (`lookup_value_id`),
  CONSTRAINT `CAMPUS_UPLOAD_LOG_ibfk_3` FOREIGN KEY (`program_id`) REFERENCES `PROGRAM` (`program_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `CAMPUS_UPLOAD_LOG`
--

LOCK TABLES `CAMPUS_UPLOAD_LOG` WRITE;
/*!40000 ALTER TABLE `CAMPUS_UPLOAD_LOG` DISABLE KEYS */;
/*!40000 ALTER TABLE `CAMPUS_UPLOAD_LOG` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `CITY`
--

DROP TABLE IF EXISTS `CITY`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `CITY` (
  `city_id` int(15) NOT NULL AUTO_INCREMENT,
  `country_code` varchar(16) NOT NULL,
  `state_code` varchar(16) DEFAULT NULL,
  `city_name` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`city_id`),
  KEY `IDX_CITY_01` (`country_code`,`state_code`),
  KEY `state_code` (`state_code`),
  CONSTRAINT `CITY_ibfk_1` FOREIGN KEY (`state_code`) REFERENCES `STATE` (`state_code`),
  CONSTRAINT `CITY_ibfk_2` FOREIGN KEY (`country_code`) REFERENCES `STATE` (`country_code`)
) ENGINE=InnoDB AUTO_INCREMENT=632 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `CITY`
--

LOCK TABLES `CITY` WRITE;
/*!40000 ALTER TABLE `CITY` DISABLE KEYS */;
INSERT INTO `CITY` VALUES (1,'IN','AN','Nicobar'),(2,'IN','AN','North And Middle Andaman'),(3,'IN','AN','South Andaman'),(4,'IN','AP','Srikakulam'),(5,'IN','AP','Vizianagaram'),(6,'IN','AP','Visakhapatnam'),(7,'IN','AP','East Godavari'),(8,'IN','AP','West Godavari'),(9,'IN','AP','Krishna'),(10,'IN','AP','Guntur'),(11,'IN','AP','Prakasam'),(12,'IN','AP','Nellore'),(13,'IN','AP','Cuddapah'),(14,'IN','AP','Kurnool'),(15,'IN','AP','Ananthapur'),(16,'IN','AP','Chittoor'),(17,'IN','AS','Kokrajhar'),(18,'IN','AS','Dhubri'),(19,'IN','AS','Goalpara'),(20,'IN','AS','Barpeta'),(21,'IN','AS','Marigaon'),(22,'IN','AS','Nagaon'),(23,'IN','AS','Sonitpur'),(24,'IN','AS','Lakhimpur'),(25,'IN','AS','Dhemaji'),(26,'IN','AS','Tinsukia'),(27,'IN','AS','Dibrugarh'),(28,'IN','AS','Sibsagar'),(29,'IN','AS','Jorhat'),(30,'IN','AS','Golaghat'),(31,'IN','AS','Karbi Anglong'),(32,'IN','AS','North Cachar Hills'),(33,'IN','AS','Cachar'),(34,'IN','AS','Karimganj'),(35,'IN','AS','Hailakandi'),(36,'IN','AS','Bongaigaon'),(37,'IN','AS','Kamrup'),(38,'IN','AS','Nalbari'),(39,'IN','AS','Darrang'),(40,'IN','CH','Chandigarh'),(41,'IN','DN','Dadra & Nagar Haveli'),(42,'IN','DL','North West Delhi'),(43,'IN','DL','North Delhi'),(44,'IN','DL','North East Delhi'),(45,'IN','DL','East Delhi'),(46,'IN','DL','New Delhi'),(47,'IN','DL','Central Delhi'),(48,'IN','DL','West Delhi'),(49,'IN','DL','South West Delhi'),(50,'IN','DL','South Delhi'),(51,'IN','GJ','Kachchh'),(52,'IN','GJ','Banaskantha'),(53,'IN','GJ','Patan'),(54,'IN','GJ','Mahesana'),(55,'IN','GJ','Sabarkantha'),(56,'IN','GJ','Gandhi Nagar'),(57,'IN','GJ','Ahmedabad'),(58,'IN','GJ','Surendra Nagar'),(59,'IN','GJ','Rajkot'),(60,'IN','GJ','Jamnagar'),(61,'IN','GJ','Porbandar'),(62,'IN','GJ','Junagadh'),(63,'IN','GJ','Amreli'),(64,'IN','GJ','Bhavnagar'),(65,'IN','GJ','Anand'),(66,'IN','GJ','Kheda'),(67,'IN','GJ','Panch Mahals'),(68,'IN','GJ','Dahod'),(69,'IN','GJ','Vadodara'),(70,'IN','GJ','Narmada'),(71,'IN','GJ','Bharuch'),(72,'IN','GJ','The Dangs'),(73,'IN','GJ','Navsari'),(74,'IN','GJ','Valsad'),(75,'IN','GJ','Surat'),(76,'IN','GJ','Tapi'),(77,'IN','HR','Panchkula'),(78,'IN','HR','Ambala'),(79,'IN','HR','Yamuna Nagar'),(80,'IN','HR','Kurukshetra'),(81,'IN','HR','Kaithal'),(82,'IN','HR','Karnal'),(83,'IN','HR','Panipat'),(84,'IN','HR','Sonipat'),(85,'IN','HR','Jind'),(86,'IN','HR','Fatehabad'),(87,'IN','HR','Sirsa'),(88,'IN','HR','Hisar'),(89,'IN','HR','Bhiwani'),(90,'IN','HR','Rohtak'),(91,'IN','HR','Jhajjar'),(92,'IN','HR','Mahendragarh'),(93,'IN','HR','Rewari'),(94,'IN','HR','Gurgaon'),(95,'IN','HR','Faridabad'),(96,'IN','HP','Chamba'),(97,'IN','HP','Kangra'),(98,'IN','HP','Lahul & Spiti'),(99,'IN','HP','Kullu'),(100,'IN','HP','Mandi'),(101,'IN','HP','Hamirpur(HP)'),(102,'IN','HP','Una'),(103,'IN','HP','Bilaspur (HP)'),(104,'IN','HP','Solan'),(105,'IN','HP','Sirmaur'),(106,'IN','HP','Shimla'),(107,'IN','HP','Kinnaur'),(108,'IN','JK','Kupwara'),(109,'IN','JK','Budgam'),(110,'IN','JK','Leh'),(111,'IN','JK','Kargil'),(112,'IN','JK','Poonch'),(113,'IN','JK','Rajauri'),(114,'IN','JK','Kathua'),(115,'IN','JK','Baramulla'),(116,'IN','JK','Bandipur'),(117,'IN','JK','Srinagar'),(118,'IN','JK','Pulwama'),(119,'IN','JK','Shopian'),(120,'IN','JK','Ananthnag'),(121,'IN','JK','Kulgam'),(122,'IN','JK','Doda'),(123,'IN','JK','Udhampur'),(124,'IN','JK','Reasi'),(125,'IN','JK','Jammu'),(126,'IN','KL','Kasargod'),(127,'IN','KL','Kannur'),(128,'IN','KL','Wayanad'),(129,'IN','KL','Kozhikode'),(130,'IN','KL','Malappuram'),(131,'IN','KL','Palakkad'),(132,'IN','KL','Thrissur'),(133,'IN','KL','Ernakulam'),(134,'IN','KL','Idukki'),(135,'IN','KL','Kottayam'),(136,'IN','KL','Alappuzha'),(137,'IN','KL','Pathanamthitta'),(138,'IN','KL','Kollam'),(139,'IN','KL','Thiruvananthapuram'),(140,'IN','LD','Lakshadweep'),(141,'IN','MH','Nandurbar'),(142,'IN','MH','Dhule'),(143,'IN','MH','Jalgaon'),(144,'IN','MH','Buldhana'),(145,'IN','MH','Akola'),(146,'IN','MH','Washim'),(147,'IN','MH','Amravati'),(148,'IN','MH','Wardha'),(149,'IN','MH','Nagpur'),(150,'IN','MH','Bhandara'),(151,'IN','MH','Gondia'),(152,'IN','MH','Gadchiroli'),(153,'IN','MH','Chandrapur'),(154,'IN','MH','Yavatmal'),(155,'IN','MH','Nanded'),(156,'IN','MH','Hingoli'),(157,'IN','MH','Parbhani'),(158,'IN','MH','Jalna'),(159,'IN','MH','Aurangabad'),(160,'IN','MH','Nashik'),(161,'IN','MH','Thane'),(162,'IN','MH','Mumbai'),(163,'IN','MH','Raigarh(MH)'),(164,'IN','MH','Pune'),(165,'IN','MH','Ahmed Nagar'),(166,'IN','MH','Beed'),(167,'IN','MH','Latur'),(168,'IN','MH','Osmanabad'),(169,'IN','MH','Solapur'),(170,'IN','MH','Satara'),(171,'IN','MH','Ratnagiri'),(172,'IN','MH','Sindhudurg'),(173,'IN','MH','Kolhapur'),(174,'IN','MH','Sangli'),(175,'IN','MN','Senapati'),(176,'IN','MN','Tamenglong'),(177,'IN','MN','Churachandpur'),(178,'IN','MN','Bishnupur'),(179,'IN','MN','Thoubal'),(180,'IN','MN','Imphal West'),(181,'IN','MN','Imphal East'),(182,'IN','MN','Ukhrul'),(183,'IN','MN','Chandel'),(184,'IN','ML','West Garo Hills'),(185,'IN','ML','East Garo Hills'),(186,'IN','ML','South Garo Hills'),(187,'IN','ML','West Khasi Hills'),(188,'IN','ML','Ri Bhoi'),(189,'IN','ML','East Khasi Hills'),(190,'IN','ML','Jaintia Hills'),(191,'IN','KA','Belgaum'),(192,'IN','KA','Bagalkot'),(193,'IN','KA','Bijapur(KAR)'),(194,'IN','KA','Bidar'),(195,'IN','KA','Raichur'),(196,'IN','KA','Koppal'),(197,'IN','KA','Gadag'),(198,'IN','KA','Dharwad'),(199,'IN','KA','Uttara Kannada'),(200,'IN','KA','Haveri'),(201,'IN','KA','Bellary'),(202,'IN','KA','Chitradurga'),(203,'IN','KA','Davangere'),(204,'IN','KA','Shimoga'),(205,'IN','KA','Udupi'),(206,'IN','KA','Chickmagalur'),(207,'IN','KA','Tumkur'),(208,'IN','KA','Bangalore'),(209,'IN','KA','Mandya'),(210,'IN','KA','Hassan'),(211,'IN','KA','Dakshina Kannada'),(212,'IN','KA','Kodagu'),(213,'IN','KA','Mysore'),(214,'IN','KA','Chamrajnagar'),(215,'IN','KA','Gulbarga'),(216,'IN','KA','Yadgir'),(217,'IN','KA','Kolar'),(218,'IN','KA','Chikkaballapur'),(219,'IN','KA','Bangalore Rural'),(220,'IN','KA','Ramanagar'),(221,'IN','NL','Mon'),(222,'IN','NL','Mokokchung'),(223,'IN','NL','Zunhebotto'),(224,'IN','NL','Wokha'),(225,'IN','NL','Dimapur'),(226,'IN','NL','Phek'),(227,'IN','NL','Tuensang'),(228,'IN','NL','Longleng'),(229,'IN','NL','Kiphire'),(230,'IN','NL','Kohima'),(231,'IN','NL','Peren'),(232,'IN','OD','Bargarh'),(233,'IN','OD','Jharsuguda'),(234,'IN','OD','Sambalpur'),(235,'IN','OD','Debagarh'),(236,'IN','OD','Sundergarh'),(237,'IN','OD','Kendujhar'),(238,'IN','OD','Mayurbhanj'),(239,'IN','OD','Baleswar'),(240,'IN','OD','Bhadrak'),(241,'IN','OD','Kendrapara'),(242,'IN','OD','Jagatsinghapur'),(243,'IN','OD','Cuttack'),(244,'IN','OD','Jajapur'),(245,'IN','OD','Dhenkanal'),(246,'IN','OD','Angul'),(247,'IN','OD','Nayagarh'),(248,'IN','OD','Khorda'),(249,'IN','OD','Puri'),(250,'IN','OD','Ganjam'),(251,'IN','OD','Gajapati'),(252,'IN','OD','Kandhamal'),(253,'IN','OD','Boudh'),(254,'IN','OD','Sonapur'),(255,'IN','OD','Balangir'),(256,'IN','OD','Nuapada'),(257,'IN','OD','Kalahandi'),(258,'IN','OD','Rayagada'),(259,'IN','OD','Nabarangapur'),(260,'IN','OD','Koraput'),(261,'IN','OD','Malkangiri'),(262,'IN','PY','Pondicherry'),(263,'IN','PY','Mahe'),(264,'IN','PY','Karaikal'),(265,'IN','PY','Gurdaspur'),(266,'IN','PB','Kapurthala'),(267,'IN','PB','Jalandhar'),(268,'IN','PB','Hoshiarpur'),(269,'IN','PB','Nawanshahr'),(270,'IN','PB','Fatehgarh Sahib'),(271,'IN','PB','Ludhiana'),(272,'IN','PB','Moga'),(273,'IN','PB','Firozpur'),(274,'IN','PB','Muktsar'),(275,'IN','PB','Faridkot'),(276,'IN','PB','Bathinda'),(277,'IN','PB','Mansa'),(278,'IN','PB','Patiala'),(279,'IN','PB','Amritsar'),(280,'IN','PB','Tarn Taran'),(281,'IN','PB','Rupnagar'),(282,'IN','PB','Ropar'),(283,'IN','PB','Mohali'),(284,'IN','PB','Sangrur'),(285,'IN','PB','Barnala'),(286,'IN','PB','Fazilka'),(287,'IN','PB','Pathankot'),(288,'IN','RJ','Ganganagar'),(289,'IN','RJ','Hanumangarh'),(290,'IN','RJ','Bikaner'),(291,'IN','RJ','Churu'),(292,'IN','RJ','Jhujhunu'),(293,'IN','RJ','Alwar'),(294,'IN','RJ','Bharatpur'),(295,'IN','RJ','Dholpur'),(296,'IN','RJ','Karauli'),(297,'IN','RJ','Sawai Madhopur'),(298,'IN','RJ','Dausa'),(299,'IN','RJ','Jaipur'),(300,'IN','RJ','Sikar'),(301,'IN','RJ','Nagaur'),(302,'IN','RJ','Jodhpur'),(303,'IN','RJ','Jaisalmer'),(304,'IN','RJ','Barmer'),(305,'IN','RJ','Jalor'),(306,'IN','RJ','Sirohi'),(307,'IN','RJ','Pali'),(308,'IN','RJ','Ajmer'),(309,'IN','RJ','Tonk'),(310,'IN','RJ','Bundi'),(311,'IN','RJ','Bhilwara'),(312,'IN','RJ','Rajsamand'),(313,'IN','RJ','Dungarpur'),(314,'IN','RJ','Banswara'),(315,'IN','RJ','Chittorgarh'),(316,'IN','RJ','Kota'),(317,'IN','RJ','Baran'),(318,'IN','RJ','Jhalawar'),(319,'IN','RJ','Udaipur'),(320,'IN','TN','Tiruvallur'),(321,'IN','TN','Chennai'),(322,'IN','TN','Kanchipuram'),(323,'IN','TN','Vellore'),(324,'IN','TN','Tiruvannamalai'),(325,'IN','TN','Villupuram'),(326,'IN','TN','Salem'),(327,'IN','TN','Namakkal'),(328,'IN','TN','Erode'),(329,'IN','TN','Nilgiris'),(330,'IN','TN','Dindigul'),(331,'IN','TN','Karur'),(332,'IN','TN','Tiruchirappalli'),(333,'IN','TN','Perambalur'),(334,'IN','TN','Ariyalur'),(335,'IN','TN','Cuddalore'),(336,'IN','TN','Nagapattinam'),(337,'IN','TN','Tiruvarur'),(338,'IN','TN','Thanjavur'),(339,'IN','TN','Pudukkottai'),(340,'IN','TN','Sivaganga'),(341,'IN','TN','Madurai'),(342,'IN','TN','Theni'),(343,'IN','TN','Virudhunagar'),(344,'IN','TN','Ramanathapuram'),(345,'IN','TN','Tuticorin'),(346,'IN','TN','Tirunelveli'),(347,'IN','TN','Kanyakumari'),(348,'IN','TN','Dharmapuri'),(349,'IN','TN','Krishnagiri'),(350,'IN','TN','Coimbatore'),(351,'IN','TR','West Tripura'),(352,'IN','TR','South Tripura'),(353,'IN','TR','Dhalai'),(354,'IN','TR','North Tripura'),(355,'IN','WB','Darjiling'),(356,'IN','WB','Jalpaiguri'),(357,'IN','WB','Cooch Behar'),(358,'IN','WB','North Dinajpur'),(359,'IN','WB','South Dinajpur'),(360,'IN','WB','Malda'),(361,'IN','WB','Murshidabad'),(362,'IN','WB','Birbhum'),(363,'IN','WB','Bardhaman'),(364,'IN','WB','Nadia'),(365,'IN','WB','North 24 Parganas'),(366,'IN','WB','Hooghly'),(367,'IN','WB','Bankura'),(368,'IN','WB','Puruliya'),(369,'IN','WB','Howrah'),(370,'IN','WB','Kolkata'),(371,'IN','WB','South 24 Parganas'),(372,'IN','WB','West Midnapore'),(373,'IN','WB','East Midnapore'),(374,'IN','WB','Medinipur'),(375,'IN','SK','North Sikkim'),(376,'IN','SK','West Sikkim'),(377,'IN','SK','South Sikkim'),(378,'IN','SK','East Sikkim'),(379,'IN','AR','Tawang'),(380,'IN','AR','West Kameng'),(381,'IN','AR','East Kameng'),(382,'IN','AR','Papum Pare'),(383,'IN','AR','Upper Subansiri'),(384,'IN','AR','West Siang'),(385,'IN','AR','East Siang'),(386,'IN','AR','Upper Siang'),(387,'IN','AR','Changlang'),(388,'IN','AR','Tirap'),(389,'IN','AR','Lower Subansiri'),(390,'IN','AR','Kurung Kumey'),(391,'IN','AR','Dibang Valley'),(392,'IN','AR','Lower Dibang Valley'),(393,'IN','AR','Lohit'),(394,'IN','MZ','Mammit'),(395,'IN','MZ','Kolasib'),(396,'IN','MZ','Aizawl'),(397,'IN','MZ','Champhai'),(398,'IN','MZ','Serchhip'),(399,'IN','MZ','Lunglei'),(400,'IN','MZ','Lawngtlai'),(401,'IN','MZ','Saiha'),(402,'IN','DD','Diu'),(403,'IN','DD','Daman'),(404,'IN','GA','North Goa'),(405,'IN','GA','South Goa'),(406,'IN','BR','West Champaran'),(407,'IN','BR','East Champaran'),(408,'IN','BR','Sheohar'),(409,'IN','BR','Sitamarhi'),(410,'IN','BR','Madhubani'),(411,'IN','BR','Supaul'),(412,'IN','BR','Araria'),(413,'IN','BR','Kishanganj'),(414,'IN','BR','Purnia'),(415,'IN','BR','Katihar'),(416,'IN','BR','Madhepura'),(417,'IN','BR','Saharsa'),(418,'IN','BR','Darbhanga'),(419,'IN','BR','Muzaffarpur'),(420,'IN','BR','Gopalganj'),(421,'IN','BR','Siwan'),(422,'IN','BR','Saran'),(423,'IN','BR','Vaishali'),(424,'IN','BR','Samastipur'),(425,'IN','BR','Begusarai'),(426,'IN','BR','Khagaria'),(427,'IN','BR','Bhagalpur'),(428,'IN','BR','Banka'),(429,'IN','BR','Munger'),(430,'IN','BR','Lakhisarai'),(431,'IN','BR','Sheikhpura'),(432,'IN','BR','Nalanda'),(433,'IN','BR','Patna'),(434,'IN','BR','Bhojpur'),(435,'IN','BR','Buxar'),(436,'IN','BR','Kaimur (Bhabua)'),(437,'IN','BR','Rohtas'),(438,'IN','BR','Aurangabad(BH)'),(439,'IN','BR','Gaya'),(440,'IN','BR','Nawada'),(441,'IN','BR','Jamui'),(442,'IN','BR','Jehanabad'),(443,'IN','BR','Arwal'),(444,'IN','MP','Sheopur'),(445,'IN','MP','Morena'),(446,'IN','MP','Bhind'),(447,'IN','MP','Gwalior'),(448,'IN','MP','Datia'),(449,'IN','MP','Shivpuri'),(450,'IN','MP','Tikamgarh'),(451,'IN','MP','Chhatarpur'),(452,'IN','MP','Panna'),(453,'IN','MP','Sagar'),(454,'IN','MP','Damoh'),(455,'IN','MP','Satna'),(456,'IN','MP','Rewa'),(457,'IN','MP','Umaria'),(458,'IN','MP','Neemuch'),(459,'IN','MP','Mandsaur'),(460,'IN','MP','Ratlam'),(461,'IN','MP','Ujjain'),(462,'IN','MP','Shajapur'),(463,'IN','MP','Dewas'),(464,'IN','MP','Dhar'),(465,'IN','MP','Indore'),(466,'IN','MP','Khargone'),(467,'IN','MP','Barwani'),(468,'IN','MP','Rajgarh'),(469,'IN','MP','Vidisha'),(470,'IN','MP','Bhopal'),(471,'IN','MP','Sehore'),(472,'IN','MP','Raisen'),(473,'IN','MP','Betul'),(474,'IN','MP','Harda'),(475,'IN','MP','Hoshangabad'),(476,'IN','MP','Katni'),(477,'IN','MP','Jabalpur'),(478,'IN','MP','Narsinghpur'),(479,'IN','MP','Dindori'),(480,'IN','MP','Mandla'),(481,'IN','MP','Chhindwara'),(482,'IN','MP','Seoni'),(483,'IN','MP','Balaghat'),(484,'IN','MP','Guna'),(485,'IN','MP','Ashok Nagar'),(486,'IN','MP','Shahdol'),(487,'IN','MP','Anuppur'),(488,'IN','MP','Sidhi'),(489,'IN','MP','Singrauli'),(490,'IN','MP','Jhabua'),(491,'IN','MP','Alirajpur'),(492,'IN','MP','East Nimar'),(493,'IN','MP','Burhanpur'),(494,'IN','MP','Khandwa'),(495,'IN','MP','West Nimar'),(496,'IN','UP','Saharanpur'),(497,'IN','UP','Muzaffarnagar'),(498,'IN','UP','Bijnor'),(499,'IN','UP','Moradabad'),(500,'IN','UP','Rampur'),(501,'IN','UP','Jyotiba Phule Nagar'),(502,'IN','UP','Meerut'),(503,'IN','UP','Bagpat'),(504,'IN','UP','Ghaziabad'),(505,'IN','UP','Gautam Buddha Nagar'),(506,'IN','UP','Bulandshahr'),(507,'IN','UP','Aligarh'),(508,'IN','UP','Hathras'),(509,'IN','UP','Mathura'),(510,'IN','UP','Agra'),(511,'IN','UP','Firozabad'),(512,'IN','UP','Mainpuri'),(513,'IN','UP','Budaun'),(514,'IN','UP','Bareilly'),(515,'IN','UP','Pilibhit'),(516,'IN','UP','Shahjahanpur'),(517,'IN','UP','Kheri'),(518,'IN','UP','Sitapur'),(519,'IN','UP','Hardoi'),(520,'IN','UP','Unnao'),(521,'IN','UP','Lucknow'),(522,'IN','UP','Raebareli'),(523,'IN','UP','Farrukhabad'),(524,'IN','UP','Kannauj'),(525,'IN','UP','Etawah'),(526,'IN','UP','Auraiya'),(527,'IN','UP','Kanpur Dehat'),(528,'IN','UP','Kanpur Nagar'),(529,'IN','UP','Jalaun'),(530,'IN','UP','Jhansi'),(531,'IN','UP','Lalitpur'),(532,'IN','UP','Hamirpur'),(533,'IN','UP','Mahoba'),(534,'IN','UP','Banda'),(535,'IN','UP','Chitrakoot'),(536,'IN','UP','Fatehpur'),(537,'IN','UP','Pratapgarh'),(538,'IN','UP','Kaushambi'),(539,'IN','UP','Allahabad'),(540,'IN','UP','Barabanki'),(541,'IN','UP','Faizabad'),(542,'IN','UP','Ambedkar Nagar'),(543,'IN','UP','Sultanpur'),(544,'IN','UP','Bahraich'),(545,'IN','UP','Shrawasti'),(546,'IN','UP','Balrampur'),(547,'IN','UP','Gonda'),(548,'IN','UP','Siddharthnagar'),(549,'IN','UP','Basti'),(550,'IN','UP','Sant Kabir Nagar'),(551,'IN','UP','Maharajganj'),(552,'IN','UP','Gorakhpur'),(553,'IN','UP','Kushinagar'),(554,'IN','UP','Deoria'),(555,'IN','UP','Azamgarh'),(556,'IN','UP','Mau'),(557,'IN','UP','Ballia'),(558,'IN','UP','Jaunpur'),(559,'IN','UP','Ghazipur'),(560,'IN','UP','Chandauli'),(561,'IN','UP','Varanasi'),(562,'IN','UP','Sant Ravidas Nagar'),(563,'IN','UP','Mirzapur'),(564,'IN','UP','Sonbhadra'),(565,'IN','UP','Etah'),(566,'IN','CT','Koriya'),(567,'IN','CT','Surguja'),(568,'IN','CT','Jashpur'),(569,'IN','CT','Raigarh'),(570,'IN','CT','Korba'),(571,'IN','CT','Janjgir-champa'),(572,'IN','CT','Bilaspur(CGH)'),(573,'IN','CT','Rajnandgaon'),(574,'IN','CT','Durg'),(575,'IN','CT','Raipur'),(576,'IN','CT','Mahasamund'),(577,'IN','CT','Dhamtari'),(578,'IN','CT','Kanker'),(579,'IN','CT','Bastar'),(580,'IN','CT','Narayanpur'),(581,'IN','CT','Dantewada'),(582,'IN','CT','Bijapur(CGH)'),(583,'IN','CT','Kawardha'),(584,'IN','CT','Gariaband'),(585,'IN','JH','Garhwa'),(586,'IN','JH','Chatra'),(587,'IN','JH','Koderma'),(588,'IN','JH','Giridh'),(589,'IN','JH','Deoghar'),(590,'IN','JH','Godda'),(591,'IN','JH','Sahibganj'),(592,'IN','JH','Pakur'),(593,'IN','JH','Dhanbad'),(594,'IN','JH','Bokaro'),(595,'IN','JH','Lohardaga'),(596,'IN','JH','East Singhbhum'),(597,'IN','JH','Palamau'),(598,'IN','JH','Latehar'),(599,'IN','JH','Hazaribag'),(600,'IN','JH','Ramgarh'),(601,'IN','JH','Dumka'),(602,'IN','JH','Jamtara'),(603,'IN','JH','Ranchi'),(604,'IN','JH','Khunti'),(605,'IN','JH','Gumla'),(606,'IN','JH','Simdega'),(607,'IN','JH','West Singhbhum'),(608,'IN','JH','Seraikela-kharsawan'),(609,'IN','UK','Uttarkashi'),(610,'IN','UK','Chamoli'),(611,'IN','UK','Rudraprayag'),(612,'IN','UK','Tehri Garhwal'),(613,'IN','UK','Dehradun'),(614,'IN','UK','Pauri Garhwal'),(615,'IN','UK','Pithoragarh'),(616,'IN','UK','Bageshwar'),(617,'IN','UK','Almora'),(618,'IN','UK','Champawat'),(619,'IN','UK','Nainital'),(620,'IN','UK','Udham Singh Nagar'),(621,'IN','UK','Haridwar'),(622,'IN','TS','Adilabad'),(623,'IN','TS','Nizamabad'),(624,'IN','TS','Karim Nagar'),(625,'IN','TS','Medak'),(626,'IN','TS','Hyderabad'),(627,'IN','TS','K.V.Rangareddy'),(628,'IN','TS','Mahabub Nagar'),(629,'IN','TS','Nalgonda'),(630,'IN','TS','Warangal'),(631,'IN','TS','Khammam');
/*!40000 ALTER TABLE `CITY` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `COMPANY`
--

DROP TABLE IF EXISTS `COMPANY`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `COMPANY` (
  `company_id` int(15) NOT NULL AUTO_INCREMENT,
  `short_name` varchar(50) DEFAULT NULL,
  `name` varchar(100) NOT NULL,
  `description` varchar(2000) DEFAULT NULL,
  `rating` int(2) DEFAULT NULL,
  `company_size_value_id` int(15) DEFAULT NULL,
  `company_type_value_id` int(15) DEFAULT NULL,
  `industry_type_value_id` int(15) DEFAULT NULL,
  `website_address` varchar(200) DEFAULT NULL,
  `logo` varchar(200) DEFAULT NULL,
  `mission_statement` varchar(2000) DEFAULT NULL,
  `objective` varchar(2000) DEFAULT NULL,
  `branding_image` varchar(200) DEFAULT NULL,
  `create_datetime` datetime NOT NULL,
  `update_datetime` datetime NOT NULL,
  `create_user_id` int(15) NOT NULL,
  `update_user_id` int(15) NOT NULL,
  `facebook` varchar(200) DEFAULT NULL,
  `linkedin` varchar(200) DEFAULT NULL,
  `twitter` varchar(200) DEFAULT NULL,
  `youtube` varchar(200) DEFAULT NULL,
  `internship_ind` varchar(1) DEFAULT NULL,
  `company_status_value_id` int(15) DEFAULT NULL,
  `search_name` varchar(100) NOT NULL,
  `search_short_name` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`company_id`),
  KEY `company_type_value_id` (`company_type_value_id`),
  KEY `company_size_value_id` (`company_size_value_id`),
  KEY `industry_type_value_id` (`industry_type_value_id`),
  KEY `IDX_COMPANY_1` (`search_name`),
  KEY `IDX_COMPANY_2` (`search_short_name`),
  CONSTRAINT `COMPANY_ibfk_1` FOREIGN KEY (`company_type_value_id`) REFERENCES `LOOKUP_VALUE` (`lookup_value_id`),
  CONSTRAINT `COMPANY_ibfk_2` FOREIGN KEY (`company_size_value_id`) REFERENCES `LOOKUP_VALUE` (`lookup_value_id`),
  CONSTRAINT `COMPANY_ibfk_3` FOREIGN KEY (`industry_type_value_id`) REFERENCES `LOOKUP_VALUE` (`lookup_value_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `COMPANY`
--

LOCK TABLES `COMPANY` WRITE;
/*!40000 ALTER TABLE `COMPANY` DISABLE KEYS */;
INSERT INTO `COMPANY` VALUES (1,NULL,'Scora Technologies Pvt. Ltd.','Scora is a technology company founded by IT industry veterans with 75+ years of experience and have been leaders in the space of technology. Company was formed with aim of building innovative solutions for the business issues and efficiency improvement.',NULL,344,254,258,'www.scoratech.com','https://prodbackend.scoraxchange.com/api/Attachments/scora-company-logo/download/logo scora.png',NULL,'Our first product offering is a cloud-based digital platform ScoraXchange. A digital exchange with the latest tools & technology for search, data analytics, communications, and networking services, bringing together colleges, companies, testing & assessment schools on a single digital platform. ScoraXchange is designed to incentivize and enhance partnerships between governments, educators, training providers companies and students to better manage the transformative impact on employment, skills and education.','https://prodbackend.scoraxchange.com/api/Attachments/scora-company-branding-image/download/scora.jpg','2018-04-04 07:42:41','2018-04-04 07:42:41',1,1,'https://www.facebook.com/ScoraXchange/','https://www.linkedin.com/company/13670590/',NULL,NULL,'Y',363,'SCORA TECHNOLOGIES PVT. LTD.',NULL),(2,'Abjayon','Abjayon Consultancy Pvt. Ltd.','The Utilities industry is changing and evolving at a very fast pace, the DISCOMs (Distribution companies) are looking to capture consumption by the hour instead of monthly metered readings. With the changing industry, the technology needs to enable a smooth workflow is growing.\nThe Oracle suite of utilities products are fairly complex to configure and implement and needs very strong Utilities domain and Oracle product expertise to complete a successful implementation.\n \n\nThe Abjayon Utilities team has deep level of expertise on the entire Oracle utilities product stack. Most of our key members have number of years on Oracle utilities, while working at Oracle where they were an integral part of product development and consulting team.',NULL,344,253,258,'www.abjayon.com','https://prodbackend.scoraxchange.com/api/Attachments/scora-company-logo/download/Untitled-3.jpg','“To be a specialized services organization that focuses on select Industry segments to provide value added services and solutions driven by a high degree of technical depth and expertise.”','We partner with you and plug in the solutions to fill up the gaps for smooth working of Oracle products and services. Consolidated we have 40+ years of Strategy, Development and Customer Success experience in Oracle Utilities.','https://prodbackend.scoraxchange.com/api/Attachments/scora-company-branding-image/download/Untitled-2.jpg','2018-04-04 07:44:13','2018-04-04 07:44:13',1,1,'www.facebook.com','www.linkedin.com','www.twitter.com','www.youtube.com','Y',363,'ABJAYON CONSULTANCY PVT. LTD.','ACPT');
/*!40000 ALTER TABLE `COMPANY` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `COMPANY_ADDRESS`
--

DROP TABLE IF EXISTS `COMPANY_ADDRESS`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `COMPANY_ADDRESS` (
  `company_id` int(15) NOT NULL,
  `address_id` int(15) NOT NULL,
  `primary_ind` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`company_id`,`address_id`),
  KEY `IDX_COMP_ADD` (`address_id`),
  CONSTRAINT `COMPANY_ADDRESS_ibfk_1` FOREIGN KEY (`company_id`) REFERENCES `COMPANY` (`company_id`),
  CONSTRAINT `COMPANY_ADDRESS_ibfk_2` FOREIGN KEY (`address_id`) REFERENCES `ADDRESS` (`address_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `COMPANY_ADDRESS`
--

LOCK TABLES `COMPANY_ADDRESS` WRITE;
/*!40000 ALTER TABLE `COMPANY_ADDRESS` DISABLE KEYS */;
INSERT INTO `COMPANY_ADDRESS` VALUES (1,3,'Y'),(2,1,'N'),(2,2,'N'),(2,4,'Y');
/*!40000 ALTER TABLE `COMPANY_ADDRESS` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `COMPANY_CONTACT`
--

DROP TABLE IF EXISTS `COMPANY_CONTACT`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `COMPANY_CONTACT` (
  `company_id` int(15) NOT NULL,
  `contact_id` int(15) NOT NULL,
  `primary_ind` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`company_id`,`contact_id`),
  KEY `contact_id` (`contact_id`),
  CONSTRAINT `COMPANY_CONTACT_ibfk_1` FOREIGN KEY (`company_id`) REFERENCES `COMPANY` (`company_id`),
  CONSTRAINT `COMPANY_CONTACT_ibfk_2` FOREIGN KEY (`contact_id`) REFERENCES `CONTACT` (`contact_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `COMPANY_CONTACT`
--

LOCK TABLES `COMPANY_CONTACT` WRITE;
/*!40000 ALTER TABLE `COMPANY_CONTACT` DISABLE KEYS */;
INSERT INTO `COMPANY_CONTACT` VALUES (1,4,NULL),(1,5,NULL),(2,1,'Y'),(2,2,NULL),(2,3,NULL);
/*!40000 ALTER TABLE `COMPANY_CONTACT` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `COMPANY_HIRING_AGGREGATES`
--

DROP TABLE IF EXISTS `COMPANY_HIRING_AGGREGATES`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `COMPANY_HIRING_AGGREGATES` (
  `company_aggregate_id` int(15) NOT NULL AUTO_INCREMENT,
  `company_id` int(15) DEFAULT NULL,
  `organization_id` int(15) DEFAULT NULL,
  `campus_id` int(15) DEFAULT NULL,
  `no_of_offers` int(5) DEFAULT NULL,
  `sum_of_offers` decimal(18,6) DEFAULT NULL,
  `minimum_offer` decimal(18,6) DEFAULT NULL,
  `maximum_offer` decimal(18,6) DEFAULT NULL,
  `calendar_year` int(4) DEFAULT NULL,
  `job_role_id` int(15) DEFAULT NULL,
  PRIMARY KEY (`company_aggregate_id`),
  KEY `company_id` (`company_id`),
  KEY `organization_id` (`organization_id`),
  KEY `campus_id` (`campus_id`),
  KEY `IDX_COMPANY_HIRING_AGGREGATES_1` (`company_id`,`calendar_year`),
  CONSTRAINT `COMPANY_HIRING_AGGREGATES_ibfk_1` FOREIGN KEY (`company_id`) REFERENCES `COMPANY` (`company_id`),
  CONSTRAINT `COMPANY_HIRING_AGGREGATES_ibfk_2` FOREIGN KEY (`organization_id`) REFERENCES `ORGANIZATION` (`organization_id`),
  CONSTRAINT `COMPANY_HIRING_AGGREGATES_ibfk_3` FOREIGN KEY (`campus_id`) REFERENCES `CAMPUS` (`campus_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `COMPANY_HIRING_AGGREGATES`
--

LOCK TABLES `COMPANY_HIRING_AGGREGATES` WRITE;
/*!40000 ALTER TABLE `COMPANY_HIRING_AGGREGATES` DISABLE KEYS */;
/*!40000 ALTER TABLE `COMPANY_HIRING_AGGREGATES` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Temporary view structure for view `COMPANY_HIRING_AGGREGATES_VW`
--

DROP TABLE IF EXISTS `COMPANY_HIRING_AGGREGATES_VW`;
/*!50001 DROP VIEW IF EXISTS `COMPANY_HIRING_AGGREGATES_VW`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `COMPANY_HIRING_AGGREGATES_VW` AS SELECT 
 1 AS `company_id`,
 1 AS `calendar_year`,
 1 AS `avg_salary`,
 1 AS `total_no_of_offers`*/;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `COMPANY_HIRING_AGGREGATES_WORK`
--

DROP TABLE IF EXISTS `COMPANY_HIRING_AGGREGATES_WORK`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `COMPANY_HIRING_AGGREGATES_WORK` (
  `row_number` int(5) DEFAULT NULL,
  `organization_name` varchar(100) DEFAULT NULL,
  `campus_name` varchar(100) DEFAULT NULL,
  `job_role_name` varchar(50) DEFAULT NULL,
  `no_of_offers` int(5) DEFAULT NULL,
  `sum_of_offers` decimal(18,6) DEFAULT NULL,
  `minimum_offer` decimal(18,6) DEFAULT NULL,
  `maximum_offer` decimal(18,6) DEFAULT NULL,
  `calendar_year` int(4) DEFAULT NULL,
  `error` varchar(500) DEFAULT NULL,
  `organization_id` int(15) DEFAULT NULL,
  `job_role_id` int(15) DEFAULT NULL,
  `campus_id` int(15) DEFAULT NULL,
  `company_upload_log_id` int(15) DEFAULT NULL,
  KEY `company_upload_log_id` (`company_upload_log_id`),
  CONSTRAINT `COMPANY_HIRING_AGGREGATES_WORK_ibfk_1` FOREIGN KEY (`company_upload_log_id`) REFERENCES `COMPANY_UPLOAD_LOG` (`company_upload_log_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `COMPANY_HIRING_AGGREGATES_WORK`
--

LOCK TABLES `COMPANY_HIRING_AGGREGATES_WORK` WRITE;
/*!40000 ALTER TABLE `COMPANY_HIRING_AGGREGATES_WORK` DISABLE KEYS */;
/*!40000 ALTER TABLE `COMPANY_HIRING_AGGREGATES_WORK` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `COMPANY_PROGRAM_AGGREGATES`
--

DROP TABLE IF EXISTS `COMPANY_PROGRAM_AGGREGATES`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `COMPANY_PROGRAM_AGGREGATES` (
  `comp_prog_aggregate_id` int(15) NOT NULL AUTO_INCREMENT,
  `company_id` int(15) DEFAULT NULL,
  `organization_id` int(15) DEFAULT NULL,
  `campus_id` int(15) DEFAULT NULL,
  `program_major_value_id` int(15) DEFAULT NULL,
  `no_of_offers` int(5) DEFAULT NULL,
  `calendar_year` int(4) DEFAULT NULL,
  `no_of_interns` int(15) DEFAULT NULL,
  PRIMARY KEY (`comp_prog_aggregate_id`),
  KEY `company_id` (`company_id`),
  KEY `campus_id` (`campus_id`),
  KEY `organization_id` (`organization_id`),
  CONSTRAINT `COMPANY_PROGRAM_AGGREGATES_ibfk_1` FOREIGN KEY (`company_id`) REFERENCES `COMPANY` (`company_id`),
  CONSTRAINT `COMPANY_PROGRAM_AGGREGATES_ibfk_2` FOREIGN KEY (`campus_id`) REFERENCES `CAMPUS` (`campus_id`),
  CONSTRAINT `COMPANY_PROGRAM_AGGREGATES_ibfk_3` FOREIGN KEY (`organization_id`) REFERENCES `ORGANIZATION` (`organization_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `COMPANY_PROGRAM_AGGREGATES`
--

LOCK TABLES `COMPANY_PROGRAM_AGGREGATES` WRITE;
/*!40000 ALTER TABLE `COMPANY_PROGRAM_AGGREGATES` DISABLE KEYS */;
/*!40000 ALTER TABLE `COMPANY_PROGRAM_AGGREGATES` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Temporary view structure for view `COMPANY_SEARCH_VW`
--

DROP TABLE IF EXISTS `COMPANY_SEARCH_VW`;
/*!50001 DROP VIEW IF EXISTS `COMPANY_SEARCH_VW`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `COMPANY_SEARCH_VW` AS SELECT 
 1 AS `name`,
 1 AS `search_name`,
 1 AS `short_name`,
 1 AS `search_short_name`,
 1 AS `company_id`,
 1 AS `city_id`,
 1 AS `city_name`,
 1 AS `state_code`,
 1 AS `region_flag`,
 1 AS `company_type_value_id`,
 1 AS `industry_type_value_id`,
 1 AS `company_size_value_id`,
 1 AS `internship_ind`,
 1 AS `rating`,
 1 AS `company_status_value_id`,
 1 AS `company_type`,
 1 AS `company_size`,
 1 AS `industry_type`,
 1 AS `company_status`*/;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `COMPANY_UPLOAD_LOG`
--

DROP TABLE IF EXISTS `COMPANY_UPLOAD_LOG`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `COMPANY_UPLOAD_LOG` (
  `company_upload_log_id` int(15) NOT NULL AUTO_INCREMENT,
  `comp_upload_type_value_id` int(15) NOT NULL,
  `company_id` int(15) NOT NULL,
  `input_parameters` varchar(2000) DEFAULT NULL,
  `create_user_id` int(15) DEFAULT NULL,
  `create_datetime` datetime DEFAULT NULL,
  `csv_file_location` varchar(200) DEFAULT NULL,
  `error_file_location` varchar(200) DEFAULT NULL,
  `total_no_recs` int(10) DEFAULT NULL,
  `no_success_recs` int(10) DEFAULT NULL,
  `no_fail_recs` int(10) DEFAULT NULL,
  PRIMARY KEY (`company_upload_log_id`),
  KEY `company_id` (`company_id`),
  KEY `comp_upload_type_value_id` (`comp_upload_type_value_id`),
  KEY `IDX_COMPANY_UPLOAD_LOG_1` (`company_id`),
  CONSTRAINT `COMPANY_UPLOAD_LOG_ibfk_1` FOREIGN KEY (`company_id`) REFERENCES `COMPANY` (`company_id`),
  CONSTRAINT `COMPANY_UPLOAD_LOG_ibfk_2` FOREIGN KEY (`comp_upload_type_value_id`) REFERENCES `LOOKUP_VALUE` (`lookup_value_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `COMPANY_UPLOAD_LOG`
--

LOCK TABLES `COMPANY_UPLOAD_LOG` WRITE;
/*!40000 ALTER TABLE `COMPANY_UPLOAD_LOG` DISABLE KEYS */;
/*!40000 ALTER TABLE `COMPANY_UPLOAD_LOG` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `COMPENSATION_PACKAGE`
--

DROP TABLE IF EXISTS `COMPENSATION_PACKAGE`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `COMPENSATION_PACKAGE` (
  `comp_package_id` int(15) NOT NULL AUTO_INCREMENT,
  `company_id` int(15) NOT NULL,
  `comp_package_name` varchar(50) NOT NULL,
  `description` varchar(100) DEFAULT NULL,
  `comp_approval_status_value_id` int(15) NOT NULL,
  `create_datetime` datetime NOT NULL,
  `update_datetime` datetime NOT NULL,
  `create_user_id` int(15) NOT NULL,
  `update_user_id` int(15) NOT NULL,
  `total_comp_pkg_value` decimal(18,6) NOT NULL,
  PRIMARY KEY (`comp_package_id`),
  KEY `IDX_FK_COMP_PKG` (`company_id`),
  KEY `comp_approval_status_value_id` (`comp_approval_status_value_id`),
  CONSTRAINT `COMPENSATION_PACKAGE_ibfk_1` FOREIGN KEY (`company_id`) REFERENCES `COMPANY` (`company_id`),
  CONSTRAINT `COMPENSATION_PACKAGE_ibfk_3` FOREIGN KEY (`comp_approval_status_value_id`) REFERENCES `LOOKUP_VALUE` (`lookup_value_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `COMPENSATION_PACKAGE`
--

LOCK TABLES `COMPENSATION_PACKAGE` WRITE;
/*!40000 ALTER TABLE `COMPENSATION_PACKAGE` DISABLE KEYS */;
/*!40000 ALTER TABLE `COMPENSATION_PACKAGE` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `COMPENSATION_PACKAGE_ITEM`
--

DROP TABLE IF EXISTS `COMPENSATION_PACKAGE_ITEM`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `COMPENSATION_PACKAGE_ITEM` (
  `comp_package_item_id` int(15) NOT NULL AUTO_INCREMENT,
  `comp_package_id` int(15) NOT NULL,
  `company_id` int(15) NOT NULL,
  `comp_package_item_name` varchar(50) NOT NULL,
  `comp_package_type_value_id` int(15) DEFAULT NULL,
  `description` varchar(100) DEFAULT NULL,
  `currency_code` varchar(16) DEFAULT NULL,
  `amount` decimal(18,6) DEFAULT NULL,
  `create_datetime` datetime NOT NULL,
  `update_datetime` datetime NOT NULL,
  `create_user_id` int(15) NOT NULL,
  `update_user_id` int(15) NOT NULL,
  PRIMARY KEY (`comp_package_item_id`),
  KEY `comp_package_id` (`comp_package_id`),
  KEY `comp_package_type_value_id` (`comp_package_type_value_id`),
  KEY `IDX_COMPENSATION_PACKAGE_ITEM_1` (`company_id`,`comp_package_id`),
  CONSTRAINT `COMPENSATION_PACKAGE_ITEM_ibfk_1` FOREIGN KEY (`company_id`) REFERENCES `COMPANY` (`company_id`),
  CONSTRAINT `COMPENSATION_PACKAGE_ITEM_ibfk_2` FOREIGN KEY (`comp_package_id`) REFERENCES `COMPENSATION_PACKAGE` (`comp_package_id`),
  CONSTRAINT `COMPENSATION_PACKAGE_ITEM_ibfk_3` FOREIGN KEY (`comp_package_type_value_id`) REFERENCES `LOOKUP_VALUE` (`lookup_value_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `COMPENSATION_PACKAGE_ITEM`
--

LOCK TABLES `COMPENSATION_PACKAGE_ITEM` WRITE;
/*!40000 ALTER TABLE `COMPENSATION_PACKAGE_ITEM` DISABLE KEYS */;
/*!40000 ALTER TABLE `COMPENSATION_PACKAGE_ITEM` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `CONTACT`
--

DROP TABLE IF EXISTS `CONTACT`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `CONTACT` (
  `contact_id` int(15) NOT NULL AUTO_INCREMENT,
  `contact_type_value_id` int(15) NOT NULL,
  `contact_info` varchar(100) DEFAULT NULL,
  `create_datetime` datetime NOT NULL,
  `update_datetime` datetime NOT NULL,
  `create_user_id` int(15) NOT NULL,
  `update_user_id` int(15) NOT NULL,
  PRIMARY KEY (`contact_id`),
  KEY `contact_type_value_id` (`contact_type_value_id`),
  CONSTRAINT `CONTACT_ibfk_1` FOREIGN KEY (`contact_type_value_id`) REFERENCES `LOOKUP_VALUE` (`lookup_value_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `CONTACT`
--

LOCK TABLES `CONTACT` WRITE;
/*!40000 ALTER TABLE `CONTACT` DISABLE KEYS */;
INSERT INTO `CONTACT` VALUES (1,15,'9948018889','2018-04-04 10:31:03','2018-04-04 10:31:03',13163,13163),(2,13,'info@abjayon.com','2018-04-04 10:32:38','2018-04-04 10:32:38',13163,13163),(3,15,'5108243260','2018-04-04 10:38:32','2018-04-04 10:38:32',13163,13163),(4,13,'jayanarayan.rayaroth@scoratech.com','2018-04-04 10:47:04','2018-04-04 10:47:04',13162,13162),(5,14,'9076300300','2018-04-04 10:56:16','2018-04-04 10:56:16',13162,13162),(6,13,'jayanarayan.rayaroth@scoratech.com','2018-04-04 11:43:47','2018-04-04 11:43:47',1,1);
/*!40000 ALTER TABLE `CONTACT` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `COUNTRY`
--

DROP TABLE IF EXISTS `COUNTRY`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `COUNTRY` (
  `country_code` varchar(16) NOT NULL,
  `country_name` varchar(100) NOT NULL,
  PRIMARY KEY (`country_code`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `COUNTRY`
--

LOCK TABLES `COUNTRY` WRITE;
/*!40000 ALTER TABLE `COUNTRY` DISABLE KEYS */;
INSERT INTO `COUNTRY` VALUES ('IN','India');
/*!40000 ALTER TABLE `COUNTRY` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `CURRENCY`
--

DROP TABLE IF EXISTS `CURRENCY`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `CURRENCY` (
  `currency_code` varchar(16) NOT NULL,
  `symbol` varchar(4) NOT NULL,
  `description` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`currency_code`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `CURRENCY`
--

LOCK TABLES `CURRENCY` WRITE;
/*!40000 ALTER TABLE `CURRENCY` DISABLE KEYS */;
INSERT INTO `CURRENCY` VALUES ('INR','Rs.','Indian Rupees');
/*!40000 ALTER TABLE `CURRENCY` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `DEPARTMENT`
--

DROP TABLE IF EXISTS `DEPARTMENT`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `DEPARTMENT` (
  `department_id` int(15) NOT NULL AUTO_INCREMENT,
  `campus_id` int(15) NOT NULL,
  `short_name` varchar(50) DEFAULT NULL,
  `name` varchar(100) DEFAULT NULL,
  `description` varchar(2000) DEFAULT NULL,
  `create_datetime` datetime NOT NULL,
  `update_datetime` datetime NOT NULL,
  `create_user_id` int(15) NOT NULL,
  `update_user_id` int(15) NOT NULL,
  `logo` varchar(200) DEFAULT NULL,
  `branding_image` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`department_id`),
  KEY `IDX_FK_DEPT` (`campus_id`),
  CONSTRAINT `DEPARTMENT_ibfk_1` FOREIGN KEY (`campus_id`) REFERENCES `CAMPUS` (`campus_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `DEPARTMENT`
--

LOCK TABLES `DEPARTMENT` WRITE;
/*!40000 ALTER TABLE `DEPARTMENT` DISABLE KEYS */;
/*!40000 ALTER TABLE `DEPARTMENT` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `DEPARTMENT_ADDRESS`
--

DROP TABLE IF EXISTS `DEPARTMENT_ADDRESS`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `DEPARTMENT_ADDRESS` (
  `department_id` int(15) NOT NULL,
  `campus_id` int(15) NOT NULL,
  `address_id` int(15) NOT NULL,
  `primary_ind` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`department_id`,`address_id`),
  KEY `campus_id` (`campus_id`),
  KEY `address_id` (`address_id`),
  CONSTRAINT `DEPARTMENT_ADDRESS_ibfk_1` FOREIGN KEY (`department_id`) REFERENCES `DEPARTMENT` (`department_id`),
  CONSTRAINT `DEPARTMENT_ADDRESS_ibfk_2` FOREIGN KEY (`campus_id`) REFERENCES `CAMPUS` (`campus_id`),
  CONSTRAINT `DEPARTMENT_ADDRESS_ibfk_3` FOREIGN KEY (`address_id`) REFERENCES `ADDRESS` (`address_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `DEPARTMENT_ADDRESS`
--

LOCK TABLES `DEPARTMENT_ADDRESS` WRITE;
/*!40000 ALTER TABLE `DEPARTMENT_ADDRESS` DISABLE KEYS */;
/*!40000 ALTER TABLE `DEPARTMENT_ADDRESS` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `DEPARTMENT_CONTACT`
--

DROP TABLE IF EXISTS `DEPARTMENT_CONTACT`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `DEPARTMENT_CONTACT` (
  `department_id` int(15) NOT NULL,
  `contact_id` int(15) NOT NULL,
  `campus_id` int(15) NOT NULL,
  `primary_ind` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`department_id`,`contact_id`),
  KEY `contact_id` (`contact_id`),
  KEY `IDX_FK_DEPT_CONT` (`campus_id`),
  CONSTRAINT `DEPARTMENT_CONTACT_ibfk_1` FOREIGN KEY (`department_id`) REFERENCES `DEPARTMENT` (`department_id`),
  CONSTRAINT `DEPARTMENT_CONTACT_ibfk_2` FOREIGN KEY (`contact_id`) REFERENCES `CONTACT` (`contact_id`),
  CONSTRAINT `DEPARTMENT_CONTACT_ibfk_3` FOREIGN KEY (`campus_id`) REFERENCES `CAMPUS` (`campus_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `DEPARTMENT_CONTACT`
--

LOCK TABLES `DEPARTMENT_CONTACT` WRITE;
/*!40000 ALTER TABLE `DEPARTMENT_CONTACT` DISABLE KEYS */;
/*!40000 ALTER TABLE `DEPARTMENT_CONTACT` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `EDUCATION_PERSON`
--

DROP TABLE IF EXISTS `EDUCATION_PERSON`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `EDUCATION_PERSON` (
  `education_person_id` int(15) NOT NULL AUTO_INCREMENT,
  `campus_id` int(15) NOT NULL,
  `user_id` int(15) NOT NULL,
  `first_name` varchar(50) NOT NULL,
  `middle_name` varchar(50) DEFAULT NULL,
  `last_name` varchar(50) DEFAULT NULL,
  `prefix_value_id` int(15) DEFAULT NULL,
  `gender_value_id` int(15) DEFAULT NULL,
  `person_type_value_id` int(15) DEFAULT NULL,
  `designation` varchar(100) DEFAULT NULL,
  `create_datetime` datetime NOT NULL,
  `update_datetime` datetime NOT NULL,
  `create_user_id` int(15) NOT NULL,
  `update_user_id` int(15) NOT NULL,
  `picture_url` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`education_person_id`),
  KEY `IDX_FK_EDU_PER_CAM` (`campus_id`),
  KEY `IDX_FK_EDU_PER_USR` (`user_id`),
  KEY `prefix_value_id` (`prefix_value_id`),
  KEY `gender_value_id` (`gender_value_id`),
  KEY `person_type_value_id` (`person_type_value_id`),
  CONSTRAINT `EDUCATION_PERSON_ibfk_1` FOREIGN KEY (`campus_id`) REFERENCES `CAMPUS` (`campus_id`),
  CONSTRAINT `EDUCATION_PERSON_ibfk_3` FOREIGN KEY (`prefix_value_id`) REFERENCES `LOOKUP_VALUE` (`lookup_value_id`),
  CONSTRAINT `EDUCATION_PERSON_ibfk_4` FOREIGN KEY (`gender_value_id`) REFERENCES `LOOKUP_VALUE` (`lookup_value_id`),
  CONSTRAINT `EDUCATION_PERSON_ibfk_5` FOREIGN KEY (`person_type_value_id`) REFERENCES `LOOKUP_VALUE` (`lookup_value_id`),
  CONSTRAINT `EDUCATION_PERSON_ibfk_6` FOREIGN KEY (`user_id`) REFERENCES `SCORA_USER` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `EDUCATION_PERSON`
--

LOCK TABLES `EDUCATION_PERSON` WRITE;
/*!40000 ALTER TABLE `EDUCATION_PERSON` DISABLE KEYS */;
INSERT INTO `EDUCATION_PERSON` VALUES (1,1,13164,'Nachiket',NULL,'Kulkarni',NULL,NULL,NULL,NULL,'2018-04-05 13:07:18','2018-04-05 13:07:18',1,1,NULL);
/*!40000 ALTER TABLE `EDUCATION_PERSON` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `EDUCATION_PERSON_ADDRESS`
--

DROP TABLE IF EXISTS `EDUCATION_PERSON_ADDRESS`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `EDUCATION_PERSON_ADDRESS` (
  `education_person_id` int(15) NOT NULL,
  `address_id` int(15) NOT NULL,
  `campus_id` int(15) NOT NULL,
  `primary_ind` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`education_person_id`,`address_id`),
  KEY `address_id` (`address_id`),
  KEY `IDX_FK_EDU_PER_ADD` (`campus_id`),
  CONSTRAINT `EDUCATION_PERSON_ADDRESS_ibfk_1` FOREIGN KEY (`education_person_id`) REFERENCES `EDUCATION_PERSON` (`education_person_id`),
  CONSTRAINT `EDUCATION_PERSON_ADDRESS_ibfk_2` FOREIGN KEY (`campus_id`) REFERENCES `CAMPUS` (`campus_id`),
  CONSTRAINT `EDUCATION_PERSON_ADDRESS_ibfk_3` FOREIGN KEY (`address_id`) REFERENCES `ADDRESS` (`address_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `EDUCATION_PERSON_ADDRESS`
--

LOCK TABLES `EDUCATION_PERSON_ADDRESS` WRITE;
/*!40000 ALTER TABLE `EDUCATION_PERSON_ADDRESS` DISABLE KEYS */;
/*!40000 ALTER TABLE `EDUCATION_PERSON_ADDRESS` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `EDUCATION_PERSON_CONTACT`
--

DROP TABLE IF EXISTS `EDUCATION_PERSON_CONTACT`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `EDUCATION_PERSON_CONTACT` (
  `education_person_id` int(15) NOT NULL,
  `contact_id` int(15) NOT NULL,
  `campus_id` int(15) NOT NULL,
  `primary_ind` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`education_person_id`,`contact_id`),
  KEY `contact_id` (`contact_id`),
  KEY `IDX_FK_EDU_PER_CONT` (`campus_id`),
  CONSTRAINT `EDUCATION_PERSON_CONTACT_ibfk_1` FOREIGN KEY (`education_person_id`) REFERENCES `EDUCATION_PERSON` (`education_person_id`),
  CONSTRAINT `EDUCATION_PERSON_CONTACT_ibfk_2` FOREIGN KEY (`contact_id`) REFERENCES `CONTACT` (`contact_id`),
  CONSTRAINT `EDUCATION_PERSON_CONTACT_ibfk_3` FOREIGN KEY (`campus_id`) REFERENCES `CAMPUS` (`campus_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `EDUCATION_PERSON_CONTACT`
--

LOCK TABLES `EDUCATION_PERSON_CONTACT` WRITE;
/*!40000 ALTER TABLE `EDUCATION_PERSON_CONTACT` DISABLE KEYS */;
/*!40000 ALTER TABLE `EDUCATION_PERSON_CONTACT` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `EMPLOYER_CAMPUS_LIST_COMP_PKG`
--

DROP TABLE IF EXISTS `EMPLOYER_CAMPUS_LIST_COMP_PKG`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `EMPLOYER_CAMPUS_LIST_COMP_PKG` (
  `list_comp_pkg_id` int(15) NOT NULL AUTO_INCREMENT,
  `list_id` int(15) NOT NULL,
  `company_id` int(15) NOT NULL,
  `comp_package_id` int(15) NOT NULL,
  `start_date` date DEFAULT NULL,
  `end_date` date DEFAULT NULL,
  `end_reason` varchar(100) DEFAULT NULL,
  `create_datetime` datetime NOT NULL,
  `update_datetime` datetime NOT NULL,
  `create_user_id` int(15) NOT NULL,
  `update_user_id` int(15) NOT NULL,
  PRIMARY KEY (`list_comp_pkg_id`),
  KEY `list_id` (`list_id`),
  KEY `comp_package_id` (`comp_package_id`),
  KEY `IDX_EMPLOYER_CAMPUS_LIST_COMP_PKG_1` (`company_id`,`list_id`),
  CONSTRAINT `EMPLOYER_CAMPUS_LIST_COMP_PKG_ibfk_1` FOREIGN KEY (`company_id`) REFERENCES `COMPANY` (`company_id`),
  CONSTRAINT `EMPLOYER_CAMPUS_LIST_COMP_PKG_ibfk_2` FOREIGN KEY (`list_id`) REFERENCES `EMPLOYER_CAMPUS_LIST_HDR` (`list_id`),
  CONSTRAINT `EMPLOYER_CAMPUS_LIST_COMP_PKG_ibfk_3` FOREIGN KEY (`comp_package_id`) REFERENCES `COMPENSATION_PACKAGE` (`comp_package_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `EMPLOYER_CAMPUS_LIST_COMP_PKG`
--

LOCK TABLES `EMPLOYER_CAMPUS_LIST_COMP_PKG` WRITE;
/*!40000 ALTER TABLE `EMPLOYER_CAMPUS_LIST_COMP_PKG` DISABLE KEYS */;
/*!40000 ALTER TABLE `EMPLOYER_CAMPUS_LIST_COMP_PKG` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `EMPLOYER_CAMPUS_LIST_DTL`
--

DROP TABLE IF EXISTS `EMPLOYER_CAMPUS_LIST_DTL`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `EMPLOYER_CAMPUS_LIST_DTL` (
  `list_campus_id` int(15) NOT NULL AUTO_INCREMENT,
  `list_id` int(15) NOT NULL,
  `company_id` int(15) NOT NULL,
  `campus_id` int(15) NOT NULL,
  `start_date` date DEFAULT NULL,
  `end_date` date DEFAULT NULL,
  `end_reason` varchar(100) DEFAULT NULL,
  `create_datetime` datetime NOT NULL,
  `update_datetime` datetime NOT NULL,
  `create_user_id` int(15) NOT NULL,
  `update_user_id` int(15) NOT NULL,
  PRIMARY KEY (`list_campus_id`),
  KEY `list_id` (`list_id`),
  KEY `campus_id` (`campus_id`),
  KEY `IDX_EMPLOYER_CAMPUS_LIST_DTL_1` (`company_id`,`list_id`),
  CONSTRAINT `EMPLOYER_CAMPUS_LIST_DTL_ibfk_1` FOREIGN KEY (`company_id`) REFERENCES `COMPANY` (`company_id`),
  CONSTRAINT `EMPLOYER_CAMPUS_LIST_DTL_ibfk_2` FOREIGN KEY (`list_id`) REFERENCES `EMPLOYER_CAMPUS_LIST_HDR` (`list_id`),
  CONSTRAINT `EMPLOYER_CAMPUS_LIST_DTL_ibfk_3` FOREIGN KEY (`campus_id`) REFERENCES `CAMPUS` (`campus_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `EMPLOYER_CAMPUS_LIST_DTL`
--

LOCK TABLES `EMPLOYER_CAMPUS_LIST_DTL` WRITE;
/*!40000 ALTER TABLE `EMPLOYER_CAMPUS_LIST_DTL` DISABLE KEYS */;
/*!40000 ALTER TABLE `EMPLOYER_CAMPUS_LIST_DTL` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `EMPLOYER_CAMPUS_LIST_HDR`
--

DROP TABLE IF EXISTS `EMPLOYER_CAMPUS_LIST_HDR`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `EMPLOYER_CAMPUS_LIST_HDR` (
  `list_id` int(15) NOT NULL AUTO_INCREMENT,
  `company_id` int(15) NOT NULL,
  `job_role_id` int(15) NOT NULL,
  `list_name` varchar(50) NOT NULL,
  `description` varchar(100) DEFAULT NULL,
  `comp_approval_status_value_id` int(15) NOT NULL,
  `create_datetime` datetime NOT NULL,
  `update_datetime` datetime NOT NULL,
  `create_user_id` int(15) NOT NULL,
  `update_user_id` int(15) NOT NULL,
  PRIMARY KEY (`list_id`),
  KEY `job_role_id` (`job_role_id`),
  KEY `comp_approval_status_value_id` (`comp_approval_status_value_id`),
  KEY `IDX_EMPLOYER_CAMPUS_LIST_HDR_1` (`company_id`,`job_role_id`),
  CONSTRAINT `EMPLOYER_CAMPUS_LIST_HDR_ibfk_1` FOREIGN KEY (`company_id`) REFERENCES `COMPANY` (`company_id`),
  CONSTRAINT `EMPLOYER_CAMPUS_LIST_HDR_ibfk_2` FOREIGN KEY (`job_role_id`) REFERENCES `JOB_ROLE` (`job_role_id`),
  CONSTRAINT `EMPLOYER_CAMPUS_LIST_HDR_ibfk_3` FOREIGN KEY (`comp_approval_status_value_id`) REFERENCES `LOOKUP_VALUE` (`lookup_value_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `EMPLOYER_CAMPUS_LIST_HDR`
--

LOCK TABLES `EMPLOYER_CAMPUS_LIST_HDR` WRITE;
/*!40000 ALTER TABLE `EMPLOYER_CAMPUS_LIST_HDR` DISABLE KEYS */;
/*!40000 ALTER TABLE `EMPLOYER_CAMPUS_LIST_HDR` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `EMPLOYER_DRIVE`
--

DROP TABLE IF EXISTS `EMPLOYER_DRIVE`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `EMPLOYER_DRIVE` (
  `emp_drive_id` int(15) NOT NULL AUTO_INCREMENT,
  `company_id` int(15) NOT NULL,
  `organization_id` int(15) DEFAULT NULL,
  `drive_name` varchar(50) DEFAULT NULL,
  `description` varchar(100) NOT NULL,
  `drive_type_value_id` int(15) NOT NULL,
  `job_role_id` int(15) DEFAULT NULL,
  `no_of_positions` int(15) DEFAULT NULL,
  `budget` decimal(18,6) DEFAULT NULL,
  `start_date` date NOT NULL,
  `end_date` date DEFAULT NULL,
  `drive_status_value_id` int(15) NOT NULL,
  `create_datetime` datetime NOT NULL,
  `update_datetime` datetime NOT NULL,
  `create_user_id` int(15) NOT NULL,
  `update_user_id` int(15) NOT NULL,
  PRIMARY KEY (`emp_drive_id`),
  KEY `job_role_id` (`job_role_id`),
  KEY `FK_EMP_DRV_ORG` (`organization_id`),
  KEY `drive_type_value_id` (`drive_type_value_id`),
  KEY `drive_status_value_id` (`drive_status_value_id`),
  KEY `IDX_EMPLOYER_DRIVE_1` (`company_id`,`start_date`,`drive_status_value_id`),
  KEY `IDX_EMPLOYER_DRIVE_2` (`company_id`),
  CONSTRAINT `EMPLOYER_DRIVE_ibfk_1` FOREIGN KEY (`company_id`) REFERENCES `COMPANY` (`company_id`),
  CONSTRAINT `EMPLOYER_DRIVE_ibfk_2` FOREIGN KEY (`organization_id`) REFERENCES `ORGANIZATION` (`organization_id`),
  CONSTRAINT `EMPLOYER_DRIVE_ibfk_3` FOREIGN KEY (`job_role_id`) REFERENCES `JOB_ROLE` (`job_role_id`),
  CONSTRAINT `EMPLOYER_DRIVE_ibfk_4` FOREIGN KEY (`drive_type_value_id`) REFERENCES `LOOKUP_VALUE` (`lookup_value_id`),
  CONSTRAINT `EMPLOYER_DRIVE_ibfk_5` FOREIGN KEY (`drive_status_value_id`) REFERENCES `LOOKUP_VALUE` (`lookup_value_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `EMPLOYER_DRIVE`
--

LOCK TABLES `EMPLOYER_DRIVE` WRITE;
/*!40000 ALTER TABLE `EMPLOYER_DRIVE` DISABLE KEYS */;
/*!40000 ALTER TABLE `EMPLOYER_DRIVE` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `EMPLOYER_DRIVE_CAMPUSES`
--

DROP TABLE IF EXISTS `EMPLOYER_DRIVE_CAMPUSES`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `EMPLOYER_DRIVE_CAMPUSES` (
  `emp_drive_campus_id` int(15) NOT NULL AUTO_INCREMENT,
  `emp_drive_id` int(15) NOT NULL,
  `company_id` int(15) NOT NULL,
  `list_id` int(15) NOT NULL,
  `campus_id` int(15) NOT NULL,
  `selection_flag` varchar(4) DEFAULT NULL,
  `create_datetime` datetime NOT NULL,
  `update_datetime` datetime NOT NULL,
  `create_user_id` int(15) NOT NULL,
  `update_user_id` int(15) NOT NULL,
  `emp_event_id` int(15) DEFAULT NULL,
  `status_value_id` int(15) DEFAULT NULL,
  PRIMARY KEY (`emp_drive_campus_id`),
  KEY `emp_drive_id` (`emp_drive_id`),
  KEY `list_id` (`list_id`),
  KEY `campus_id` (`campus_id`),
  KEY `emp_event_id` (`emp_event_id`),
  KEY `IDX_EMPLOYER_DRIVE_CAMPUSES_1` (`company_id`,`emp_drive_id`,`emp_event_id`),
  CONSTRAINT `EMPLOYER_DRIVE_CAMPUSES_ibfk_1` FOREIGN KEY (`emp_drive_id`) REFERENCES `EMPLOYER_DRIVE` (`emp_drive_id`),
  CONSTRAINT `EMPLOYER_DRIVE_CAMPUSES_ibfk_2` FOREIGN KEY (`company_id`) REFERENCES `COMPANY` (`company_id`),
  CONSTRAINT `EMPLOYER_DRIVE_CAMPUSES_ibfk_3` FOREIGN KEY (`list_id`) REFERENCES `EMPLOYER_CAMPUS_LIST_HDR` (`list_id`),
  CONSTRAINT `EMPLOYER_DRIVE_CAMPUSES_ibfk_4` FOREIGN KEY (`campus_id`) REFERENCES `CAMPUS` (`campus_id`),
  CONSTRAINT `EMPLOYER_DRIVE_CAMPUSES_ibfk_5` FOREIGN KEY (`emp_event_id`) REFERENCES `EMPLOYER_EVENT` (`emp_event_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `EMPLOYER_DRIVE_CAMPUSES`
--

LOCK TABLES `EMPLOYER_DRIVE_CAMPUSES` WRITE;
/*!40000 ALTER TABLE `EMPLOYER_DRIVE_CAMPUSES` DISABLE KEYS */;
/*!40000 ALTER TABLE `EMPLOYER_DRIVE_CAMPUSES` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `EMPLOYER_EVENT`
--

DROP TABLE IF EXISTS `EMPLOYER_EVENT`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `EMPLOYER_EVENT` (
  `emp_event_id` int(15) NOT NULL AUTO_INCREMENT,
  `emp_drive_id` int(15) NOT NULL,
  `company_id` int(15) NOT NULL,
  `cgpa` int(3) DEFAULT NULL,
  `event_name` varchar(100) NOT NULL,
  `event_type_value_id` int(15) NOT NULL,
  `event_status_value_id` int(15) NOT NULL,
  `scheduled_date` date DEFAULT NULL,
  `scheduled_start_time` time DEFAULT NULL,
  `duration` int(15) DEFAULT NULL,
  `host_name` varchar(100) DEFAULT NULL,
  `host_contact` varchar(100) DEFAULT NULL,
  `address_line1` varchar(100) DEFAULT NULL,
  `address_line2` varchar(100) DEFAULT NULL,
  `address_line3` varchar(100) DEFAULT NULL,
  `city_id` int(15) DEFAULT NULL,
  `state_code` varchar(16) DEFAULT NULL,
  `country_code` varchar(16) DEFAULT NULL,
  `postal_code` varchar(16) DEFAULT NULL,
  `create_datetime` datetime NOT NULL,
  `update_datetime` datetime NOT NULL,
  `create_user_id` int(15) NOT NULL,
  `update_user_id` int(15) NOT NULL,
  `event_requirement` varchar(2000) DEFAULT NULL,
  PRIMARY KEY (`emp_event_id`),
  KEY `emp_drive_id` (`emp_drive_id`),
  KEY `company_id` (`company_id`),
  KEY `event_type_value_id` (`event_type_value_id`),
  KEY `event_status_value_id` (`event_status_value_id`),
  KEY `IDX_EMPLOYER_EVENT_1` (`company_id`,`emp_drive_id`),
  CONSTRAINT `EMPLOYER_EVENT_ibfk_1` FOREIGN KEY (`emp_drive_id`) REFERENCES `EMPLOYER_DRIVE` (`emp_drive_id`),
  CONSTRAINT `EMPLOYER_EVENT_ibfk_2` FOREIGN KEY (`company_id`) REFERENCES `COMPANY` (`company_id`),
  CONSTRAINT `EMPLOYER_EVENT_ibfk_3` FOREIGN KEY (`event_type_value_id`) REFERENCES `LOOKUP_VALUE` (`lookup_value_id`),
  CONSTRAINT `EMPLOYER_EVENT_ibfk_4` FOREIGN KEY (`event_status_value_id`) REFERENCES `LOOKUP_VALUE` (`lookup_value_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `EMPLOYER_EVENT`
--

LOCK TABLES `EMPLOYER_EVENT` WRITE;
/*!40000 ALTER TABLE `EMPLOYER_EVENT` DISABLE KEYS */;
/*!40000 ALTER TABLE `EMPLOYER_EVENT` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `EMPLOYER_EVENT_PANEL`
--

DROP TABLE IF EXISTS `EMPLOYER_EVENT_PANEL`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `EMPLOYER_EVENT_PANEL` (
  `emp_event_panel_id` int(15) NOT NULL AUTO_INCREMENT,
  `emp_event_id` int(15) NOT NULL,
  `emp_drive_id` int(15) NOT NULL,
  `company_id` int(15) NOT NULL,
  `employer_person_id` int(15) NOT NULL,
  `create_datetime` datetime NOT NULL,
  `update_datetime` datetime NOT NULL,
  `create_user_id` int(15) NOT NULL,
  `update_user_id` int(15) NOT NULL,
  PRIMARY KEY (`emp_event_panel_id`),
  KEY `emp_drive_id` (`emp_drive_id`),
  KEY `emp_event_id` (`emp_event_id`),
  KEY `company_id` (`company_id`),
  KEY `employer_person_id` (`employer_person_id`),
  KEY `IDX_EMPLOYER_EVENT_PANEL_1` (`company_id`,`emp_event_id`),
  KEY `IDX_EMPLOYER_EVENT_PANEL_2` (`company_id`,`employer_person_id`),
  CONSTRAINT `EMPLOYER_EVENT_PANEL_ibfk_1` FOREIGN KEY (`emp_drive_id`) REFERENCES `EMPLOYER_DRIVE` (`emp_drive_id`),
  CONSTRAINT `EMPLOYER_EVENT_PANEL_ibfk_2` FOREIGN KEY (`emp_event_id`) REFERENCES `EMPLOYER_EVENT` (`emp_event_id`),
  CONSTRAINT `EMPLOYER_EVENT_PANEL_ibfk_3` FOREIGN KEY (`company_id`) REFERENCES `COMPANY` (`company_id`),
  CONSTRAINT `EMPLOYER_EVENT_PANEL_ibfk_4` FOREIGN KEY (`employer_person_id`) REFERENCES `EMPLOYER_PERSON` (`employer_person_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `EMPLOYER_EVENT_PANEL`
--

LOCK TABLES `EMPLOYER_EVENT_PANEL` WRITE;
/*!40000 ALTER TABLE `EMPLOYER_EVENT_PANEL` DISABLE KEYS */;
/*!40000 ALTER TABLE `EMPLOYER_EVENT_PANEL` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `EMPLOYER_PERSON`
--

DROP TABLE IF EXISTS `EMPLOYER_PERSON`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `EMPLOYER_PERSON` (
  `employer_person_id` int(15) NOT NULL AUTO_INCREMENT,
  `company_id` int(15) NOT NULL,
  `user_id` int(15) NOT NULL,
  `first_name` varchar(50) NOT NULL,
  `middle_name` varchar(50) DEFAULT NULL,
  `last_name` varchar(50) DEFAULT NULL,
  `prefix_value_id` int(15) DEFAULT NULL,
  `gender_value_id` int(15) DEFAULT NULL,
  `person_type_value_id` int(15) DEFAULT NULL,
  `designation` varchar(100) DEFAULT NULL,
  `create_datetime` datetime NOT NULL,
  `update_datetime` datetime NOT NULL,
  `create_user_id` int(15) NOT NULL,
  `update_user_id` int(15) NOT NULL,
  `picture_url` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`employer_person_id`),
  KEY `IDX_FK_EMP_PER_USR` (`user_id`),
  KEY `IDX_FK_EMP_PER_COM` (`company_id`),
  KEY `prefix_value_id` (`prefix_value_id`),
  KEY `gender_value_id` (`gender_value_id`),
  KEY `person_type_value_id` (`person_type_value_id`),
  CONSTRAINT `EMPLOYER_PERSON_ibfk_1` FOREIGN KEY (`company_id`) REFERENCES `COMPANY` (`company_id`),
  CONSTRAINT `EMPLOYER_PERSON_ibfk_3` FOREIGN KEY (`prefix_value_id`) REFERENCES `LOOKUP_VALUE` (`lookup_value_id`),
  CONSTRAINT `EMPLOYER_PERSON_ibfk_4` FOREIGN KEY (`gender_value_id`) REFERENCES `LOOKUP_VALUE` (`lookup_value_id`),
  CONSTRAINT `EMPLOYER_PERSON_ibfk_5` FOREIGN KEY (`person_type_value_id`) REFERENCES `LOOKUP_VALUE` (`lookup_value_id`),
  CONSTRAINT `EMPLOYER_PERSON_ibfk_6` FOREIGN KEY (`user_id`) REFERENCES `SCORA_USER` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `EMPLOYER_PERSON`
--

LOCK TABLES `EMPLOYER_PERSON` WRITE;
/*!40000 ALTER TABLE `EMPLOYER_PERSON` DISABLE KEYS */;
INSERT INTO `EMPLOYER_PERSON` VALUES (1,1,13162,'Jayanarayan',NULL,'Rayaroth',4,2,22,'Director Of Product Engineering','2018-04-04 07:42:41','2018-04-04 07:42:41',1,1,'Attachments/scora-employer-person-profile-picture/download/jay.jpg'),(2,2,13163,'Pavitra',NULL,'Kalra',NULL,NULL,NULL,NULL,'2018-04-04 07:44:13','2018-04-04 07:44:13',1,1,NULL);
/*!40000 ALTER TABLE `EMPLOYER_PERSON` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `EMPLOYER_PERSON_ADDRESS`
--

DROP TABLE IF EXISTS `EMPLOYER_PERSON_ADDRESS`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `EMPLOYER_PERSON_ADDRESS` (
  `employer_person_id` int(15) NOT NULL,
  `company_id` int(15) NOT NULL,
  `address_id` int(15) NOT NULL,
  `primary_ind` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`employer_person_id`,`address_id`),
  KEY `address_id` (`address_id`),
  KEY `IDX_EMPLOYER_PERSON_ADDRESS_1` (`company_id`,`employer_person_id`),
  CONSTRAINT `EMPLOYER_PERSON_ADDRESS_ibfk_1` FOREIGN KEY (`employer_person_id`) REFERENCES `EMPLOYER_PERSON` (`employer_person_id`),
  CONSTRAINT `EMPLOYER_PERSON_ADDRESS_ibfk_2` FOREIGN KEY (`company_id`) REFERENCES `COMPANY` (`company_id`),
  CONSTRAINT `EMPLOYER_PERSON_ADDRESS_ibfk_3` FOREIGN KEY (`address_id`) REFERENCES `ADDRESS` (`address_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `EMPLOYER_PERSON_ADDRESS`
--

LOCK TABLES `EMPLOYER_PERSON_ADDRESS` WRITE;
/*!40000 ALTER TABLE `EMPLOYER_PERSON_ADDRESS` DISABLE KEYS */;
/*!40000 ALTER TABLE `EMPLOYER_PERSON_ADDRESS` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `EMPLOYER_PERSON_CONTACT`
--

DROP TABLE IF EXISTS `EMPLOYER_PERSON_CONTACT`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `EMPLOYER_PERSON_CONTACT` (
  `employer_person_id` int(15) NOT NULL,
  `contact_id` int(15) NOT NULL,
  `company_id` int(15) NOT NULL,
  `primary_ind` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`employer_person_id`,`contact_id`),
  KEY `contact_id` (`contact_id`),
  KEY `IDX_EMPLOYER_PERSON_CONTACT_1` (`company_id`,`employer_person_id`),
  CONSTRAINT `EMPLOYER_PERSON_CONTACT_ibfk_1` FOREIGN KEY (`employer_person_id`) REFERENCES `EMPLOYER_PERSON` (`employer_person_id`),
  CONSTRAINT `EMPLOYER_PERSON_CONTACT_ibfk_2` FOREIGN KEY (`contact_id`) REFERENCES `CONTACT` (`contact_id`),
  CONSTRAINT `EMPLOYER_PERSON_CONTACT_ibfk_3` FOREIGN KEY (`company_id`) REFERENCES `COMPANY` (`company_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `EMPLOYER_PERSON_CONTACT`
--

LOCK TABLES `EMPLOYER_PERSON_CONTACT` WRITE;
/*!40000 ALTER TABLE `EMPLOYER_PERSON_CONTACT` DISABLE KEYS */;
INSERT INTO `EMPLOYER_PERSON_CONTACT` VALUES (1,6,1,'N');
/*!40000 ALTER TABLE `EMPLOYER_PERSON_CONTACT` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ENROLLMENT`
--

DROP TABLE IF EXISTS `ENROLLMENT`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ENROLLMENT` (
  `enrollment_id` int(15) NOT NULL AUTO_INCREMENT,
  `student_id` int(15) NOT NULL,
  `program_id` int(15) DEFAULT NULL,
  `admission_no` varchar(50) NOT NULL,
  `start_date` date NOT NULL,
  `actual_completion_date` date DEFAULT NULL,
  `cgpa_score` int(3) DEFAULT NULL,
  `highlights` varchar(2000) DEFAULT NULL,
  `hash_tag` varchar(2000) DEFAULT NULL,
  `data_verified_ind` varchar(1) DEFAULT NULL,
  `create_datetime` datetime NOT NULL,
  `update_datetime` datetime NOT NULL,
  `create_user_id` int(15) NOT NULL,
  `update_user_id` int(15) NOT NULL,
  `internship_avail_ind` varchar(1) DEFAULT NULL,
  `summ_prog_avail_ind` varchar(1) DEFAULT NULL,
  `placement_avail_ind` varchar(1) DEFAULT NULL,
  `planed_completion_date` date NOT NULL,
  `campus_id` int(15) DEFAULT NULL,
  PRIMARY KEY (`enrollment_id`),
  UNIQUE KEY `IDX_ENROLLMENT_01` (`campus_id`,`admission_no`,`program_id`),
  KEY `program_id` (`program_id`),
  KEY `IDX_FK_ENROLL_STU` (`student_id`),
  KEY `IDX_FK_ENROLL_ADM` (`admission_no`),
  CONSTRAINT `ENROLLMENT_ibfk_1` FOREIGN KEY (`student_id`) REFERENCES `STUDENT` (`student_id`),
  CONSTRAINT `ENROLLMENT_ibfk_2` FOREIGN KEY (`program_id`) REFERENCES `PROGRAM` (`program_id`),
  CONSTRAINT `ENROLLMENT_ibfk_3` FOREIGN KEY (`campus_id`) REFERENCES `CAMPUS` (`campus_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ENROLLMENT`
--

LOCK TABLES `ENROLLMENT` WRITE;
/*!40000 ALTER TABLE `ENROLLMENT` DISABLE KEYS */;
/*!40000 ALTER TABLE `ENROLLMENT` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `EVENT_STUDENT_LIST`
--

DROP TABLE IF EXISTS `EVENT_STUDENT_LIST`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `EVENT_STUDENT_LIST` (
  `student_list_id` int(15) NOT NULL AUTO_INCREMENT,
  `student_id` int(15) NOT NULL,
  `company_id` int(15) DEFAULT NULL,
  `employer_event_id` int(15) DEFAULT NULL,
  `campus_id` int(15) DEFAULT NULL,
  `campus_event_id` int(15) DEFAULT NULL,
  `candidate_status_value_id` int(15) DEFAULT NULL,
  `campus_comments` varchar(2000) DEFAULT NULL,
  `employer_comments` varchar(2000) DEFAULT NULL,
  `create_datetime` datetime NOT NULL,
  `update_datetime` datetime NOT NULL,
  `create_user_id` int(15) NOT NULL,
  `update_user_id` int(15) NOT NULL,
  `eligibility_ind` varchar(1) DEFAULT NULL,
  `student_subscribe_ind` varchar(1) DEFAULT NULL,
  `campus_publish_ind` varchar(1) DEFAULT NULL,
  `offer_status_value_id` int(15) DEFAULT NULL,
  `comp_package_id` int(15) DEFAULT NULL,
  `registration_ind` varchar(1) DEFAULT NULL,
  `total_comp_value` decimal(18,6) DEFAULT NULL,
  `department_id` int(15) DEFAULT NULL,
  PRIMARY KEY (`student_list_id`),
  KEY `employer_event_id` (`employer_event_id`),
  KEY `company_id` (`company_id`),
  KEY `campus_id` (`campus_id`),
  KEY `campus_event_id` (`campus_event_id`),
  KEY `IDX_FK_EVNT_STU_LST` (`student_id`),
  KEY `offer_status_value_id` (`offer_status_value_id`),
  KEY `comp_package_id` (`comp_package_id`),
  KEY `candidate_status_value_id` (`candidate_status_value_id`),
  KEY `department_id` (`department_id`),
  CONSTRAINT `EVENT_STUDENT_LIST_ibfk_2` FOREIGN KEY (`employer_event_id`) REFERENCES `EMPLOYER_EVENT` (`emp_event_id`),
  CONSTRAINT `EVENT_STUDENT_LIST_ibfk_3` FOREIGN KEY (`company_id`) REFERENCES `COMPANY` (`company_id`),
  CONSTRAINT `EVENT_STUDENT_LIST_ibfk_4` FOREIGN KEY (`campus_id`) REFERENCES `CAMPUS` (`campus_id`),
  CONSTRAINT `EVENT_STUDENT_LIST_ibfk_5` FOREIGN KEY (`campus_event_id`) REFERENCES `CAMPUS_EVENT` (`campus_event_id`),
  CONSTRAINT `EVENT_STUDENT_LIST_ibfk_6` FOREIGN KEY (`offer_status_value_id`) REFERENCES `LOOKUP_VALUE` (`lookup_value_id`),
  CONSTRAINT `EVENT_STUDENT_LIST_ibfk_7` FOREIGN KEY (`comp_package_id`) REFERENCES `COMPENSATION_PACKAGE` (`comp_package_id`),
  CONSTRAINT `EVENT_STUDENT_LIST_ibfk_8` FOREIGN KEY (`candidate_status_value_id`) REFERENCES `LOOKUP_VALUE` (`lookup_value_id`),
  CONSTRAINT `EVENT_STUDENT_LIST_ibfk_9` FOREIGN KEY (`department_id`) REFERENCES `DEPARTMENT` (`department_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `EVENT_STUDENT_LIST`
--

LOCK TABLES `EVENT_STUDENT_LIST` WRITE;
/*!40000 ALTER TABLE `EVENT_STUDENT_LIST` DISABLE KEYS */;
/*!40000 ALTER TABLE `EVENT_STUDENT_LIST` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `EVENT_TEST_SCORE`
--

DROP TABLE IF EXISTS `EVENT_TEST_SCORE`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `EVENT_TEST_SCORE` (
  `event_test_score_id` int(15) NOT NULL AUTO_INCREMENT,
  `student_id` int(15) NOT NULL,
  `test_name` varchar(50) DEFAULT NULL,
  `description` varchar(100) DEFAULT NULL,
  `score` int(3) DEFAULT NULL,
  `employer_event_id` int(15) NOT NULL,
  `test_vendor_name` varchar(50) DEFAULT NULL,
  `max_score` int(3) DEFAULT NULL,
  PRIMARY KEY (`event_test_score_id`),
  KEY `eventTestScoreIbfk1rel_idx` (`student_id`),
  CONSTRAINT `eventTestScoreIbfk1rel` FOREIGN KEY (`student_id`) REFERENCES `STUDENT` (`student_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `EVENT_TEST_SCORE`
--

LOCK TABLES `EVENT_TEST_SCORE` WRITE;
/*!40000 ALTER TABLE `EVENT_TEST_SCORE` DISABLE KEYS */;
/*!40000 ALTER TABLE `EVENT_TEST_SCORE` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `EVENT_TEST_SCORE_WORK`
--

DROP TABLE IF EXISTS `EVENT_TEST_SCORE_WORK`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `EVENT_TEST_SCORE_WORK` (
  `company_id` int(15) DEFAULT NULL,
  `employer_event_id` int(15) DEFAULT NULL,
  `student_id` int(15) DEFAULT NULL,
  `test_name` varchar(50) DEFAULT NULL,
  `description` varchar(100) DEFAULT NULL,
  `score` int(3) DEFAULT NULL,
  `error` varchar(100) DEFAULT NULL,
  `test_vendor_name` varchar(50) DEFAULT NULL,
  `max_score` int(3) DEFAULT NULL,
  `row_number` int(5) DEFAULT NULL,
  `student_email` varchar(254) DEFAULT NULL,
  `company_upload_log_id` int(15) DEFAULT NULL,
  KEY `company_upload_log_id` (`company_upload_log_id`),
  CONSTRAINT `EVENT_TEST_SCORE_WORK_ibfk_1` FOREIGN KEY (`company_upload_log_id`) REFERENCES `COMPANY_UPLOAD_LOG` (`company_upload_log_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `EVENT_TEST_SCORE_WORK`
--

LOCK TABLES `EVENT_TEST_SCORE_WORK` WRITE;
/*!40000 ALTER TABLE `EVENT_TEST_SCORE_WORK` DISABLE KEYS */;
/*!40000 ALTER TABLE `EVENT_TEST_SCORE_WORK` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `FIELD_LABEL`
--

DROP TABLE IF EXISTS `FIELD_LABEL`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `FIELD_LABEL` (
  `field_name` varchar(40) NOT NULL,
  `label` varchar(256) NOT NULL,
  PRIMARY KEY (`field_name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `FIELD_LABEL`
--

LOCK TABLES `FIELD_LABEL` WRITE;
/*!40000 ALTER TABLE `FIELD_LABEL` DISABLE KEYS */;
INSERT INTO `FIELD_LABEL` VALUES ('# field_name','label'),('about','About'),('addLine1','Address Line 1'),('addLine2','Address Line 2'),('addLine3','Address Line 3'),('addressInfoHead','Address Information'),('addressType','Address Type'),('amount','Amount'),('btnAdd','Add'),('btnCancel','Cancel'),('btnDelete','Delete'),('btnSave','Save'),('btnUpdate','Update'),('campusList','Institute List'),('campusName','Institute Name'),('campusShortName','Institute Short Name'),('city','City'),('cmpPackages','Compensation Packages'),('company','Employer'),('companyName','Employer Name'),('companyShortName','Employer Short Name'),('companySize','Employer Size'),('companyType','Employer Type'),('compensationApprStatus','Approval Status'),('compensationPckgName','Compensation Package Name'),('compensationPckgValue','Compensation Package Item'),('contactInfo','Contact Info'),('contactInfoHead','Contact Information'),('contactType','Contact Type'),('controlData','Control Data'),('country','Country'),('currencyType','CurrencyType'),('department','Department'),('departmentHead','Department Details'),('departmentName','Department Name'),('departmentShortName','Department Short Name'),('description','Description'),('designation','Designation'),('email','Email Address'),('estDate','Established Date'),('facebook','Facebook'),('fax','Fax Number'),('filterByName','Filter By Name'),('firstName','First name'),('gender','Gender'),('industryType','Industry Type'),('institute','Institute'),('instituteHead','Institute Details'),('internshipHead','Internship Offered?'),('jobRole','Job Role'),('jobRoleName','Job Role Name'),('jobRoleType','Job Role Type'),('lastname','Last Name'),('linkedIn','LinkedIn'),('listName','Name of the List'),('masterData','Master Data'),('middleName','Middle Name'),('missionStmt','Mission Statement'),('mobile','Mobile Number'),('no','No'),('noAddressInfo','Please Add Address'),('noContactInfo','Please Add contact'),('noDataFound','No Data Found'),('noOfStudents','No of Students'),('objective','Objective'),('offCampusInd','Off Institute Indication'),('organisation','Organization'),('organisationShortName','Organization Short Name'),('organizationName','Organization Name'),('packageName','Package Name'),('packageType','Package Type'),('pckgItemName','Package Item Name'),('pckgType','Package Type'),('phone','Phone Number'),('placement','Placement'),('postal','Postal Code'),('progCategory','Programme Category'),('progClassification','Programme Classification'),('progMajor','Programme Major '),('programme','Programme'),('programmeHead','Programme Details'),('programmeName','Programme Name'),('programmeStartDate','Programme Start Date'),('progType','Programme Type'),('recruitment','Recruitment'),('shortName','Short Name'),('skillSet','Skill Set'),('socialMediaHead','Social Media'),('state','State'),('status','Status'),('totCompensationPckgVal','Total Compensation Package Value'),('twitter','Twitter'),('universityName','University Name'),('uploadBrdLogoText','Upload Branding Logo'),('uploadCmpLogoText','Upload Company Logo'),('uploadImgHead','Upload Images'),('website','Website'),('yes','Yes'),('youtube','YouTube');
/*!40000 ALTER TABLE `FIELD_LABEL` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `JOB_ROLE`
--

DROP TABLE IF EXISTS `JOB_ROLE`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `JOB_ROLE` (
  `job_role_id` int(15) NOT NULL AUTO_INCREMENT,
  `company_id` int(15) NOT NULL,
  `job_role_name` varchar(50) DEFAULT NULL,
  `job_role_type_value_id` int(15) DEFAULT NULL,
  `skill_set` varchar(2000) DEFAULT NULL,
  `summary` varchar(2000) DEFAULT NULL,
  `create_datetime` datetime NOT NULL,
  `update_datetime` datetime NOT NULL,
  `create_user_id` int(15) NOT NULL,
  `update_user_id` int(15) NOT NULL,
  PRIMARY KEY (`job_role_id`),
  KEY `IDX_FK_JOB_ROLE` (`company_id`),
  KEY `job_role_type_value_id` (`job_role_type_value_id`),
  CONSTRAINT `JOB_ROLE_ibfk_1` FOREIGN KEY (`company_id`) REFERENCES `COMPANY` (`company_id`),
  CONSTRAINT `JOB_ROLE_ibfk_2` FOREIGN KEY (`job_role_type_value_id`) REFERENCES `LOOKUP_VALUE` (`lookup_value_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `JOB_ROLE`
--

LOCK TABLES `JOB_ROLE` WRITE;
/*!40000 ALTER TABLE `JOB_ROLE` DISABLE KEYS */;
INSERT INTO `JOB_ROLE` VALUES (1,1,'Sales and Marketing',325,'IT, budget and report writing skills, excellent sales, negotiation skills, ability to motivate and lead a team, excellent communication, people skills, good planning, organisational skills, the ability to work calmly under pressure',NULL,'2018-04-04 11:36:52','2018-04-04 11:36:52',13162,13162),(2,1,'Full Stack Developer',323,'Java, JavaScript, Angular4, NodeJS, MySQL, HTML, CSS, MongoDB, Design Patterns','Truly love working in a fast-paced, start-up and creative environment, dabble into new Web/Open Source technologies \nConsider yourself an enthusiastic, pro-active, dependable and collaborative team player with clear thinking and attention to detail\nDon\'t hesitate speaking up and challenge cliched and get a kick in finding innovative solutions to ambiguous problems','2018-04-04 11:39:58','2018-04-04 11:39:58',13162,13162);
/*!40000 ALTER TABLE `JOB_ROLE` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Temporary view structure for view `JOB_SRCH_OFF_CAMPUS_STUDENTS_VW`
--

DROP TABLE IF EXISTS `JOB_SRCH_OFF_CAMPUS_STUDENTS_VW`;
/*!50001 DROP VIEW IF EXISTS `JOB_SRCH_OFF_CAMPUS_STUDENTS_VW`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `JOB_SRCH_OFF_CAMPUS_STUDENTS_VW` AS SELECT 
 1 AS `company_id`,
 1 AS `name`,
 1 AS `industry_type_value_id`,
 1 AS `company_size_value_id`,
 1 AS `company_type_value_id`,
 1 AS `rating`,
 1 AS `emp_event_id`,
 1 AS `search_name`,
 1 AS `search_short_name`,
 1 AS `emp_drive_id`,
 1 AS `job_role_id`,
 1 AS `job_role_name`,
 1 AS `skill_set`,
 1 AS `summary`*/;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `LOOKUP_TYPE`
--

DROP TABLE IF EXISTS `LOOKUP_TYPE`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `LOOKUP_TYPE` (
  `lookup_type_id` int(15) NOT NULL AUTO_INCREMENT,
  `lookup_code` varchar(100) NOT NULL,
  `description` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`lookup_type_id`)
) ENGINE=InnoDB AUTO_INCREMENT=45 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `LOOKUP_TYPE`
--

LOCK TABLES `LOOKUP_TYPE` WRITE;
/*!40000 ALTER TABLE `LOOKUP_TYPE` DISABLE KEYS */;
INSERT INTO `LOOKUP_TYPE` VALUES (1,'GENDER_CODE',NULL),(2,'PREFIX_CODE',NULL),(3,'MARITAL_STATUS_CODE',NULL),(4,'CONTACT_TYPE_CODE',NULL),(5,'ADDRESS_TYPE_CODE',NULL),(6,'PERSON_TYPE_CODE',NULL),(7,'ROLE_TYPE_CODE',NULL),(8,'ATTACH_CAT_CODE',NULL),(9,'ATTACH_TYPE_CODE',NULL),(10,'USER_STATUS_CODE',NULL),(11,'UNIVERSITY_TYPE_CODE',NULL),(12,'PROGRAM_CATEGORY_CODE',NULL),(13,'PROGRAM_TYPE_CODE',NULL),(14,'PROGRAM_CLASSIFICATION_CODE',NULL),(15,'PROGRAM_MAJOR',NULL),(16,'PROGRAM_UNIT_TYPE_CODE',NULL),(17,'EDUCATION_DRIVE_TYPE_CODE',NULL),(18,'EDUCATION_DRIVE_STATUS_CODE',NULL),(19,'EDUCATION_EVENT_TYPE_CODE',NULL),(20,'EDUCATION_EVENT_STATUS_CODE',NULL),(21,'STUDENT_STATUS_CODE',NULL),(22,'OFFER_STATUS_CODE',NULL),(23,'COMPANY_TYPE_CODE',NULL),(24,'INDUSTRY_TYPE_CODE',NULL),(25,'EMPLOYER_DRIVE_TYPE_CODE',NULL),(26,'EMPLOYER_DRIVE_STATUS_CODE',NULL),(27,'EMPLOYER_EVENT_TYPE_CODE',NULL),(28,'EMPLOYER_EVENT_STATUS_CODE',NULL),(29,'JOB_ROLE_TYPE_CODE',NULL),(30,'COMP_APPROVAL_STATUS_CODE',NULL),(31,'COMP_PACKAGE_TYPE_CODE',NULL),(32,'COMPANY_SIZE_CODE',NULL),(33,'SKILL_TYPE_CODE',NULL),(34,'INTEREST_TYPE_CODE',NULL),(35,'LANGUAGE_TYPE_CODE',NULL),(36,'LOGIN_TYPE',NULL),(37,'COMPANY_STATUS_CODE',NULL),(38,'CAMPUS_STATUS_CODE',NULL),(39,'CAMPUS_UPLOAD_TYPE',NULL),(40,'COMPANY_UPLOAD_TYPE',NULL),(41,'CANDIDATE_STATUS_TYPE',NULL),(42,'DURATION_TYPE',NULL),(43,'INSTITUTE_TIER',NULL),(44,'REGION_FLAG',NULL);
/*!40000 ALTER TABLE `LOOKUP_TYPE` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `LOOKUP_VALUE`
--

DROP TABLE IF EXISTS `LOOKUP_VALUE`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `LOOKUP_VALUE` (
  `lookup_value_id` int(15) NOT NULL AUTO_INCREMENT,
  `lookup_type_id` int(15) NOT NULL,
  `lookup_value` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`lookup_value_id`),
  KEY `lookup_type_id` (`lookup_type_id`),
  CONSTRAINT `LOOKUP_VALUE_ibfk_1` FOREIGN KEY (`lookup_type_id`) REFERENCES `LOOKUP_TYPE` (`lookup_type_id`)
) ENGINE=InnoDB AUTO_INCREMENT=515 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `LOOKUP_VALUE`
--

LOCK TABLES `LOOKUP_VALUE` WRITE;
/*!40000 ALTER TABLE `LOOKUP_VALUE` DISABLE KEYS */;
INSERT INTO `LOOKUP_VALUE` VALUES (1,1,'Male'),(2,1,'Female'),(3,1,'Unknown'),(4,2,'Mr.'),(5,2,'Miss.'),(6,2,'Mrs.'),(7,2,'Ms.'),(8,2,'Dr.'),(9,2,'Prof.'),(10,3,'Married'),(11,3,'Single'),(12,3,'Undisclosed'),(13,4,'Email'),(14,4,'Mobile'),(15,4,'Phone'),(18,5,'Residential'),(19,5,'Permanent'),(20,5,'Office'),(21,5,'Communication'),(22,6,'Full Time Employee'),(23,6,'Part Time Employee'),(24,6,'Intern'),(25,6,'Contractor'),(26,7,'Employer'),(27,7,'Campus'),(28,7,'Student'),(29,7,'System Admin'),(30,8,'About Myself'),(31,8,'Twitter'),(32,8,'Hobbies'),(33,8,'Project'),(34,8,'Certificate'),(35,8,'Award'),(36,8,'Reference'),(37,8,'Profile Picture'),(38,8,'Facebook Link'),(39,8,'LinkedIn Link'),(40,8,'White paper'),(41,8,'Resume'),(42,8,'GitHub'),(43,9,'Video'),(44,9,'Picture'),(45,9,'Url'),(46,9,'Document'),(47,10,'Pending Confirmation'),(48,10,'Confirmed'),(49,10,'In Active'),(50,11,'Autonomus'),(51,11,'Deemed'),(52,11,'Public'),(53,11,'Private'),(54,11,'Unitary'),(55,12,'Full Time'),(56,12,'Part Time'),(57,12,'Correspondence'),(58,13,'Diploma/Associate\'s Degree'),(59,13,'Bachelor\'s Degree'),(60,13,'Master\'s Degree'),(61,13,'Doctoral Degree'),(62,13,'Professional Certification'),(63,14,'Architecture & Planning'),(65,14,'Cultural & Social'),(66,14,'Education'),(67,14,'Engineering'),(68,14,'Fine Arts'),(69,14,'Humanities'),(70,14,'Law'),(71,14,'Medicine'),(72,14,'Management'),(73,14,'Pharmacy'),(74,14,'Science'),(75,14,'Social & Behaviour Science'),(76,15,'Accounting & Finance'),(77,15,'Aerospace Studies'),(78,15,'Agricultural Engineering'),(79,15,'Anesthesiology'),(80,15,'Anthropology'),(81,15,'Architectual Engineering'),(82,15,'Automotive Engineering'),(83,15,'Dentistry'),(84,15,'Asian Studies'),(85,15,'Atmospheric Science'),(86,15,'Bio Chemistry'),(87,15,'Bio Engeering'),(88,15,'Biomechanical Engineering'),(89,15,'Bio Medical Engineering'),(90,15,'Biology'),(91,15,'Business Management'),(92,15,'Chemical Engineering'),(93,15,'Chemistry'),(94,15,'City & Metropolitan Planning'),(95,15,'Civil & Environmental Engineering'),(96,15,'Computing'),(97,15,'Computer Engineering'),(98,15,'Dance and Modern Arts'),(99,15,'Dermatology'),(100,15,'Economics'),(101,15,'Education, Culture & Society'),(102,15,'Education Leadership Policy'),(103,15,'Educational Psycology'),(104,15,'Electrical Engineering'),(105,15,'Electronics Engineering'),(106,15,'English'),(107,15,'Enterpreneurship & Strategy'),(108,15,'Environmental Engineering'),(109,15,'Ethnic Studies'),(110,15,'Exercise & Sports Science'),(111,15,'Family & Consumer Studies'),(112,15,'Family & Preventive Medicine'),(113,15,'Film & Media Arts'),(114,15,'Insurance & Risk Management'),(115,15,'Gender Studies'),(116,15,'Geography'),(117,15,'Geology & Geophysics'),(118,15,'Geological Engineering'),(119,15,'Geotechnical Engineering'),(120,15,'Industrial Engineering'),(121,15,'Information Technology & Computer Science'),(122,15,'Kinesilogy'),(123,15,'Marine Engineering'),(124,15,'Mathematics'),(125,15,'Latin American Studies'),(126,15,'Linguistics'),(127,15,'Management'),(128,15,'Manufacturing Engineering'),(129,15,'Marketing & Operations'),(130,15,'Material Science & Engineering'),(131,15,'Medical Laboratory Science'),(132,15,'Medicinal Chemistry'),(133,15,'Metallurgical Engineering'),(134,15,'Mechanical Engineering'),(135,15,'Military Science'),(136,15,'Multi-Disciplinary Design'),(137,15,'Music'),(138,15,'Nanotechnology Engineering'),(139,15,'Naval Science'),(140,15,'NeuroBiology & Anatomy'),(141,15,'Neurology'),(142,15,'Neurosurgery'),(143,15,'Nuclear Engineering'),(144,15,'Nursing'),(145,15,'Nutrition and Integrative Physiology'),(146,15,'Obstetrics & Gynecology'),(147,15,'Occupational Theory'),(148,15,'Oncology Science'),(149,15,'Operations & Inforamation Systems'),(150,15,'Opthalmology'),(151,15,'Orthopaedics'),(152,15,'Parks, Recreation & Tourism'),(153,15,'Pathology'),(154,15,'Pediatrics'),(155,15,'Petrolium Engineering'),(156,15,'Pharmacology & Toxicology'),(157,15,'Pharmacotherapy'),(158,15,'Philosophy'),(159,15,'Physical Medicine & Rehabilitation'),(160,15,'Physical Therapy'),(161,15,'Physics & Astronomy'),(162,15,'Physiology'),(163,15,'Political Science'),(164,15,'Psychiatry'),(165,15,'Psycology'),(166,15,'Radiation & Oncology'),(167,15,'Radiology'),(168,15,'Social Science'),(169,15,'Sociology'),(170,15,'Special Education'),(171,15,'Structural Engineering'),(172,15,'Surgery'),(173,15,'Theater'),(174,16,'Year I,Semister I'),(175,16,'Year I,Semister II'),(176,16,'Year II,Semister I'),(177,16,'Year II,Semister II'),(178,16,'Year III,Semister I'),(179,16,'Year III,Semister II'),(180,16,'Year IV,Semister I'),(181,16,'Year IV,Semister II'),(182,16,'Year V,Semister I'),(183,16,'Year V,Semister II'),(184,16,'Year I,Trimester I'),(185,16,'Year I,Trimester II'),(186,16,'Year I,Trimester III'),(187,16,'Year II,Trimester I'),(188,16,'Year II,Trimister II'),(189,16,'Year II,Trimester III'),(190,16,'Year III,Trimester I'),(191,16,'Year III,Trimester II'),(192,16,'Year III,Trimester III'),(193,16,'Year IV,Trimester I'),(194,16,'Year IV,Trimester II'),(195,16,'Year IV,Trimester III'),(196,16,'Year V,Trimester I'),(197,16,'Year V,Trimester II'),(198,16,'Year V,Trimester III'),(199,16,'Year I,Quarter I'),(200,16,'Year I,Quarter II'),(201,16,'Year I,Quarter III'),(202,16,'Year I,Quarter IV'),(203,16,'Year II,Quarter I'),(204,16,'Year II,Quarter II'),(205,16,'Year II,Quarter III'),(206,16,'Year II,Quarter IV'),(207,16,'Year III,Quarter I'),(208,16,'Year III,Quarter II'),(209,16,'Year III,Quarter III'),(210,16,'Year III,Quarter IV'),(211,16,'Year IV,Quarter I'),(212,16,'Year IV,Quarter II'),(213,16,'Year IV,Quarter III'),(214,16,'Year IV,Quarter IV'),(215,16,'Year V,Quarter I'),(216,16,'Year V,Quarter II'),(217,16,'Year V,Quarter III'),(218,16,'Year V,Quarter IV'),(219,16,'Year I'),(220,16,'Year II'),(221,16,'Year III'),(222,16,'Year IV'),(223,16,'Year V'),(224,17,'Placement'),(225,17,'Campaign'),(226,17,'Tech Talk'),(227,17,'Internship'),(228,17,'Summer Program'),(229,17,'Apprentice'),(231,18,'Initial'),(233,18,'In Progress'),(234,18,'Closed'),(235,19,'On Campus'),(236,19,'Off Campus'),(237,19,'Pooled'),(238,20,'Initial'),(239,20,'In Progress'),(240,20,'Shared with Employer'),(241,20,'Published'),(242,20,'Students shortlisted'),(243,20,'Scheduled'),(244,20,'Closed'),(246,21,'Pending Confirmation'),(247,21,'Confirmed'),(248,21,'Inactive'),(249,22,'Offered'),(250,22,'Accepted'),(251,22,'Rejected'),(252,23,'Multinational'),(253,23,'Small/Medium Enterprise'),(254,23,'Startup'),(255,23,'Government'),(256,23,'Privately Held'),(257,23,'Public Sector Unit'),(258,24,'Information Technology'),(259,24,'Manufacturing'),(260,24,'Agriculture'),(261,24,'Healthcare'),(262,24,'Engineering'),(263,24,'Advertising'),(264,24,'Aerospace'),(265,24,'Transportation'),(266,24,'Automobile'),(267,24,'Banking'),(268,24,'Biotechnology'),(269,24,'Chemical'),(270,24,'Communication'),(271,24,'Construction'),(272,24,'Education'),(273,24,'Energy'),(274,24,'Entertainment'),(275,24,'Fashion'),(276,24,'Financial'),(277,24,'Food & Beverage'),(278,24,'Green Technology'),(279,24,'Insurance'),(280,24,'Journalism'),(281,24,'Legal'),(282,24,'Media & Boradcasting'),(283,24,'Medical Devices & Supplies'),(284,24,'Motion Pictures & Video'),(285,24,'Music'),(286,24,'Pharmaceutical'),(287,24,'Public Administration'),(288,24,'Publishing'),(289,24,'Real Estate'),(290,24,'Retail'),(291,24,'Service'),(292,24,'Telecommunications'),(293,24,'Tourism'),(294,24,'Travel'),(295,24,'Utilities'),(296,24,'Web Services'),(297,25,'Hiring'),(298,25,'Social Media Campaign'),(299,25,'Tech Talk'),(300,25,'Internship'),(301,25,'Summer Program'),(302,25,'Contractor'),(303,25,'Brand Development'),(304,25,'Apprentice'),(305,26,'Initial'),(306,26,'In Progress'),(308,26,'Closed'),(309,27,'On Campus'),(310,27,'Off Campus'),(311,27,'Pooled'),(312,28,'Initial'),(314,28,'Published'),(315,28,'Rejected'),(316,28,'Accepted'),(317,28,'Screening'),(318,28,'Students shortlisted'),(319,28,'Scheduled'),(320,28,'In Progress'),(321,28,'Closed'),(323,29,'Technical'),(324,29,'Management'),(325,29,'Sales'),(326,29,'Admin'),(327,29,'Human Resources'),(328,29,'Network Engineer'),(329,29,'Nursing'),(330,29,'Security'),(331,29,'Support'),(332,29,'Facility'),(333,30,'Initial'),(334,30,'Approved'),(335,30,'Rejected'),(336,30,'Inactive'),(337,31,'Salary'),(338,31,'Shares'),(339,31,'Bonus'),(340,31,'Joining Bonus'),(341,31,'Benefits'),(342,31,'Commision'),(343,31,'Stock Options'),(344,32,'1-100'),(345,32,'101-500'),(346,32,'501-2000'),(347,32,'2001-5000'),(348,32,'5001-10000'),(349,32,'10000+'),(350,33,'Java'),(351,33,'C++'),(352,33,'Photoshop'),(353,33,'Java Script'),(354,34,'Mobile Development'),(355,34,'Artificial Intelligence'),(356,34,'Machine Learning'),(357,35,'English'),(358,8,'Youtube'),(359,8,'10th'),(360,8,'12th'),(361,8,'Cover Photo'),(362,36,'Facebook'),(363,37,'Registered'),(364,37,'Inactive'),(365,37,'Unregistered'),(366,37,'Suspended'),(367,38,'Registered'),(368,38,'Inactive'),(369,38,'Unregistered'),(370,38,'Suspended'),(371,39,'Enrollment'),(372,39,'Assessment'),(373,39,'Placement Aggregates'),(374,40,'Test Score'),(375,40,'Hiring Aggregates'),(376,41,'Screening'),(377,41,'Shortlisted'),(378,41,'Rejected'),(379,41,'On Hold'),(380,41,'Interview in progress'),(381,41,'Offered'),(382,41,'Applied'),(383,42,'Month(s)'),(384,15,'Computer Science & Engineering'),(385,15,'Infrastructure and Transportation Engineering'),(386,15,'Organic Chemistry'),(387,15,'Science'),(388,15,'Water Resources and Environmental Engineering'),(390,43,'1'),(391,43,'2'),(392,43,'3'),(393,43,'4'),(394,40,'Event Unregistered Students'),(395,42,'Year(s)'),(396,36,'Google'),(397,36,'LinkedIn'),(398,11,'Affiliating'),(399,11,'State'),(400,11,'Central'),(401,44,'North'),(402,44,'South'),(403,44,'East'),(404,44,'West'),(405,33,'Angular JS'),(406,33,'Hadoop'),(407,33,'HTML & CSS'),(408,33,'Android'),(409,33,'PHP'),(410,33,'MySQL'),(411,33,'Selenium'),(412,33,'Python'),(413,33,'ASP.NET'),(414,33,'Node JS'),(415,33,'React JS'),(416,33,'ORACLE PL/SQL'),(417,33,'Web Services'),(418,33,'Electronics circuit design'),(419,33,'Integrated circuit design'),(420,33,'Mobile Technology'),(421,33,'Microprocessor & Controller design'),(422,33,'CAD'),(423,34,'Data Science'),(424,34,'Big Data'),(425,34,'Research and Design'),(426,34,'Cycling'),(427,34,'Chess'),(428,34,'Trekking'),(429,34,'Squash'),(430,34,'Sailing'),(431,34,'Swimming'),(432,34,'Jogging'),(433,34,'Skating'),(434,34,'Tennis'),(435,34,'Cricket'),(436,34,'Soccer'),(437,34,'Reading'),(438,34,'Gardening'),(439,34,'Sports'),(440,34,'Culinary arts'),(441,34,'Landscaping'),(442,34,'Debates'),(443,34,'Literature'),(444,34,'Philosophy'),(445,34,'Acting/Dram'),(446,34,'Astronomy'),(447,34,'Archery'),(448,34,'Basketball'),(449,34,'Blogging'),(450,34,'Board Games'),(451,34,'Calligraphy'),(452,34,'Services'),(453,34,'Origami'),(454,34,'Crafts'),(455,34,'Crossword'),(456,34,'Music'),(457,34,'Dancing'),(458,34,'Drawing'),(459,34,'Fishing'),(460,34,'Golf'),(461,34,'Hiking'),(462,34,'Hunting'),(463,34,'Martical Arts'),(464,34,'Painting'),(465,34,'Paragliding'),(466,34,'Photography'),(467,34,'Pottery'),(468,34,'Robotics'),(469,34,'Skiing'),(470,34,'Scuba Diving'),(471,34,'Skydiving'),(472,34,'Deep Sea Diving'),(473,34,'Travelling'),(474,33,'Map reading'),(475,33,'Design techniques'),(476,33,'CAD software.'),(477,33,'Civil 3D'),(478,33,'Project Management'),(479,33,'Cost Estimates for Material,Equipment and/or Labor'),(480,33,'Reinforced Concrete and Steel Design'),(481,33,'Legal Guidelines,Health and Safety Requirements'),(482,33,'Insurance and Risk Management'),(483,33,'Insurance Business Environment'),(484,33,'Life and non-Life Insurance'),(485,33,'Management of Insurance Companies'),(486,33,'Marketing Research'),(487,33,'Insurance Marketing'),(488,33,'Corporate Governance and Business Ethics'),(489,33,'Accounting Techniques'),(490,33,'Auditing'),(491,33,'Budgeting'),(492,33,'Business Analytics'),(493,33,'Cash Flow Management'),(494,33,'Cost Analysis'),(495,33,'Cost Reduction'),(496,33,'Data Analysis'),(497,33,'Financial Management'),(498,33,'Financial Modelling'),(499,33,'Interpersonal skills'),(500,33,'Strategic thinking'),(501,33,'Entrepreneurial skills'),(502,33,'Communication skills'),(503,33,'Leadership Skills'),(504,33,'International Business Management'),(505,33,'Current Trends in Management'),(506,33,'Management Accounting'),(507,33,'Financial Accounting'),(508,33,'Accounting Concepts'),(509,33,'Materials Costs'),(510,33,'Labour Costs and Labour Turnover'),(511,33,'Apportionment and Absorption'),(512,33,'Preparation of Cost Sheet'),(513,39,'Event Student Shortlist'),(514,41,'Initial');
/*!40000 ALTER TABLE `LOOKUP_VALUE` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `MESSAGE_DTL`
--

DROP TABLE IF EXISTS `MESSAGE_DTL`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `MESSAGE_DTL` (
  `message_dtl_id` int(15) NOT NULL,
  `message_hdr_id` int(15) NOT NULL,
  `language_value_id` int(15) NOT NULL,
  `message_text` varchar(256) NOT NULL,
  PRIMARY KEY (`message_dtl_id`),
  KEY `MESSAGE_DTL_ibfk_1` (`message_hdr_id`),
  KEY `MESSAGE_DTL_ibfk_2` (`language_value_id`),
  CONSTRAINT `MESSAGE_DTL_ibfk_1` FOREIGN KEY (`message_hdr_id`) REFERENCES `MESSAGE_HDR` (`message_hdr_id`),
  CONSTRAINT `MESSAGE_DTL_ibfk_2` FOREIGN KEY (`language_value_id`) REFERENCES `LOOKUP_VALUE` (`lookup_value_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `MESSAGE_DTL`
--

LOCK TABLES `MESSAGE_DTL` WRITE;
/*!40000 ALTER TABLE `MESSAGE_DTL` DISABLE KEYS */;
/*!40000 ALTER TABLE `MESSAGE_DTL` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `MESSAGE_HDR`
--

DROP TABLE IF EXISTS `MESSAGE_HDR`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `MESSAGE_HDR` (
  `message_hdr_id` int(15) NOT NULL,
  `message_id` int(5) NOT NULL,
  `message_category` int(2) NOT NULL,
  `display_type` int(2) NOT NULL,
  `message_type` varchar(1) NOT NULL,
  `description` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`message_hdr_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `MESSAGE_HDR`
--

LOCK TABLES `MESSAGE_HDR` WRITE;
/*!40000 ALTER TABLE `MESSAGE_HDR` DISABLE KEYS */;
/*!40000 ALTER TABLE `MESSAGE_HDR` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `NOTIFICATION_DETAILS`
--

DROP TABLE IF EXISTS `NOTIFICATION_DETAILS`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `NOTIFICATION_DETAILS` (
  `notification_detail_id` int(15) NOT NULL AUTO_INCREMENT,
  `notification_event_id` int(15) DEFAULT NULL,
  `role_type_value_id` int(15) NOT NULL,
  `parent_id` int(15) DEFAULT NULL,
  `recipient_id` int(15) NOT NULL,
  `notification_dismissed_ind` varchar(1) DEFAULT NULL,
  `create_datetime` datetime DEFAULT NULL,
  `read_datetime` datetime DEFAULT NULL,
  `dismiss_datetime` datetime DEFAULT NULL,
  `notification_read_ind` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`notification_detail_id`),
  KEY `role_type_value_id` (`role_type_value_id`),
  KEY `IDX_NOTIFICATION_DETAILS_1` (`recipient_id`,`notification_dismissed_ind`),
  CONSTRAINT `NOTIFICATION_DETAILS_ibfk_1` FOREIGN KEY (`role_type_value_id`) REFERENCES `LOOKUP_VALUE` (`lookup_value_id`),
  CONSTRAINT `NOTIFICATION_DETAILS_ibfk_2` FOREIGN KEY (`recipient_id`) REFERENCES `SCORA_USER` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `NOTIFICATION_DETAILS`
--

LOCK TABLES `NOTIFICATION_DETAILS` WRITE;
/*!40000 ALTER TABLE `NOTIFICATION_DETAILS` DISABLE KEYS */;
/*!40000 ALTER TABLE `NOTIFICATION_DETAILS` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `NOTIFICATION_EVENTS`
--

DROP TABLE IF EXISTS `NOTIFICATION_EVENTS`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `NOTIFICATION_EVENTS` (
  `notification_event_id` int(15) NOT NULL AUTO_INCREMENT,
  `notification_template_id` int(15) NOT NULL,
  `role_type_value_id` int(15) NOT NULL,
  `parent_id` int(15) DEFAULT NULL,
  `transaction_id` int(15) DEFAULT NULL,
  `sender_id` int(15) NOT NULL,
  `create_datetime` datetime NOT NULL,
  `update_datetime` datetime NOT NULL,
  `create_user_id` int(15) NOT NULL,
  `update_user_id` int(15) NOT NULL,
  PRIMARY KEY (`notification_event_id`),
  KEY `notification_template_id` (`notification_template_id`),
  KEY `role_type_value_id` (`role_type_value_id`),
  KEY `sender_id` (`sender_id`),
  CONSTRAINT `NOTIFICATION_EVENTS_ibfk_1` FOREIGN KEY (`notification_template_id`) REFERENCES `NOTIFICATION_MESSAGE_TEMPLATES` (`notification_template_id`),
  CONSTRAINT `NOTIFICATION_EVENTS_ibfk_2` FOREIGN KEY (`role_type_value_id`) REFERENCES `LOOKUP_VALUE` (`lookup_value_id`),
  CONSTRAINT `NOTIFICATION_EVENTS_ibfk_3` FOREIGN KEY (`sender_id`) REFERENCES `SCORA_USER` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `NOTIFICATION_EVENTS`
--

LOCK TABLES `NOTIFICATION_EVENTS` WRITE;
/*!40000 ALTER TABLE `NOTIFICATION_EVENTS` DISABLE KEYS */;
/*!40000 ALTER TABLE `NOTIFICATION_EVENTS` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `NOTIFICATION_MESSAGE_TEMPLATES`
--

DROP TABLE IF EXISTS `NOTIFICATION_MESSAGE_TEMPLATES`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `NOTIFICATION_MESSAGE_TEMPLATES` (
  `notification_template_id` int(15) NOT NULL AUTO_INCREMENT,
  `notification_name` varchar(100) NOT NULL,
  `language` varchar(16) NOT NULL,
  `broadcast_flag` varchar(4) NOT NULL,
  `receiver_role_code` varchar(16) DEFAULT NULL,
  `message_text` varchar(2000) NOT NULL,
  `create_datetime` datetime NOT NULL,
  `update_datetime` datetime NOT NULL,
  `create_user_id` int(15) NOT NULL,
  `update_user_id` int(15) NOT NULL,
  PRIMARY KEY (`notification_template_id`),
  KEY `receiver_role_code` (`receiver_role_code`),
  KEY `IDX_NOTIFICATION_MESSAGE_TEMPLATES_01` (`notification_name`),
  CONSTRAINT `NOTIFICATION_MESSAGE_TEMPLATES_ibfk_1` FOREIGN KEY (`receiver_role_code`) REFERENCES `SCORA_ROLE` (`role_code`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `NOTIFICATION_MESSAGE_TEMPLATES`
--

LOCK TABLES `NOTIFICATION_MESSAGE_TEMPLATES` WRITE;
/*!40000 ALTER TABLE `NOTIFICATION_MESSAGE_TEMPLATES` DISABLE KEYS */;
INSERT INTO `NOTIFICATION_MESSAGE_TEMPLATES` VALUES (1,'Employer Event Request','English','GRP','PLCDIR','<company_name> is interested in conducting a campus drive at your campus. Please view <company_event_name> and take appropriate action.','2017-10-31 00:00:00','2017-10-31 00:00:00',1,1),(2,'Institute Declined Event','English','GRP','RECDIR','Your event <company_event_name> has been rejected by <institute_name>.','2017-10-31 00:00:00','2017-10-31 00:00:00',1,1),(3,'Institute Accepted Event','English','GRP','RECDIR','Your event <company_event_name> has been accepted by <institute_name>.','2017-10-31 00:00:00','2017-10-31 00:00:00',1,1),(4,'Subscribed to Event','English','IND','STUDENT','You are subscribed to the event <campus_event_name> for <company_name>  <organization_name>','2017-10-31 00:00:00','2017-10-31 00:00:00',1,1),(5,'Student List Shared','English','GRP','RECDIR','<institute_name> has shared the student list for your event <company_event_name>','2017-10-31 00:00:00','2017-10-31 00:00:00',1,1),(6,'Student Shortlist Shared','English','GRP','PLCDIR','<company_name> has shared the shortlisted students for your event <campus_event_name>','2017-10-31 00:00:00','2017-10-31 00:00:00',1,1),(7,'Institute Scheduled Event','English','GRP','RECDIR','<institute_name> has scheduled your event <company_event_name> on <scheduled_date> at <scheduled_time>.','2017-10-31 00:00:00','2017-10-31 00:00:00',1,1),(8,'Event Scheduled','English','IND','STUDENT','<campus_event_name> for <company_name> has been scheduled for <scheduled_date> at <scheduled_time>.','2017-10-31 00:00:00','2017-10-31 00:00:00',1,1),(9,'Student Subscription Changed','English','GRP','PLCDIR','One or more students have modified their subscription status on the event <campus_event_name>. Please review the updated list.','2017-10-31 00:00:00','2017-10-31 00:00:00',1,1),(10,'Student Subscription Changed','English','GRP','RECDIR','One or more students have modified their subscription status on the event <company_event_name>. Please review the updated list.','2017-10-31 00:00:00','2017-10-31 00:00:00',1,1),(11,'Event status changed by Institute','English','IND','STUDENT','Your status on the event <campus_event_name> has been changed by the placement director. Please check the event for detail.','2017-10-31 00:00:00','2017-10-31 00:00:00',1,1),(12,'Event status changed by Employer','English','IND','STUDENT','Your status on the event <campus_event_name> has been changed by the recruitment director. Please check the event for detail.','2017-10-31 00:00:00','2017-10-31 00:00:00',1,1),(13,'Hired by Employer','English','IND','STUDENT','Congratulations! You have been offered by <company_name>. Please review the offer.','2017-10-31 00:00:00','2017-10-31 00:00:00',1,1),(14,'Selected by Employer','English','IND','STUDENT','Congratulations! You have been Selected by <company_name> for <event type>','2017-10-31 00:00:00','2017-10-31 00:00:00',1,1),(15,'Student List Reminder','English','GRP','PLCDIR','Reminder: <company_name> has requested to share the student list for your event <campus_event_name>.','2017-10-31 00:00:00','2017-10-31 00:00:00',1,1),(16,'Schedule Reminder','English','GRP','PLCDIR','Reminder: <company_name> has requested to schedule the event <campus_event_name>.','2017-10-31 00:00:00','2017-10-31 00:00:00',1,1),(17,'Student Shortlist Reminder','English','GRP','RECDIR','Reminder: <institute_name> has requested to share the shortlisted students for the event <company_event_name>.','2017-10-31 00:00:00','2017-10-31 00:00:00',1,1),(18,'Institute Connect Request','English','GRP','RECDIR','<institute_name> would like to connect with you. Please review <institute_name> profie.','2017-10-31 00:00:00','2017-10-31 00:00:00',1,1),(19,'Employer Connect Request','English','GRP','PLCDIR','Message From <company_name> <input_messages>','2017-10-31 00:00:00','2017-10-31 00:00:00',1,1),(20,'Employer Event Request Reminder','English','GRP','PLCDIR','Reminder: <company_name> is interested in conducting a campus drive at your campus. Please view <company_event_name> and take appropriate action.','2017-10-31 00:00:00','2017-10-31 00:00:00',1,1);
/*!40000 ALTER TABLE `NOTIFICATION_MESSAGE_TEMPLATES` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Temporary view structure for view `OFF_CAMPUS_STUDENT_SEARCH_VW`
--

DROP TABLE IF EXISTS `OFF_CAMPUS_STUDENT_SEARCH_VW`;
/*!50001 DROP VIEW IF EXISTS `OFF_CAMPUS_STUDENT_SEARCH_VW`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `OFF_CAMPUS_STUDENT_SEARCH_VW` AS SELECT 
 1 AS `first_name`,
 1 AS `last_name`,
 1 AS `expected_salary`,
 1 AS `university_name`,
 1 AS `university_id`,
 1 AS `campus_name`,
 1 AS `campus_id`,
 1 AS `program_name`,
 1 AS `highlights`,
 1 AS `student_id`,
 1 AS `program_id`,
 1 AS `program_type_value_id`,
 1 AS `program_class_value_id`,
 1 AS `program_cat_value_id`,
 1 AS `program_major_value_id`,
 1 AS `city_name`,
 1 AS `city_id`,
 1 AS `state_code`,
 1 AS `state_name`,
 1 AS `region`,
 1 AS `cgpa_score`,
 1 AS `planed_completion_date`,
 1 AS `actual_completion_date`*/;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `ORGANIZATION`
--

DROP TABLE IF EXISTS `ORGANIZATION`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ORGANIZATION` (
  `organization_id` int(15) NOT NULL AUTO_INCREMENT,
  `company_id` int(15) NOT NULL,
  `short_name` varchar(50) DEFAULT NULL,
  `name` varchar(100) DEFAULT NULL,
  `description` varchar(2000) DEFAULT NULL,
  `create_datetime` datetime NOT NULL,
  `update_datetime` datetime NOT NULL,
  `create_user_id` int(15) NOT NULL,
  `update_user_id` int(15) NOT NULL,
  `facebook` varchar(200) DEFAULT NULL,
  `linkedin` varchar(200) DEFAULT NULL,
  `twitter` varchar(200) DEFAULT NULL,
  `youtube` varchar(200) DEFAULT NULL,
  `branding_image` varchar(200) DEFAULT NULL,
  `logo` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`organization_id`),
  KEY `IDX_FK_ORG` (`company_id`),
  CONSTRAINT `ORGANIZATION_ibfk_1` FOREIGN KEY (`company_id`) REFERENCES `COMPANY` (`company_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ORGANIZATION`
--

LOCK TABLES `ORGANIZATION` WRITE;
/*!40000 ALTER TABLE `ORGANIZATION` DISABLE KEYS */;
INSERT INTO `ORGANIZATION` VALUES (1,1,'R&D','Research and Development',NULL,'2018-04-04 10:59:05','2018-04-04 10:59:05',13162,13162,NULL,NULL,NULL,NULL,NULL,NULL),(2,1,'Sales','Sales and Marketing',NULL,'2018-04-04 11:40:33','2018-04-04 11:40:33',13162,13162,NULL,NULL,NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `ORGANIZATION` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ORGANIZATION_ADDRESS`
--

DROP TABLE IF EXISTS `ORGANIZATION_ADDRESS`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ORGANIZATION_ADDRESS` (
  `organization_id` int(15) NOT NULL,
  `company_id` int(15) NOT NULL,
  `address_id` int(15) NOT NULL,
  `primary_ind` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`organization_id`,`address_id`),
  KEY `company_id` (`company_id`),
  KEY `address_id` (`address_id`),
  KEY `IDX_ORGANIZATION_ADDRESS_1` (`company_id`,`organization_id`),
  CONSTRAINT `ORGANIZATION_ADDRESS_ibfk_1` FOREIGN KEY (`organization_id`) REFERENCES `ORGANIZATION` (`organization_id`),
  CONSTRAINT `ORGANIZATION_ADDRESS_ibfk_2` FOREIGN KEY (`company_id`) REFERENCES `COMPANY` (`company_id`),
  CONSTRAINT `ORGANIZATION_ADDRESS_ibfk_3` FOREIGN KEY (`address_id`) REFERENCES `ADDRESS` (`address_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ORGANIZATION_ADDRESS`
--

LOCK TABLES `ORGANIZATION_ADDRESS` WRITE;
/*!40000 ALTER TABLE `ORGANIZATION_ADDRESS` DISABLE KEYS */;
/*!40000 ALTER TABLE `ORGANIZATION_ADDRESS` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ORGANIZATION_CONTACT`
--

DROP TABLE IF EXISTS `ORGANIZATION_CONTACT`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ORGANIZATION_CONTACT` (
  `organization_id` int(15) NOT NULL,
  `contact_id` int(15) NOT NULL,
  `company_id` int(15) NOT NULL,
  `primary_ind` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`organization_id`,`contact_id`),
  KEY `contact_id` (`contact_id`),
  KEY `IDX_ORGANIZATION_CONTACT_1` (`company_id`,`organization_id`),
  CONSTRAINT `ORGANIZATION_CONTACT_ibfk_1` FOREIGN KEY (`organization_id`) REFERENCES `ORGANIZATION` (`organization_id`),
  CONSTRAINT `ORGANIZATION_CONTACT_ibfk_2` FOREIGN KEY (`company_id`) REFERENCES `COMPANY` (`company_id`),
  CONSTRAINT `ORGANIZATION_CONTACT_ibfk_3` FOREIGN KEY (`contact_id`) REFERENCES `CONTACT` (`contact_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ORGANIZATION_CONTACT`
--

LOCK TABLES `ORGANIZATION_CONTACT` WRITE;
/*!40000 ALTER TABLE `ORGANIZATION_CONTACT` DISABLE KEYS */;
/*!40000 ALTER TABLE `ORGANIZATION_CONTACT` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `POSTAL_CODE`
--

DROP TABLE IF EXISTS `POSTAL_CODE`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `POSTAL_CODE` (
  `postal_id` int(15) NOT NULL AUTO_INCREMENT,
  `postal_code` varchar(16) NOT NULL,
  `city_id` int(15) DEFAULT NULL,
  `country_code` varchar(16) DEFAULT NULL,
  PRIMARY KEY (`postal_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `POSTAL_CODE`
--

LOCK TABLES `POSTAL_CODE` WRITE;
/*!40000 ALTER TABLE `POSTAL_CODE` DISABLE KEYS */;
/*!40000 ALTER TABLE `POSTAL_CODE` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `PROGRAM`
--

DROP TABLE IF EXISTS `PROGRAM`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `PROGRAM` (
  `program_id` int(15) NOT NULL AUTO_INCREMENT,
  `campus_id` int(15) NOT NULL,
  `department_id` int(15) NOT NULL,
  `program_name` varchar(50) NOT NULL,
  `description` varchar(2000) DEFAULT NULL,
  `program_type_value_id` int(15) NOT NULL,
  `program_class_value_id` int(15) NOT NULL,
  `program_cat_value_id` int(15) NOT NULL,
  `number_of_students` int(15) DEFAULT NULL,
  `start_date` date DEFAULT NULL,
  `create_datetime` datetime NOT NULL,
  `update_datetime` datetime NOT NULL,
  `create_user_id` int(15) NOT NULL,
  `update_user_id` int(15) NOT NULL,
  `program_major_value_id` int(15) NOT NULL,
  `off_campus_ind` varchar(1) DEFAULT NULL,
  `program_duration` int(2) DEFAULT NULL,
  PRIMARY KEY (`program_id`),
  KEY `department_id` (`department_id`),
  KEY `IDX_FK_PROGRAM_PNA` (`program_name`),
  KEY `IDX_FK_PROGRAM_CAM` (`campus_id`),
  KEY `program_type_value_id` (`program_type_value_id`),
  KEY `program_class_value_id` (`program_class_value_id`),
  KEY `program_cat_value_id` (`program_cat_value_id`),
  CONSTRAINT `PROGRAM_ibfk_1` FOREIGN KEY (`campus_id`) REFERENCES `CAMPUS` (`campus_id`),
  CONSTRAINT `PROGRAM_ibfk_3` FOREIGN KEY (`program_type_value_id`) REFERENCES `LOOKUP_VALUE` (`lookup_value_id`),
  CONSTRAINT `PROGRAM_ibfk_4` FOREIGN KEY (`program_class_value_id`) REFERENCES `LOOKUP_VALUE` (`lookup_value_id`),
  CONSTRAINT `PROGRAM_ibfk_5` FOREIGN KEY (`program_cat_value_id`) REFERENCES `LOOKUP_VALUE` (`lookup_value_id`),
  CONSTRAINT `PROGRAM_ibfk_6` FOREIGN KEY (`department_id`) REFERENCES `DEPARTMENT` (`department_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `PROGRAM`
--

LOCK TABLES `PROGRAM` WRITE;
/*!40000 ALTER TABLE `PROGRAM` DISABLE KEYS */;
/*!40000 ALTER TABLE `PROGRAM` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `PROGRAM_CONTACT`
--

DROP TABLE IF EXISTS `PROGRAM_CONTACT`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `PROGRAM_CONTACT` (
  `program_id` int(15) NOT NULL,
  `contact_id` int(15) NOT NULL,
  `campus_id` int(15) NOT NULL,
  `primary_ind` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`program_id`,`contact_id`),
  KEY `contact_id` (`contact_id`),
  KEY `IDX_FK_PROG_CONT` (`campus_id`),
  CONSTRAINT `PROGRAM_CONTACT_ibfk_1` FOREIGN KEY (`program_id`) REFERENCES `PROGRAM` (`program_id`),
  CONSTRAINT `PROGRAM_CONTACT_ibfk_2` FOREIGN KEY (`contact_id`) REFERENCES `CONTACT` (`contact_id`),
  CONSTRAINT `PROGRAM_CONTACT_ibfk_3` FOREIGN KEY (`campus_id`) REFERENCES `CAMPUS` (`campus_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `PROGRAM_CONTACT`
--

LOCK TABLES `PROGRAM_CONTACT` WRITE;
/*!40000 ALTER TABLE `PROGRAM_CONTACT` DISABLE KEYS */;
/*!40000 ALTER TABLE `PROGRAM_CONTACT` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `SCORA_ACCESS_LEVEL`
--

DROP TABLE IF EXISTS `SCORA_ACCESS_LEVEL`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `SCORA_ACCESS_LEVEL` (
  `access_level` int(15) NOT NULL AUTO_INCREMENT,
  `description` varchar(100) NOT NULL,
  PRIMARY KEY (`access_level`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `SCORA_ACCESS_LEVEL`
--

LOCK TABLES `SCORA_ACCESS_LEVEL` WRITE;
/*!40000 ALTER TABLE `SCORA_ACCESS_LEVEL` DISABLE KEYS */;
INSERT INTO `SCORA_ACCESS_LEVEL` VALUES (0,'Admin'),(1,'Campus'),(2,'Company'),(3,'Student'),(4,'Campus and Student'),(5,'Company and Campus'),(6,'Company and Student'),(7,'All');
/*!40000 ALTER TABLE `SCORA_ACCESS_LEVEL` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `SCORA_ROLE`
--

DROP TABLE IF EXISTS `SCORA_ROLE`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `SCORA_ROLE` (
  `role_code` varchar(16) NOT NULL,
  `role_type_value_id` int(15) NOT NULL,
  `description` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`role_code`),
  KEY `role_type_value_id` (`role_type_value_id`),
  CONSTRAINT `SCORA_ROLE_ibfk_1` FOREIGN KEY (`role_type_value_id`) REFERENCES `LOOKUP_VALUE` (`lookup_value_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `SCORA_ROLE`
--

LOCK TABLES `SCORA_ROLE` WRITE;
/*!40000 ALTER TABLE `SCORA_ROLE` DISABLE KEYS */;
INSERT INTO `SCORA_ROLE` VALUES ('PLCDIR',27,'Placement Director'),('RECDIR',26,'Recruitment Director'),('STUDENT',28,'Student'),('SYSADMIN',29,'Scora System Admin');
/*!40000 ALTER TABLE `SCORA_ROLE` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `SCORA_ROLE_ACCESS`
--

DROP TABLE IF EXISTS `SCORA_ROLE_ACCESS`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `SCORA_ROLE_ACCESS` (
  `role_access_id` int(15) NOT NULL AUTO_INCREMENT,
  `role_code` varchar(16) NOT NULL,
  `access_level` int(15) DEFAULT NULL,
  `description` varchar(100) NOT NULL,
  PRIMARY KEY (`role_access_id`),
  KEY `role_code` (`role_code`),
  KEY `access_level` (`access_level`),
  CONSTRAINT `SCORA_ROLE_ACCESS_ibfk_1` FOREIGN KEY (`role_code`) REFERENCES `SCORA_ROLE` (`role_code`),
  CONSTRAINT `SCORA_ROLE_ACCESS_ibfk_2` FOREIGN KEY (`access_level`) REFERENCES `SCORA_ACCESS_LEVEL` (`access_level`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `SCORA_ROLE_ACCESS`
--

LOCK TABLES `SCORA_ROLE_ACCESS` WRITE;
/*!40000 ALTER TABLE `SCORA_ROLE_ACCESS` DISABLE KEYS */;
INSERT INTO `SCORA_ROLE_ACCESS` VALUES (1,'PLCDIR',1,'Only Campus'),(2,'RECDIR',2,'Only Company'),(3,'STUDENT',3,'Only Student'),(4,'PLCDIR',4,'Campus And Student'),(5,'STUDENT',4,'Campus And Student'),(6,'PLCDIR',7,'All'),(7,'RECDIR',7,'All'),(8,'STUDENT',7,'All'),(9,'PLCDIR',5,'Company And Campus'),(10,'RECDIR',5,'Company And Campus'),(11,'RECDIR',6,'Company And Student'),(12,'STUDENT',6,'Company And Student'),(13,'SYSADMIN',0,'System Admin');
/*!40000 ALTER TABLE `SCORA_ROLE_ACCESS` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `SCORA_SERVICES`
--

DROP TABLE IF EXISTS `SCORA_SERVICES`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `SCORA_SERVICES` (
  `service_id` int(15) NOT NULL AUTO_INCREMENT,
  `service_name` varchar(200) NOT NULL,
  `description` varchar(100) DEFAULT NULL,
  `method_name` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`service_id`)
) ENGINE=InnoDB AUTO_INCREMENT=225 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `SCORA_SERVICES`
--

LOCK TABLES `SCORA_SERVICES` WRITE;
/*!40000 ALTER TABLE `SCORA_SERVICES` DISABLE KEYS */;
INSERT INTO `SCORA_SERVICES` VALUES (1,'/api/Addresses','','POST'),(3,'/api/Addresses/deleteCompanyAddress','','DELETE'),(5,'/api/Addresses/getCompanyAddress','','GET'),(6,'/api/Addresses/updateAddress','','PUT'),(7,'/api/Assessments','','POST'),(8,'/api/Assessments/getAssessmentDet','','GET'),(9,'/api/Assessments/updateAssessmentDet','','PUT'),(10,'/api/CampusAddresses/createCampusAddress','','POST'),(11,'/api/CampusAddresses/getCampusAddress','','GET'),(12,'/api/CampusAddresses/updateCampusAddress','','PUT'),(13,'/api/CampusContacts/createCampusContact','','POST'),(14,'/api/CampusContacts/deleteCampusContact','','DELETE'),(15,'/api/CampusContacts/getCampusContact','','GET'),(16,'/api/CampusContacts/updateCampusContact','','PUT'),(17,'/api/CampusDrives','','POST'),(18,'/api/CampusDrives/getAllDriveDetails','','GET'),(19,'/api/CampusDrives/getAllDriveDetailsCampusDashbord','','GET'),(20,'/api/CampusDrives/getCampusDriveDetails','','GET'),(21,'/api/CampusDrives/updateCampusDrive','','PUT'),(22,'/api/Campuses','','POST'),(23,'/api/Campuses/getCampus','','GET'),(24,'/api/Campuses/getCampusDashboard','','POST'),(25,'/api/Campuses/getCampusGraphDataForDashboard','','GET'),(26,'/api/Campuses/getCampusHiringData','','GET'),(27,'/api/Campuses/getCampusProfile','','GET'),(28,'/api/Campuses/getDepartmentData','','POST'),(29,'/api/Campuses/getGraphAndTableData','','GET'),(30,'/api/Campuses/getRecentPlacements','','GET'),(31,'/api/Campuses/upCommingEvents','','GET'),(32,'/api/Campuses/updateCampus','','PUT'),(34,'/api/CampusEvents','','POST'),(35,'/api/CampusEvents/campusBussinessLogic','','PUT'),(36,'/api/CampusEvents/createCampusEvent','','POST'),(37,'/api/CampusEvents/getAllCampusEvents','','GET'),(38,'/api/CampusEvents/getAllEventsOfMyCampus','','GET'),(39,'/api/CampusEvents/getCampusUnregisteredEvents','','GET'),(40,'/api/CampusEvents/updateCampusEvent','','PUT'),(41,'/api/CampusEventStudentSearchVws/campusStudentSearchVw','','GET'),(42,'/api/CampusEventStudentSearchVws/getSkills','','GET'),(43,'/api/CampusPlacementAggregatesWorks/campusPlacementUpload','','POST'),(44,'/api/CampusSearchVws/searchCampus','','GET'),(45,'/api/CampusUploadLogs/getAllUploads','','GET'),(46,'/api/Companies','','POST'),(47,'/api/Companies/companyProfile','','GET'),(48,'/api/Companies/driveDetails','','GET'),(49,'/api/Companies/getCampusHiringData','','GET'),(50,'/api/Companies/getCompany','','GET'),(51,'/api/Companies/getEventStudentList','','GET'),(52,'/api/Companies/getJobDetailsOfCompany','','GET'),(53,'/api/Companies/getRecentRecruitment','','GET'),(54,'/api/Companies/totalDriveDetails','','GET'),(55,'/api/Companies/upCommingEvents','','GET'),(56,'/api/Companies/updateCompany','','PUT'),(57,'/api/CompanyAddresses/createCompanyAddress','','POST'),(58,'/api/CompanyAddresses/updateCompanyAddress','','PUT'),(59,'/api/CompanyContacts/createCompanyContact','','POST'),(60,'/api/CompanyContacts/updateCompanyContact','','PUT'),(61,'/api/CompanyHiringAggregates/companyHiringMassUpload','','POST'),(62,'/api/CompanySearchVws/searchCompany','','GET'),(63,'/api/CompanyUploadLogs/getAllUploads','','GET'),(64,'/api/CompensationPackageItems','','DELETE'),(65,'/api/CompensationPackageItems','','POST'),(66,'/api/CompensationPackageItems/getCompensationPkgItemDetails','','GET'),(67,'/api/CompensationPackages/CreateCompleteCompensationPackage','','POST'),(68,'/api/CompensationPackages/getAllCompensation','','GET'),(69,'/api/CompensationPackages/getCompensationDetails','','GET'),(70,'/api/CompensationPackages/getCompensationPkgDetails','','GET'),(71,'/api/CompensationPackages/updateComp','','PUT'),(72,'/api/CompensationPackages/updateCompensationPackage','','PUT'),(73,'/api/Contacts','','POST'),(74,'/api/Contacts/deleteCompanyContact','','DELETE'),(75,'/api/Contacts/getCompanyContactDet','','GET'),(76,'/api/Contacts/updateContactService','','PUT'),(77,'/api/DepartmentAddresses/createDepartmentAddress','','POST'),(78,'/api/DepartmentAddresses/deleteCampusDeptAddress','','DELETE'),(79,'/api/DepartmentAddresses/getCampusDeptAddress','','GET'),(80,'/api/DepartmentAddresses/updateDepartmentAddress','','PUT'),(81,'/api/DepartmentContacts/createDepartmentContact','','POST'),(82,'/api/DepartmentContacts/deleteCampusDeptContact','','DELETE'),(83,'/api/DepartmentContacts/getCampusDeptContact','','GET'),(84,'/api/DepartmentContacts/updateDepartmentContact','','PUT'),(85,'/api/Departments','','POST'),(87,'/api/Departments/getDepartmentDetails','','GET'),(88,'/api/Departments/updateDepartment','','PUT'),(89,'/api/EducationPeople','','POST'),(90,'/api/EducationPeople/getEducationPerson','','GET'),(91,'/api/EducationPeople/updateEducationPerson','','PUT'),(92,'/api/EducationPersonAddresses/createEducationPersonAddress','','POST'),(93,'/api/EducationPersonAddresses/deleteEducationPersonAddress','','DELETE'),(94,'/api/EducationPersonAddresses/getEducationPersonAddress','','GET'),(95,'/api/EducationPersonAddresses/updateEducationPersonAddress','','PUT'),(96,'/api/EducationPersonContacts/createEducationContact','','POST'),(97,'/api/EducationPersonContacts/deleteEducationPersonContact','','DELETE'),(98,'/api/EducationPersonContacts/getEducationPersonContact','','GET'),(99,'/api/EducationPersonContacts/updateEducationPersonContact','','PUT'),(100,'/api/EmployerCampusListCompPkgs','','DELETE'),(101,'/api/EmployerCampusListCompPkgs','','POST'),(102,'/api/EmployerCampusListCompPkgs/getEmployerCampusListCompPkg','','GET'),(103,'/api/EmployerCampusListDtls','','POST'),(104,'/api/EmployerCampusListDtls/getCampusByEvent','','POST'),(105,'/api/EmployerCampusListDtls/getEmployerCampusList','','GET'),(106,'/api/EmployerCampusListDtls/getEmployerCampusListDtl','','GET'),(107,'/api/EmployerCampusListDtls/getEmployerList','','POST'),(108,'/api/EmployerCampusListHdrs/createCompleteEmployerCampusList','','POST'),(109,'/api/EmployerCampusListHdrs/getCampusListWithCompany','','GET'),(110,'/api/EmployerCampusListHdrs/getEmployerCampusListHdrDetails','','GET'),(111,'/api/EmployerCampusListHdrs/updateEmployerCampusListHdr','','PUT'),(112,'/api/EmployerDriveCampuses','','POST'),(113,'/api/EmployerDriveCampuses','','PUT'),(114,'/api/EmployerDriveCampuses/getEmployerDriveCampus','','GET'),(115,'/api/EmployerDrives','','POST'),(116,'/api/EmployerDrives/getEmployerDriveDetails','','GET'),(117,'/api/EmployerDrives/updateEmployerDrive','','PUT'),(118,'/api/EmployerEvents/createCompleteEmployerEvent','','POST'),(119,'/api/EmployerEvents/createEmployerEventAction','','POST'),(120,'/api/EmployerEvents/getAllEvents','','GET'),(121,'/api/EmployerEvents/getCompanyByEvent','','POST'),(122,'/api/EmployerEvents/getEmployerEventDetails','','GET'),(123,'/api/EmployerEvents/updateEmployerEvent','','PUT'),(124,'/api/EmployerPeople','','POST'),(125,'/api/EmployerPeople/getEmployerPerson','','GET'),(126,'/api/EmployerPeople/updateEmployeePerson','','PUT'),(127,'/api/EmployerPersonAddresses/createEmployerPersonAddress','','POST'),(128,'/api/EmployerPersonAddresses/deleteEmployeePersonAddress','','DELETE'),(129,'/api/EmployerPersonAddresses/getEmployerPersonAddress','','GET'),(130,'/api/EmployerPersonAddresses/updateEmployeePersonAddress','','PUT'),(131,'/api/EmployerPersonContacts/createEmployerContact','','POST'),(132,'/api/EmployerPersonContacts/deleteEmployerPersonContact','','DELETE'),(133,'/api/EmployerPersonContacts/getEmployerPersonContact','','GET'),(134,'/api/EmployerPersonContacts/updateEmployeePersonContact','','PUT'),(136,'/api/Enrollments','','POST'),(137,'/api/Enrollments/getEnrollmentDetails','','GET'),(138,'/api/Enrollments/updateEnrollment','','PUT'),(139,'/api/EventStudentLists','','POST'),(140,'/api/EventStudentLists/deleteSetOfStudents','','PUT'),(141,'/api/EventStudentLists/getStudentsByEventId','','GET'),(142,'/api/EventStudentLists/removeStudent','','PUT'),(143,'/api/EventStudentLists/studentEventActions','','PUT'),(144,'/api/EventStudentLists/studentEvents','','GET'),(145,'/api/EventStudentLists/unregStudentEventActions','','PUT'),(146,'/api/EventStudentLists/updateCandidateUpload','','POST'),(147,'/api/EventTestScoreWorks/eventTestScoreUpload','','POST'),(148,'/api/JobRoles','','POST'),(149,'/api/JobRoles/deleteJobRoleDetails','','DELETE'),(150,'/api/JobRoles/getJobRoleDetails','','GET'),(151,'/api/JobRoles/getTotalJobsBasedOnRole','','GET'),(152,'/api/JobRoles/updateJobRole','','PUT'),(153,'/api/NotificationDetails','','PUT'),(154,'/api/NotificationDetails/pullNotification','','POST'),(155,'/api/NotificationMessageTemplates/pushNotification','','POST'),(156,'/api/OrganizationAddresses/createOrganizationAddress','','POST'),(157,'/api/OrganizationAddresses/deleteOrganizationAddress','','DELETE'),(158,'/api/OrganizationAddresses/getOrganizationAddress','','GET'),(159,'/api/OrganizationAddresses/updateOrganizationAddress','','PUT'),(160,'/api/OrganizationContacts/createOrganizationContact','','POST'),(161,'/api/OrganizationContacts/deleteOrganizationContact','','DELETE'),(162,'/api/OrganizationContacts/getOrganizationContact','','GET'),(163,'/api/OrganizationContacts/updateOrganizationContact','','PUT'),(164,'/api/Organizations','','POST'),(166,'/api/Organizations/getOrganizationDetails','','GET'),(167,'/api/Organizations/updateOrganization','','PUT'),(168,'/api/ProgramContacts/createProgramContact','','POST'),(169,'/api/ProgramContacts/deleteProgramContact','','DELETE'),(170,'/api/ProgramContacts/getProgramContact','','GET'),(171,'/api/ProgramContacts/updateProgramContact','','PUT'),(172,'/api/Programs','','POST'),(173,'/api/Programs/getProgramDetails','','GET'),(174,'/api/Programs/searchCampusPrograms','','GET'),(175,'/api/Programs/updateProgram','','PUT'),(176,'/api/StudentAdditionalInfos','','POST'),(177,'/api/StudentAdditionalInfos/getStudentAdditionalInfo','','GET'),(178,'/api/StudentAdditionalInfos/subscribeOrUnsubscribe','','PUT'),(179,'/api/StudentAdditionalInfos/updateStudentInfo','','PUT'),(180,'/api/StudentAddresses/createStudentAddress','','POST'),(181,'/api/StudentAddresses/deleteStudentAddress','','DELETE'),(182,'/api/StudentAddresses/getStudentAddress','','GET'),(183,'/api/StudentAddresses/updateStudentAddress','','PUT'),(184,'/api/StudentAttributes','','POST'),(185,'/api/StudentAdditionalInfos/deleteStudentAdditionalInfo','','DELETE'),(186,'/api/StudentAttributes/deleteStudentAttribute','','DELETE'),(187,'/api/StudentAttributes/getStudentAttribute','','GET'),(188,'/api/StudentContacts/createStudentContact','','POST'),(189,'/api/StudentContacts/deleteStudentContact','','DELETE'),(190,'/api/StudentContacts/getStudentContact','','GET'),(191,'/api/StudentContacts/updateStudentContact','','PUT'),(192,'/api/StudentInterests','','POST'),(193,'/api/StudentInterests/deleteStudentInterest','','DELETE'),(194,'/api/StudentInterests/getStudentInterest','','GET'),(195,'/api/StudentMissingProgramWorks','','POST'),(196,'/api/StudentMissingProgramWorks/deleteMissingProgramWork','','DELETE'),(197,'/api/StudentMissingProgramWorks/getMissingWorkDetails','','GET'),(198,'/api/StudentMissingProgramWorks/updateStudentMissingWork','','PUT'),(200,'/api/Students/allDrivesOfMyDep','','GET'),(201,'/api/Students/getStudent','','GET'),(202,'/api/Students/graphData','','GET'),(203,'/api/Students/numberData','','GET'),(204,'/api/Students/studentEvents','','GET'),(205,'/api/Students/studentProfile','','GET'),(206,'/api/Students/updateStudent','','PUT'),(207,'/api/StudentsAssessmentUploadWorks/studentAssessmentMassUpload','','POST'),(208,'/api/StudentSkills','','POST'),(209,'/api/StudentSkills/deleteStudentSkill','','DELETE'),(210,'/api/StudentSkills/getStudentSkill','','GET'),(211,'/api/StudentsMassUploadWorks/studentMassUpload','','POST'),(212,'/api/UnregisterCampusStudentWorks/unregisteredCampusStudentUpload','','POST'),(213,'/api/UserRoles/sendEmail','','POST'),(214,'/api/EventStudentLists/createEventStudentList',NULL,'POST'),(215,'/api/EmployerDriveCampuses/createEmpDriveCampuses',NULL,'POST'),(216,'/api/EmployerCampusListDtls/getEmployerCampusListData',NULL,'GET'),(217,'/api/Campuses/getCampusInfo',NULL,'GET'),(218,'/api/Departments/departmentInfo',NULL,'GET'),(219,'/api/Programs/programInfo',NULL,'GET'),(220,'/api/EmployerCampusListCompPkgs/deleteCompensationPackage',NULL,'DELETE'),(221,'/api/Enrollments/deleteEnrollment',NULL,'DELETE'),(222,'/api/EmployerCampusListDtls/createEmployerCampusListDtl',NULL,'POST'),(223,'/api/EmployerCampusListCompPkgs/createEmployerCampusListCompPkg',NULL,'POST'),(224,'/api/StudentAttributes/updateStudentAttribs',NULL,'PUT');
/*!40000 ALTER TABLE `SCORA_SERVICES` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `SCORA_SERVICE_PRIVILEGES`
--

DROP TABLE IF EXISTS `SCORA_SERVICE_PRIVILEGES`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `SCORA_SERVICE_PRIVILEGES` (
  `service_privilege_id` int(15) NOT NULL AUTO_INCREMENT,
  `service_id` int(15) DEFAULT NULL,
  `access_level` int(15) NOT NULL,
  `description` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`service_privilege_id`),
  KEY `service_id` (`service_id`),
  CONSTRAINT `SCORA_SERVICE_PRIVILEGES_ibfk_1` FOREIGN KEY (`service_id`) REFERENCES `SCORA_SERVICES` (`service_id`)
) ENGINE=InnoDB AUTO_INCREMENT=225 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `SCORA_SERVICE_PRIVILEGES`
--

LOCK TABLES `SCORA_SERVICE_PRIVILEGES` WRITE;
/*!40000 ALTER TABLE `SCORA_SERVICE_PRIVILEGES` DISABLE KEYS */;
INSERT INTO `SCORA_SERVICE_PRIVILEGES` VALUES (1,1,7,''),(3,3,2,''),(5,5,2,''),(6,6,7,''),(7,7,4,''),(8,8,4,''),(9,9,4,''),(10,10,1,''),(11,11,1,''),(12,12,1,''),(13,13,1,''),(14,14,1,''),(15,15,1,''),(16,16,1,''),(17,17,1,''),(18,18,5,''),(19,19,1,''),(20,20,1,''),(21,21,1,''),(22,22,0,''),(23,23,5,''),(24,24,1,''),(25,25,1,''),(26,26,1,''),(27,27,7,''),(28,28,1,''),(29,29,7,''),(30,30,1,''),(31,31,1,''),(32,32,1,''),(34,34,1,''),(35,35,1,''),(36,36,1,''),(37,37,1,''),(38,38,1,''),(39,39,1,''),(40,40,1,''),(41,41,1,''),(42,42,7,''),(43,43,1,''),(44,44,2,''),(45,45,1,''),(46,46,0,''),(47,47,7,''),(48,48,2,''),(49,49,2,''),(50,50,2,''),(51,51,5,''),(52,52,5,''),(53,53,2,''),(54,54,5,''),(55,55,2,''),(56,56,2,''),(57,57,2,''),(58,58,2,''),(59,59,2,''),(60,60,2,''),(61,61,2,''),(62,62,1,''),(63,63,2,''),(64,64,2,''),(65,65,2,''),(66,66,2,''),(67,67,2,''),(68,68,2,''),(69,69,2,''),(70,70,2,''),(71,71,2,''),(72,72,2,''),(73,73,7,''),(74,74,2,''),(75,75,2,''),(76,76,7,''),(77,77,1,''),(78,78,1,''),(79,79,1,''),(80,80,1,''),(81,81,1,''),(82,82,1,''),(83,83,1,''),(84,84,1,''),(85,85,1,''),(87,87,1,''),(88,88,1,''),(89,89,0,''),(90,90,1,''),(91,91,1,''),(92,92,1,''),(93,93,1,''),(94,94,1,''),(95,95,1,''),(96,96,1,''),(97,97,1,''),(98,98,1,''),(99,99,1,''),(100,100,2,''),(101,101,2,''),(102,102,2,''),(103,103,2,''),(104,104,2,''),(105,105,2,''),(106,106,2,''),(107,107,2,''),(108,108,2,''),(109,109,2,''),(110,110,2,''),(111,111,2,''),(112,112,2,''),(113,113,2,''),(114,114,2,''),(115,115,2,''),(116,116,2,''),(117,117,2,''),(118,118,2,''),(119,119,2,''),(120,120,2,''),(121,121,5,''),(122,122,5,''),(123,123,2,''),(124,124,0,''),(125,125,2,''),(126,126,2,''),(127,127,2,''),(128,128,2,''),(129,129,2,''),(130,130,2,''),(131,131,2,''),(132,132,2,''),(133,133,2,''),(134,134,2,''),(136,136,4,''),(137,137,7,''),(138,138,4,''),(139,139,1,''),(140,140,1,''),(141,141,5,''),(142,142,1,''),(143,143,5,''),(144,144,4,''),(145,145,1,''),(146,146,1,''),(147,147,2,''),(148,148,2,''),(149,149,2,''),(150,150,2,''),(151,151,2,''),(152,152,2,''),(153,153,7,''),(154,154,7,''),(155,155,7,''),(156,156,2,''),(157,157,2,''),(158,158,2,''),(159,159,2,''),(160,160,2,''),(161,161,2,''),(162,162,2,''),(163,163,2,''),(164,164,2,''),(166,166,2,''),(167,167,2,''),(168,168,1,''),(169,169,1,''),(170,170,1,''),(171,171,1,''),(172,172,1,''),(173,173,1,''),(174,174,1,''),(175,175,1,''),(176,176,3,''),(177,177,7,''),(178,178,3,''),(179,179,3,''),(180,180,3,''),(181,181,3,''),(182,182,3,''),(183,183,3,''),(184,184,3,''),(185,185,3,''),(186,186,3,''),(187,187,7,''),(188,188,3,''),(189,189,3,''),(190,190,3,''),(191,191,3,''),(192,192,3,''),(193,193,3,''),(194,194,7,''),(195,195,3,''),(196,196,3,''),(197,197,3,''),(198,198,3,''),(200,200,3,''),(201,201,7,''),(202,202,3,''),(203,203,3,''),(204,204,3,''),(205,205,7,''),(206,206,3,''),(207,207,1,''),(208,208,3,''),(209,209,3,''),(210,210,7,''),(211,211,1,''),(212,212,2,''),(213,213,7,''),(214,214,1,NULL),(215,215,2,NULL),(216,216,2,NULL),(217,217,3,NULL),(218,218,3,NULL),(219,219,3,NULL),(220,220,2,NULL),(221,221,3,NULL),(222,222,2,NULL),(223,223,2,NULL),(224,224,3,NULL);
/*!40000 ALTER TABLE `SCORA_SERVICE_PRIVILEGES` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `SCORA_USER`
--

DROP TABLE IF EXISTS `SCORA_USER`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `SCORA_USER` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_status_value_id` int(11) DEFAULT NULL,
  `create_datetime` datetime NOT NULL,
  `update_datetime` datetime NOT NULL,
  `create_user_id` int(11) NOT NULL,
  `update_user_id` int(11) NOT NULL,
  `realm` varchar(512) DEFAULT NULL,
  `username` varchar(512) DEFAULT NULL,
  `password` varchar(512) NOT NULL,
  `email` varchar(512) NOT NULL,
  `emailVerified` tinyint(1) DEFAULT NULL,
  `verificationToken` varchar(512) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `IDX_SCORA_USER_01` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=13165 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `SCORA_USER`
--

LOCK TABLES `SCORA_USER` WRITE;
/*!40000 ALTER TABLE `SCORA_USER` DISABLE KEYS */;
INSERT INTO `SCORA_USER` VALUES (1,NULL,'2017-11-09 07:17:43','2017-11-09 07:17:43',1,1,NULL,NULL,'$2a$10$vcDO3wSJspmzUTRYZ94R/uPlsia9NsDTnT/Dk4vEANw5Ss0D5rHgK','admin@scoraxchange.com',1,NULL),(13162,NULL,'2018-04-04 07:42:39','2018-04-04 07:42:39',1,1,NULL,NULL,'$2a$10$OtvOEUAghHS1sVbcT1.HbexPXm6HTURpnEJuAyAOM1vYvkImNLMt.','jayanarayan.rayaroth@scoratech.com',1,NULL),(13163,NULL,'2018-04-04 07:44:11','2018-04-04 07:44:11',1,1,NULL,NULL,'$2a$10$NP/u4WtbgZRLZrUy/UOBheXN8F4/xD0DaszYm8q44KvjOlefu/5X2','pavitra.kalra@abjayonconsulting.com',1,NULL),(13164,NULL,'2018-04-05 13:07:15','2018-04-05 13:07:15',1,1,NULL,NULL,'$2a$10$ZMgAcjBg6FjUoMNcPODrAuFSSa7tm80HC1anK6OVsahfCud3iy3HW','tpo@bvucoep.edu.in',1,NULL);
/*!40000 ALTER TABLE `SCORA_USER` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `SCORA_USER_BKUP`
--

DROP TABLE IF EXISTS `SCORA_USER_BKUP`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `SCORA_USER_BKUP` (
  `id` int(11) NOT NULL DEFAULT '0',
  `user_status_value_id` int(11) DEFAULT NULL,
  `create_datetime` datetime NOT NULL,
  `update_datetime` datetime NOT NULL,
  `create_user_id` int(11) NOT NULL,
  `update_user_id` int(11) NOT NULL,
  `realm` varchar(512) DEFAULT NULL,
  `username` varchar(512) DEFAULT NULL,
  `password` varchar(512) NOT NULL,
  `email` varchar(512) NOT NULL,
  `emailVerified` tinyint(1) DEFAULT NULL,
  `verificationToken` varchar(512) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `SCORA_USER_BKUP`
--

LOCK TABLES `SCORA_USER_BKUP` WRITE;
/*!40000 ALTER TABLE `SCORA_USER_BKUP` DISABLE KEYS */;
/*!40000 ALTER TABLE `SCORA_USER_BKUP` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `SCORA_USER_ROLE`
--

DROP TABLE IF EXISTS `SCORA_USER_ROLE`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `SCORA_USER_ROLE` (
  `user_role_id` int(15) NOT NULL AUTO_INCREMENT,
  `user_id` int(15) NOT NULL,
  `role_code` varchar(16) NOT NULL,
  `start_date` datetime NOT NULL,
  `end_date` datetime DEFAULT NULL,
  `create_datetime` datetime NOT NULL,
  `update_datetime` datetime NOT NULL,
  `create_user_id` int(15) NOT NULL,
  `update_user_id` int(15) NOT NULL,
  PRIMARY KEY (`user_role_id`),
  KEY `role_code` (`role_code`),
  KEY `USER_ROLE_ibfk_1_idx` (`user_id`),
  CONSTRAINT `SCORA_USER_ROLE_ibfk_2` FOREIGN KEY (`role_code`) REFERENCES `SCORA_ROLE` (`role_code`),
  CONSTRAINT `SCORA_USER_ROLE_ibfk_3` FOREIGN KEY (`user_id`) REFERENCES `SCORA_USER` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12503 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `SCORA_USER_ROLE`
--

LOCK TABLES `SCORA_USER_ROLE` WRITE;
/*!40000 ALTER TABLE `SCORA_USER_ROLE` DISABLE KEYS */;
INSERT INTO `SCORA_USER_ROLE` VALUES (1,1,'SYSADMIN','2017-09-01 13:10:02',NULL,'2017-09-01 13:10:02','2017-09-01 13:10:02',1,1),(12500,13162,'RECDIR','2018-04-04 07:42:41',NULL,'2018-04-04 07:42:41','2018-04-04 07:42:41',1,1),(12501,13163,'RECDIR','2018-04-04 07:44:13',NULL,'2018-04-04 07:44:13','2018-04-04 07:44:13',1,1),(12502,13164,'PLCDIR','2018-04-05 13:07:18',NULL,'2018-04-05 13:07:18','2018-04-05 13:07:18',1,1);
/*!40000 ALTER TABLE `SCORA_USER_ROLE` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `STATE`
--

DROP TABLE IF EXISTS `STATE`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `STATE` (
  `state_code` varchar(16) NOT NULL,
  `country_code` varchar(16) NOT NULL,
  `state_name` varchar(100) DEFAULT NULL,
  `region_flag` varchar(4) NOT NULL,
  PRIMARY KEY (`state_code`,`country_code`),
  KEY `country_code` (`country_code`),
  KEY `IDX_STATE_01` (`region_flag`),
  CONSTRAINT `STATE_ibfk_1` FOREIGN KEY (`country_code`) REFERENCES `COUNTRY` (`country_code`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `STATE`
--

LOCK TABLES `STATE` WRITE;
/*!40000 ALTER TABLE `STATE` DISABLE KEYS */;
INSERT INTO `STATE` VALUES ('AN','IN','Andaman & Nicobar Islands','S'),('AP','IN','Andhra Pradesh','S'),('AR','IN','Arunachal Pradesh','E'),('AS','IN','Assam','E'),('BR','IN','Bihar','N'),('CH','IN','Chandigarh','N'),('CT','IN','Chattisgarh','N'),('DD','IN','Daman & Diu','W'),('DL','IN','Delhi','N'),('DN','IN','Dadra & Nagar Haveli','W'),('GA','IN','Goa','W'),('GJ','IN','Gujarat','W'),('HP','IN','Himachal Pradesh','N'),('HR','IN','Haryana','N'),('JH','IN','Jharkhand','N'),('JK','IN','Jammu & Kashmir','N'),('KA','IN','Karnataka','S'),('KL','IN','Kerala','S'),('LD','IN','Lakshadweep','S'),('MH','IN','Maharashtra','W'),('ML','IN','Meghalaya','E'),('MN','IN','Manipur','E'),('MP','IN','Madhya Pradesh','N'),('MZ','IN','Mizoram','E'),('NL','IN','Nagaland','E'),('OD','IN','Odisha','E'),('PB','IN','Punjab','N'),('PY','IN','Pondicherry','S'),('RJ','IN','Rajasthan','N'),('SK','IN','Sikkim','E'),('TN','IN','Tamil Nadu','S'),('TR','IN','Tripura','E'),('TS','IN','Telangana','S'),('UK','IN','Uttarakhand','N'),('UP','IN','Uttar Pradesh','N'),('WB','IN','West Bengal','E');
/*!40000 ALTER TABLE `STATE` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `STUDENT`
--

DROP TABLE IF EXISTS `STUDENT`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `STUDENT` (
  `student_id` int(15) NOT NULL AUTO_INCREMENT,
  `user_id` int(15) NOT NULL,
  `prefix_value_id` int(15) DEFAULT NULL,
  `first_name` varchar(50) NOT NULL,
  `middle_name` varchar(50) DEFAULT NULL,
  `last_name` varchar(50) NOT NULL,
  `gender_value_id` int(15) DEFAULT NULL,
  `date_of_birth` date DEFAULT NULL,
  `country_of_birth` varchar(16) DEFAULT NULL,
  `nationality` varchar(16) DEFAULT NULL,
  `marital_status_value_id` int(15) DEFAULT NULL,
  `guardian_name` varchar(50) DEFAULT NULL,
  `student_status_value_id` int(15) NOT NULL,
  `profilr_activation_ind` varchar(1) DEFAULT NULL,
  `disability_ind` varchar(1) DEFAULT NULL,
  `allergies` varchar(45) DEFAULT NULL,
  `about_me` varchar(2000) DEFAULT NULL,
  `highlights` varchar(2000) DEFAULT NULL,
  `hobbies` varchar(2000) DEFAULT NULL,
  `prof_body_membership` varchar(2000) DEFAULT NULL,
  `dream_company` varchar(100) DEFAULT NULL,
  `create_datetime` datetime NOT NULL,
  `update_datetime` datetime NOT NULL,
  `create_user_id` int(15) NOT NULL,
  `update_user_id` int(15) NOT NULL,
  `off_campus_ind` varchar(1) DEFAULT NULL,
  `expected_salary` decimal(18,6) DEFAULT NULL,
  PRIMARY KEY (`student_id`),
  KEY `country_of_birth` (`country_of_birth`),
  KEY `nationality` (`nationality`),
  KEY `prefix_value_id` (`prefix_value_id`),
  KEY `gender_value_id` (`gender_value_id`),
  KEY `marital_status_value_id` (`marital_status_value_id`),
  KEY `student_status_value_id` (`student_status_value_id`),
  KEY `STUDENT_ibfk_1_idx` (`user_id`),
  CONSTRAINT `STUDENT_ibfk_2` FOREIGN KEY (`country_of_birth`) REFERENCES `COUNTRY` (`country_code`),
  CONSTRAINT `STUDENT_ibfk_3` FOREIGN KEY (`nationality`) REFERENCES `COUNTRY` (`country_code`),
  CONSTRAINT `STUDENT_ibfk_4` FOREIGN KEY (`prefix_value_id`) REFERENCES `LOOKUP_VALUE` (`lookup_value_id`),
  CONSTRAINT `STUDENT_ibfk_5` FOREIGN KEY (`gender_value_id`) REFERENCES `LOOKUP_VALUE` (`lookup_value_id`),
  CONSTRAINT `STUDENT_ibfk_6` FOREIGN KEY (`marital_status_value_id`) REFERENCES `LOOKUP_VALUE` (`lookup_value_id`),
  CONSTRAINT `STUDENT_ibfk_7` FOREIGN KEY (`student_status_value_id`) REFERENCES `LOOKUP_VALUE` (`lookup_value_id`),
  CONSTRAINT `STUDENT_ibfk_8` FOREIGN KEY (`user_id`) REFERENCES `SCORA_USER` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `STUDENT`
--

LOCK TABLES `STUDENT` WRITE;
/*!40000 ALTER TABLE `STUDENT` DISABLE KEYS */;
/*!40000 ALTER TABLE `STUDENT` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `STUDENTS_ASSESSMENT_UPLOAD_WORK`
--

DROP TABLE IF EXISTS `STUDENTS_ASSESSMENT_UPLOAD_WORK`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `STUDENTS_ASSESSMENT_UPLOAD_WORK` (
  `row_number` int(5) DEFAULT NULL,
  `admission_no` varchar(50) DEFAULT NULL,
  `score` int(3) DEFAULT NULL,
  `program_unit_type_value_id` int(15) DEFAULT NULL,
  `department_id` int(15) DEFAULT NULL,
  `program_id` int(15) DEFAULT NULL,
  `error` varchar(500) DEFAULT NULL,
  `campus_upload_log_id` int(15) DEFAULT NULL,
  KEY `campus_upload_log_id` (`campus_upload_log_id`),
  KEY `IDX_STU_ASMT_UP_WRK_01` (`admission_no`),
  CONSTRAINT `STUDENTS_ASSESSMENT_UPLOAD_WORK_ibfk_1` FOREIGN KEY (`campus_upload_log_id`) REFERENCES `CAMPUS_UPLOAD_LOG` (`campus_upload_log_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `STUDENTS_ASSESSMENT_UPLOAD_WORK`
--

LOCK TABLES `STUDENTS_ASSESSMENT_UPLOAD_WORK` WRITE;
/*!40000 ALTER TABLE `STUDENTS_ASSESSMENT_UPLOAD_WORK` DISABLE KEYS */;
/*!40000 ALTER TABLE `STUDENTS_ASSESSMENT_UPLOAD_WORK` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `STUDENTS_MASS_UPLOAD_WORK`
--

DROP TABLE IF EXISTS `STUDENTS_MASS_UPLOAD_WORK`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `STUDENTS_MASS_UPLOAD_WORK` (
  `row_number` int(5) DEFAULT NULL,
  `first_name` varchar(50) DEFAULT NULL,
  `last_name` varchar(50) DEFAULT NULL,
  `email` varchar(254) DEFAULT NULL,
  `program_name` varchar(50) DEFAULT NULL,
  `admission_no` varchar(50) DEFAULT NULL,
  `start_date` varchar(500) DEFAULT NULL,
  `planned_completion_date` varchar(500) DEFAULT NULL,
  `error` varchar(500) DEFAULT NULL,
  `program_id` int(15) DEFAULT NULL,
  `campus_id` int(15) DEFAULT NULL,
  `skip_user_ind` varchar(1) DEFAULT NULL,
  `campus_upload_log_id` int(15) DEFAULT NULL,
  `student_id` int(15) DEFAULT NULL,
  `create_user_id` int(15) DEFAULT NULL,
  `update_user_id` int(15) DEFAULT NULL,
  `system_password` varchar(512) DEFAULT NULL,
  `password` varchar(512) DEFAULT NULL,
  KEY `campus_upload_log_id` (`campus_upload_log_id`),
  KEY `IDX_STU_MASS_UPLD_WRK_01` (`email`),
  KEY `IDX_STU_MASS_UPLD_WRK_02` (`admission_no`),
  CONSTRAINT `STUDENTS_MASS_UPLOAD_WORK_ibfk_1` FOREIGN KEY (`campus_upload_log_id`) REFERENCES `CAMPUS_UPLOAD_LOG` (`campus_upload_log_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `STUDENTS_MASS_UPLOAD_WORK`
--

LOCK TABLES `STUDENTS_MASS_UPLOAD_WORK` WRITE;
/*!40000 ALTER TABLE `STUDENTS_MASS_UPLOAD_WORK` DISABLE KEYS */;
/*!40000 ALTER TABLE `STUDENTS_MASS_UPLOAD_WORK` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `STUDENT_ADDITIONAL_INFO`
--

DROP TABLE IF EXISTS `STUDENT_ADDITIONAL_INFO`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `STUDENT_ADDITIONAL_INFO` (
  `academics_id` int(15) NOT NULL AUTO_INCREMENT,
  `student_id` int(15) NOT NULL,
  `institute_name` varchar(100) DEFAULT NULL,
  `program_name` varchar(100) DEFAULT NULL,
  `score_grade` int(3) DEFAULT NULL,
  `comment` varchar(2000) DEFAULT NULL,
  `create_datetime` datetime NOT NULL,
  `update_datetime` datetime NOT NULL,
  `create_user_id` int(15) NOT NULL,
  `update_user_id` int(15) NOT NULL,
  `attach_cat_code_value_id` int(15) DEFAULT NULL,
  `attachment_url` varchar(200) DEFAULT NULL,
  `duration` int(2) DEFAULT NULL,
  `duration_type_value_id` int(15) DEFAULT NULL,
  `completion_year` int(11) DEFAULT NULL,
  `highlight` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`academics_id`),
  KEY `IDX_FK_STU_ACADEMIC` (`student_id`),
  KEY `duration_type_value_id` (`duration_type_value_id`),
  CONSTRAINT `STUDENT_ADDITIONAL_INFO_ibfk_1` FOREIGN KEY (`student_id`) REFERENCES `STUDENT` (`student_id`),
  CONSTRAINT `STUDENT_ADDITIONAL_INFO_ibfk_2` FOREIGN KEY (`duration_type_value_id`) REFERENCES `LOOKUP_VALUE` (`lookup_value_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `STUDENT_ADDITIONAL_INFO`
--

LOCK TABLES `STUDENT_ADDITIONAL_INFO` WRITE;
/*!40000 ALTER TABLE `STUDENT_ADDITIONAL_INFO` DISABLE KEYS */;
/*!40000 ALTER TABLE `STUDENT_ADDITIONAL_INFO` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `STUDENT_ADDRESS`
--

DROP TABLE IF EXISTS `STUDENT_ADDRESS`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `STUDENT_ADDRESS` (
  `student_id` int(15) NOT NULL,
  `address_id` int(15) NOT NULL,
  `primary_ind` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`student_id`,`address_id`),
  KEY `address_id` (`address_id`),
  CONSTRAINT `STUDENT_ADDRESS_ibfk_1` FOREIGN KEY (`student_id`) REFERENCES `STUDENT` (`student_id`),
  CONSTRAINT `STUDENT_ADDRESS_ibfk_2` FOREIGN KEY (`address_id`) REFERENCES `ADDRESS` (`address_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `STUDENT_ADDRESS`
--

LOCK TABLES `STUDENT_ADDRESS` WRITE;
/*!40000 ALTER TABLE `STUDENT_ADDRESS` DISABLE KEYS */;
/*!40000 ALTER TABLE `STUDENT_ADDRESS` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `STUDENT_ATTRIBUTES`
--

DROP TABLE IF EXISTS `STUDENT_ATTRIBUTES`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `STUDENT_ATTRIBUTES` (
  `student_attribute_id` int(15) NOT NULL AUTO_INCREMENT,
  `student_id` int(15) NOT NULL,
  `attach_cat_value_id` int(15) NOT NULL,
  `attach_type_value_id` int(15) NOT NULL,
  `attach_location` varchar(200) NOT NULL,
  `create_datetime` datetime NOT NULL,
  `update_datetime` datetime NOT NULL,
  `create_user_id` int(15) NOT NULL,
  `update_user_id` int(15) NOT NULL,
  PRIMARY KEY (`student_attribute_id`),
  KEY `IDX_FK_STU_ATT` (`student_id`),
  KEY `attach_cat_value_id` (`attach_cat_value_id`),
  KEY `attach_type_value_id` (`attach_type_value_id`),
  CONSTRAINT `STUDENT_ATTRIBUTES_ibfk_1` FOREIGN KEY (`student_id`) REFERENCES `STUDENT` (`student_id`),
  CONSTRAINT `STUDENT_ATTRIBUTES_ibfk_2` FOREIGN KEY (`attach_cat_value_id`) REFERENCES `LOOKUP_VALUE` (`lookup_value_id`),
  CONSTRAINT `STUDENT_ATTRIBUTES_ibfk_3` FOREIGN KEY (`attach_type_value_id`) REFERENCES `LOOKUP_VALUE` (`lookup_value_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `STUDENT_ATTRIBUTES`
--

LOCK TABLES `STUDENT_ATTRIBUTES` WRITE;
/*!40000 ALTER TABLE `STUDENT_ATTRIBUTES` DISABLE KEYS */;
/*!40000 ALTER TABLE `STUDENT_ATTRIBUTES` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `STUDENT_CONTACT`
--

DROP TABLE IF EXISTS `STUDENT_CONTACT`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `STUDENT_CONTACT` (
  `student_id` int(15) NOT NULL,
  `contact_id` int(15) NOT NULL,
  `primary_ind` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`student_id`,`contact_id`),
  KEY `contact_id` (`contact_id`),
  CONSTRAINT `STUDENT_CONTACT_ibfk_1` FOREIGN KEY (`student_id`) REFERENCES `STUDENT` (`student_id`),
  CONSTRAINT `STUDENT_CONTACT_ibfk_2` FOREIGN KEY (`contact_id`) REFERENCES `CONTACT` (`contact_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `STUDENT_CONTACT`
--

LOCK TABLES `STUDENT_CONTACT` WRITE;
/*!40000 ALTER TABLE `STUDENT_CONTACT` DISABLE KEYS */;
/*!40000 ALTER TABLE `STUDENT_CONTACT` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `STUDENT_HOBBIES`
--

DROP TABLE IF EXISTS `STUDENT_HOBBIES`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `STUDENT_HOBBIES` (
  `student_hobby_id` int(15) NOT NULL AUTO_INCREMENT,
  `student_id` int(15) NOT NULL,
  `hobby` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`student_hobby_id`),
  KEY `student_id` (`student_id`),
  CONSTRAINT `STUDENT_HOBBIES_ibfk_1` FOREIGN KEY (`student_id`) REFERENCES `STUDENT` (`student_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `STUDENT_HOBBIES`
--

LOCK TABLES `STUDENT_HOBBIES` WRITE;
/*!40000 ALTER TABLE `STUDENT_HOBBIES` DISABLE KEYS */;
/*!40000 ALTER TABLE `STUDENT_HOBBIES` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `STUDENT_INTERESTS`
--

DROP TABLE IF EXISTS `STUDENT_INTERESTS`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `STUDENT_INTERESTS` (
  `student_id` int(15) NOT NULL,
  `interest_type_value_id` int(15) NOT NULL,
  PRIMARY KEY (`student_id`,`interest_type_value_id`),
  CONSTRAINT `STUDENT_INTERESTS_ibfk_1` FOREIGN KEY (`student_id`) REFERENCES `STUDENT` (`student_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `STUDENT_INTERESTS`
--

LOCK TABLES `STUDENT_INTERESTS` WRITE;
/*!40000 ALTER TABLE `STUDENT_INTERESTS` DISABLE KEYS */;
/*!40000 ALTER TABLE `STUDENT_INTERESTS` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `STUDENT_MISSING_PROGRAM_WORK`
--

DROP TABLE IF EXISTS `STUDENT_MISSING_PROGRAM_WORK`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `STUDENT_MISSING_PROGRAM_WORK` (
  `student_id` int(15) DEFAULT NULL,
  `university_id` int(15) DEFAULT NULL,
  `university_name` varchar(100) DEFAULT NULL,
  `campus_id` int(15) DEFAULT NULL,
  `campus_name` varchar(100) DEFAULT NULL,
  `department_id` int(15) DEFAULT NULL,
  `department_name` varchar(100) DEFAULT NULL,
  `program_type_value_id` int(15) DEFAULT NULL,
  `program_class_value_id` int(15) DEFAULT NULL,
  `program_cat_value_id` int(15) DEFAULT NULL,
  `program_major_value_id` int(15) DEFAULT NULL,
  `program_name` varchar(50) NOT NULL,
  `enrollment_id` int(15) NOT NULL,
  KEY `enrollment_id` (`enrollment_id`),
  CONSTRAINT `STUDENT_MISSING_PROGRAM_WORK_ibfk_1` FOREIGN KEY (`enrollment_id`) REFERENCES `ENROLLMENT` (`enrollment_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `STUDENT_MISSING_PROGRAM_WORK`
--

LOCK TABLES `STUDENT_MISSING_PROGRAM_WORK` WRITE;
/*!40000 ALTER TABLE `STUDENT_MISSING_PROGRAM_WORK` DISABLE KEYS */;
/*!40000 ALTER TABLE `STUDENT_MISSING_PROGRAM_WORK` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `STUDENT_SKILLS`
--

DROP TABLE IF EXISTS `STUDENT_SKILLS`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `STUDENT_SKILLS` (
  `student_id` int(15) NOT NULL,
  `skill_type_value_id` int(15) NOT NULL,
  PRIMARY KEY (`student_id`,`skill_type_value_id`),
  CONSTRAINT `STUDENT_SKILLS_ibfk_1` FOREIGN KEY (`student_id`) REFERENCES `STUDENT` (`student_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `STUDENT_SKILLS`
--

LOCK TABLES `STUDENT_SKILLS` WRITE;
/*!40000 ALTER TABLE `STUDENT_SKILLS` DISABLE KEYS */;
/*!40000 ALTER TABLE `STUDENT_SKILLS` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `UNIVERSITY`
--

DROP TABLE IF EXISTS `UNIVERSITY`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `UNIVERSITY` (
  `university_id` int(15) NOT NULL AUTO_INCREMENT,
  `university_type_value_id` int(15) NOT NULL,
  `short_name` varchar(50) DEFAULT NULL,
  `name` varchar(100) DEFAULT NULL,
  `description` varchar(2000) DEFAULT NULL,
  `rank` int(5) DEFAULT NULL,
  `tier` varchar(16) DEFAULT NULL,
  `website_link` varchar(200) DEFAULT NULL,
  `year_established` int(4) DEFAULT NULL,
  PRIMARY KEY (`university_id`),
  KEY `IDX_UNIV` (`name`),
  KEY `university_type_value_id` (`university_type_value_id`),
  CONSTRAINT `UNIVERSITY_ibfk_1` FOREIGN KEY (`university_type_value_id`) REFERENCES `LOOKUP_VALUE` (`lookup_value_id`)
) ENGINE=InnoDB AUTO_INCREMENT=638 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `UNIVERSITY`
--

LOCK TABLES `UNIVERSITY` WRITE;
/*!40000 ALTER TABLE `UNIVERSITY` DISABLE KEYS */;
INSERT INTO `UNIVERSITY` VALUES (1,399,'','Acharya N.G. Ranga Agricultural University','',0,'0','',1964),(2,399,'','Acharya Nagarjuna University','',0,'0','',1976),(3,53,'','Adamas University','',0,'0','',2014),(4,53,'','Adesh University','',0,'0','',2012),(5,399,'','Adikavi Nannaya University','',0,'0','',2006),(6,50,'','Agriculture University Jodhpur','',0,'0','',2013),(7,399,'','Ahmedabad University','',0,'0','',2009),(8,53,'','AISECT University','',0,'0','',0),(9,53,'','Ajeenkya DY Patil University','',0,'0','',2015),(10,53,'','Akhil Bharatiya Gandharva Mahavidyalaya Mandal','',0,'0','',1939),(11,53,'','AKS University','',0,'0','',2011),(12,399,'','Alagappa University','',0,'0','',1985),(13,53,'','Al-Falah University - AFU','',0,'0','',1997),(14,399,'','Aliah University - AU','',0,'0','',1780),(15,400,'','Aligarh Muslim University - AMU','',0,'0','',1875),(16,51,'','All India Institute of Medical Sciences','',0,'0','',2012),(17,400,'','Allahabad University','',0,'0','',1887),(18,53,'','Alliance University','',0,'0','',2010),(19,51,'','AMET University','',0,'0','',1993),(20,399,'','Amity University','',0,'0','',2010),(21,51,'','Amrita Vishwa Vidyapeetham','',0,'0','',2003),(22,399,'','Anand Agricultural University - AAU','',0,'0','',2004),(23,53,'','Anant National University','',0,'0','',2016),(24,399,'','Andhra University - AU','',0,'0','',1926),(25,399,'','Anna University','',0,'0','',1978),(26,399,'','Annamalai University','',0,'0','',1929),(27,53,'','Ansal University - AU','',0,'0','',2006),(28,53,'','AP Theological University','',0,'0','',1997),(29,399,'','Apeejay Stya University - ASU','',0,'0','',2010),(30,53,'','APG Shimla University','',0,'0','',2012),(31,399,'','APJ Abdul Kalam Technological University','',0,'0','',2014),(32,399,'','Arni University','',0,'0','',2009),(33,399,'','Arunachal University of Studies - AUS','',0,'0','',2012),(34,399,'','Aryabhatta Knowledge University - AKU','',0,'0','',2008),(35,53,'','Ashoka University','',0,'0','',0),(36,399,'','Assam Agricultural University - AAU','',0,'0','',1969),(37,399,'','Assam Down Town University','',0,'0','',2010),(38,399,'','Assam Science and Technology University','',0,'0','',2010),(39,400,'','Assam University','',0,'0','',1994),(40,399,'','Assam Women\'s University - AWU','',0,'0','',2013),(41,53,'','Atal Bihari Vajpayee Hindi Vishwavidyalaya','',0,'0','',2012),(42,53,'','Auro University','',0,'0','',0),(43,53,'','Avantika University','',0,'0','',2015),(44,51,'','Avinashilingam University - Institute for Home Science and Higher Education for Women','',0,'0','',1987),(45,399,'','Awadhesh Pratap Singh University','',0,'0','',1968),(46,399,'','Ayush and Health Science University','',0,'0','',2009),(47,399,'','Azim Premji University','',0,'0','',2011),(48,399,'','Baba Farid University of Health Sciences','',0,'0','',1998),(49,399,'','Baba Ghulam Shah Badshah University','',0,'0','',2002),(50,53,'','Baba Mast Nath University','',0,'0','',2006),(51,399,'','Babasaheb Bhimrao Ambedkar Bihar University - BRABU','',0,'0','',1960),(52,400,'','Babasaheb Bhimrao Ambedkar University - BBAU','',0,'0','',1996),(53,399,'','Babu Banarasi Das University - BBDU','',0,'0','',2010),(54,399,'','Baddi University of Emerging Sciences and Technologies','',0,'0','',2002),(55,399,'','Bahra University - BU','',0,'0','',2011),(56,400,'','Banaras Hindu University - BHU','',0,'0','',1916),(57,399,'','Banasthali University','',0,'0','',1935),(58,399,'','Bangalore University','',0,'0','',1964),(59,399,'','Barkatullah University - BU','',0,'0','',1970),(60,399,'','Bastar University','',0,'0','',2008),(61,399,'','Berhampur University','',0,'0','',1967),(62,399,'','Bhagat Phool Singh Mahila Vishwavidyalaya - BPSMV','',0,'0','',0),(63,51,'','Bhagwant University','',0,'0','',0),(64,399,'','Bharat Ratna Dr B.R. Ambedkar University','',0,'0','',2007),(65,399,'','Bharath University','',0,'0','',1984),(66,399,'','Bharathiar University','',0,'0','',1982),(67,399,'','Bharathidasan University','',0,'0','',1982),(68,51,'','Bharati Vidyapeeth Deemed University','',0,'0','',0),(69,51,'BVU','Bharati Vidyapeeth University','The institutions of Bharati Vidyapeeth (BV) was accorded deemed university status in 1996 for its academic excellence. The Bharati Vidyapeeth University (BVU)\nhas established academic excellence and offers programmes in innovative and emerging areas,\npresently has 29 constituent institutions, including three research institutes dedicated exclusively to research,\nis multi-campus and multi-disciplinary and is catering to the needs of Urban and Rural students,\nhas significant achievements in research,\nhas world-class infrastructure and facilities, launched several innovative academic programmes, best teaching-learning processes and has entered into national, as well as, international collaborations.',0,'0','http://www.bvuniversity.edu.in/Home/Home.aspx',1996),(70,51,'','Bhatkhande Music Institute Deemed University','',0,'0','',1926),(71,399,'','Bhavnagar University','',0,'0','',1978),(72,399,'','Bhupendra Narayan Mandal University','',0,'0','',1992),(73,399,'','Bidhan Chandra Krishi Viswa Vidyalaya','',0,'0','',1974),(74,399,'','Bihar Agricultural University','',0,'0','',2010),(75,51,'','Bihar Yoga Bharati - BYB','',0,'0','',1994),(76,399,'','Biju Patnaik University of Technology - BPUT','',0,'0','',2002),(77,53,'','Bilaspur University','',0,'0','',0),(78,51,'','Birla Institute of Technology - BIT Ranchi','',0,'0','',1955),(79,51,'BITS Pilani','Birla Institute of Technology and Science , Pilani','The Birla Institute of Technology & Science, BITS Pilani is an all-India Institute for higher education. The primary motive of BITS is to \"train young men and women able and eager to create and put into action such ideas, methods, techniques and information\". The Institute is a dream come true of its founder late Mr G.D.Birla - an eminent industrialist, a participant in Indian freedom struggle and a close associate of the Father of Indian Nation late Mr. Mohandas Karamchand Gandhi (Mahatma Gandhi). What started in early 1900s as a small school, blossomed into a set of colleges for higher education, ranging from the Humanities to Engineering until 1964 when all these colleges amalgamated to culminate into a unique Indian University of International standing. This university was christened as the Birla Institute of Technology and Science, Pilani, known to many as BITS, Pilani.',0,'0','http://www.bits-pilani.ac.in/',1929),(80,399,'','Birsa Agricultural University','',0,'0','',1981),(81,51,'','BLDE University','',0,'0','',2008),(82,53,'','BML Munjal University - BMU','',0,'0','',2013),(83,51,'','BS Abdur Rahman University','',0,'0','',1984),(84,399,'','Bundelkhand University','',0,'0','',1975),(85,399,'','Calorx Teacher\'s University','',0,'0','',2009),(86,53,'','Career Point University','',0,'0','',0),(87,53,'','Career Point University - CPU','',0,'0','',1993),(88,400,'','Central Agricultural University - CSU','',0,'0','',1992),(89,51,'','Central Institute of Fisheries Education - CIFE','',0,'0','',1961),(90,51,'','Central Institute of Higher Tibetan Studies/ Central University of Tibetan Studies','',0,'0','',1967),(91,400,'','Central University','',0,'0','',2009),(92,399,'','Centurion University of Technology and Management - CUTM','',0,'0','',2010),(93,399,'','CEPT University','',0,'0','',1962),(94,399,'','Chanakya National Law University - CNLU Patna','',0,'0','',2006),(95,53,'','Chandigarh University - CU','',0,'0','',2012),(96,50,'','Chandra Shekhar Azad University of Agriculture and Technology','',0,'0','',1975),(97,53,'','Charotar University of Science and Technology - CHARUSAT','',0,'0','',2009),(98,399,'','Chaudhary Bansi Lal University - CBLU','',0,'0','',2014),(99,399,'','Chaudhary Charan Singh Haryana Agricultural University','',0,'0','',1970),(100,399,'','Chaudhary Charan Singh University - Meerut','',0,'0','',1966),(101,399,'','Chaudhary Devi Lal University','',0,'0','',2003),(102,53,'','Chaudhary Ranbir Singh University - CRSU','',0,'0','',2014),(103,399,'','Chaudhary Sarwan Kumar Agricultural Vishvavidyalaya','',0,'0','',1978),(104,51,'','Chennai Mathematical Institute - CMI','',0,'0','',1989),(105,51,'','Chettinad Academy of Research and Education - CARE','',0,'0','',2005),(106,399,'','Chhatrapati Shahu Ji Maharaj University','',0,'0','',1966),(107,399,'','Chhattisgarh Swami Vivekanand Technical University','',0,'0','',2005),(108,53,'','Children\'s University','',0,'0','',0),(109,53,'','Chitkara University','',0,'0','',0),(110,51,'','Christ University','',0,'0','',1969),(111,53,'','CMJ University','',0,'0','',2009),(112,53,'','CMR University - CMRU','',0,'0','',0),(113,399,'','Cochin University of Science and Technology - CUSAT','',0,'0','',1971),(114,399,'','Damodaram Sanjivayya National Law University - DSNLU Vishakhapatnam','',0,'0','',2008),(115,53,'','Darul Huda Islamic University - DHIU','',0,'0','',1986),(116,51,'','Datta Meghe Institute of Medical Sciences - DMIMS','',0,'0','',2005),(117,53,'','DAV University','',0,'0','',2013),(118,399,'','Davangere University','',0,'0','',2009),(119,51,'','Dayalbagh Educational Institute','',0,'0','',1917),(120,53,'','Dayananda Sagar University - DSU','',0,'0','',0),(121,51,'','Deccan College Post Graduate and Research Institute','',0,'0','',1939),(122,399,'','Deenbandhu Chhotu Ram University of Science and Technology','',0,'0','',2006),(123,399,'','Deendayal Upadhyaya Gorakhpur University','',0,'0','',1965),(124,51,'','Defence Institute of Advanced Technology - DIAT','',0,'0','',1952),(125,399,'','Dehradun Institute of Technology - DIT University','',0,'0','',1998),(126,399,'','Delhi Technological University','',0,'0','',1941),(127,399,'','Desh Bhagat University','',0,'0','',1996),(128,53,'','Dev Sanskriti Vishwavidyalaya','',0,'0','',2002),(129,399,'','Devi Ahilya Vishwavidyalaya','',0,'0','',1964),(130,399,'','Dharmsinh Desai University - DDU','',0,'0','',1968),(131,53,'','Dhirubhai Ambani Institute of Information and Communication Technology - DA-IICT','',0,'0','',2001),(132,399,'','Dibrugarh University','',0,'0','',1965),(133,53,'','Divine Hands International University - DHIU','',0,'0','',0),(134,53,'','Dnyaneshwar Vidyapeeth - DV','',0,'0','',1980),(135,399,'','Don Bosco University - DBU','',0,'0','',2008),(136,399,'','Doon University','',0,'0','',2005),(137,399,'','Dr. A.P.J. Abdul Kalam Technical University','',0,'0','',2000),(138,51,'','Dr. B.R. Ambedkar National Institute of Technology - NIT Jalandhar','',0,'0','',1987),(139,399,'','Dr. B.R. Ambedkar University','',0,'0','',2008),(140,53,'','Dr. B.R. Ambedkar University of Social Sciences','',0,'0','',0),(141,399,'','Dr. Babasaheb Ambedkar University ','',0,'0','',1958),(142,399,'','Dr. Bhim Rao Ambedkar University / Agra University','',0,'0','',1927),(143,399,'','Dr. CV Raman University','',0,'0','',2006),(144,51,'','Dr. DY Patil Vidyapeeth','',0,'0','',2003),(145,400,'','Dr. Hari Singh Gour University','',0,'0','',1946),(146,53,'','Dr. KN Modi University','',0,'0','',2010),(147,399,'','Dr. MGR Educational and Research Institute','',0,'0','',2003),(148,399,'','Dr. NTR University of Health Sciences','',0,'0','',1986),(149,399,'','Dr. Panjabrao Deshmukh Krishi Vidyapeeth','',0,'0','',1969),(150,399,'','Dr. Ram Manohar Lohia Avadh University','',0,'0','',1994),(151,399,'','Dr. Ram Manohar Lohia National Law University - RMLNLU Lucknow','',0,'0','',2006),(152,53,'','Dr. Sarvepalli Radhakrishnan Rajasthan Ayurved University - DSRRAU','',0,'0','',2003),(153,399,'','Dr. Shakuntala Misra National Rehabilitation University - DSMRU','',0,'0','',2008),(154,50,'','Dr. Yashwant Singh Parmar University of Horticulture and Forestry','',0,'0','',1985),(155,399,'','Dr. YSR Horticultural University','',0,'0','',2007),(156,399,'','Dravidian University','',0,'0','',1997),(157,51,'','DY Patil University','',0,'0','',2002),(158,399,'','Eastern Institute for Integrated Learning in Management - EIILM University','',0,'0','',2006),(159,400,'','English and Foreign Languages University','',0,'0','',1958),(160,53,'','Eternal University','',0,'0','',2008),(161,399,'','Fakir Mohan University','',0,'0','',1989),(162,51,'','Forest Research Institute - FRI','',0,'0','',1906),(163,399,'','Galgotias University - GU','',0,'0','',2011),(164,399,'','Ganpat University','',0,'0','',2005),(165,399,'','Gauhati University','',0,'0','',1948),(166,399,'','Gautam Buddha University','',0,'0','',2002),(167,53,'','GD Goenka University','',0,'0','',2013),(168,53,'','Geetanjali University - GU','',0,'0','',2011),(169,51,'','GITAM University','',0,'0','',2012),(170,53,'','GLA University','',0,'0','',0),(171,53,'','GNA University','',0,'0','',0),(172,399,'','Goa University','',0,'0','',1985),(173,51,'','Gokhale Institute of Politics and Economics - GIPE','',0,'0','',1930),(174,399,'','Gondwana University','',0,'0','',0),(175,399,'','Govind Ballabh Pant University of Agriculture and Technology','',0,'0','',1960),(176,53,'','Graphic Era Hill University - GEHU Bhimtal','',0,'0','',2011),(177,51,'','Graphic Era University - GEU','',0,'0','',2001),(178,399,'','Gujarat Ayurved University','',0,'0','',1966),(179,399,'','Gujarat Forensic Sciences University - GFSU','',0,'0','',2008),(180,399,'','Gujarat National Law University - GNLU Gujarat','',0,'0','',2003),(181,399,'','Gujarat Technological University','',0,'0','',2008),(182,399,'','Gujarat University - GU','',0,'0','',1949),(183,51,'','Gujarat Vidyapith','',0,'0','',1920),(184,399,'','Gulbarga University','',0,'0','',1980),(185,50,'','Guru Angad Dev Veterinary and Animal Sciences University - GADVASU','',0,'0','',2005),(186,400,'','Guru Ghasidas Vishwavidyalaya','',0,'0','',1983),(187,399,'','Guru Gobind Singh Indraprastha University - GGSIPU','',0,'0','',1998),(188,399,'','Guru Jambheshwar University of Science and Technology','',0,'0','',1995),(189,53,'','Guru Kashi University','',0,'0','',2011),(190,399,'','Guru Nanak Dev University','',0,'0','',1969),(191,399,'','Guru Ravidas Ayurved University - GRAU','',0,'0','',2011),(192,51,'','Gurukula Kangri Vishwavidyalaya','',0,'0','',1902),(193,399,'','Hemchandracharya North Gujarat University','',0,'0','',1986),(194,400,'','Hemwati Nandan Bahuguna Garhwal University','',0,'0','',1973),(195,399,'','Hidayatullah National Law University - HNLU Raipur','',0,'0','',2003),(196,51,'','HIHT University','',0,'0','',1995),(197,399,'','Himachal Pradesh University','',0,'0','',1970),(198,53,'','Himalayan University','',0,'0','',2013),(199,53,'','Himgiri Zee University - Himgiri Nabh Vishwavidyalaya','',0,'0','',2003),(200,51,'','Hindustan University','',0,'0','',1985),(201,51,'','Homi Bhabha National Institute','',0,'0','',2005),(202,53,'','Homoeopathy University','',0,'0','',2009),(203,51,'','IASE University','',0,'0','',1950),(204,51,'','ICFAI Foundation for Higher Education - IFHE','',0,'0','http://www.icfaiuniversity.in/',1984),(205,53,'','IEC University - Baddi Campus','',0,'0','',0),(206,53,'','IFTM University','',0,'0','',2010),(207,51,'','IIS University - International College for Girls - ICG','',0,'0','',0),(208,53,'','IMS Unison University','',0,'0','',0),(209,51,'','Indian Agricultural Research Institute','',0,'0','',1905),(210,51,'','Indian Institute of Agricultural Biotechnology - IIAB','',0,'0','',0),(211,51,'','Indian Institute of Engineering Science and Technology - IIEST','',0,'0','',1856),(212,51,'','Indian Institute of Foreign Trade - IIFT New Delhi','',0,'0','',1963),(213,53,'','Indian Institute of Health Management Research - IIHMR University','',0,'0','',1984),(214,51,'','Indian Institute of Information Technology - IIIT','',0,'0','',1999),(215,50,'','Indian Institute of Management - IIM','',0,'0','',1961),(216,53,'','Indian Institute of Mass Communication - IIMC New Delhi','',0,'0','',1965),(217,51,'','Indian Institute of Science - Bangalore','',0,'0','http://www.iisc.ac.in/',1909),(218,51,'','Indian Institute of Space Science and Technology - IIST','',0,'0','',2007),(219,51,'','Indian Law Institute - ILI','',0,'0','',1956),(220,400,'','Indian Maritime University','',0,'0','',2008),(221,53,'','Indian Statistical Institute - ISI Kolkata','',0,'0','',1931),(222,51,'','Indian Veterinary Research Institute - IVRI','',0,'0','',1983),(223,399,'','Indira Gandhi Agricultural University','',0,'0','',1987),(224,53,'','Indira Gandhi Delhi Technological University for Women - IGITW','',0,'0','',2013),(225,53,'','Indira Gandhi Institute of Development Research - IGIDR','',0,'0','',1987),(226,400,'','Indira Gandhi National Open University - IGNOU','',0,'0','',1985),(227,53,'','Indira Gandhi Technological and Medical Sciences University','',0,'0','',2012),(228,399,'','Indira Kala Sangeet Vishwavidyalaya','',0,'0','',1956),(229,399,'','Indraprastha Institute of Information Technology - IIIT Delhi','',0,'0','',2008),(230,53,'','Indus University','',0,'0','',0),(231,51,'','Institute of Chemical Technology - ICT','',0,'0','',1933),(232,51,'','Institute of Liver and Biliary Sciences - ILBS','',0,'0','',2009),(233,399,'','Integral University','',0,'0','',1998),(234,51,'','International Institute for Population Sciences - IIPS','',0,'0','',1956),(235,53,'','International Open University of Theology - IOUT','',0,'0','',1998),(236,53,'','Invertis University','',0,'0','',1956),(237,399,'','Islamic University of Science and Technology','',0,'0','',2005),(238,53,'','ITM University','',0,'0','',1996),(239,53,'','J.S. University','',0,'0','',0),(240,399,'','Jadavpur University','',0,'0','',1955),(241,399,'','Jagadguru Ramanandacharya Rajasthan Sanskrit University - JRRSU','',0,'0','',2001),(242,399,'','Jagan Nath University','',0,'0','',2008),(243,399,'','Jagdishprasad Jhabarmal Tibrewala University - JJTU','',0,'0','',2009),(244,399,'','Jai Narain Vyas University','',0,'0','',1962),(245,51,'','Jain University - JU','',0,'0','',1990),(246,51,'','Jain Vishva Bharati University','',0,'0','',1991),(247,399,'','Jaiprakash University','',0,'0','',1990),(248,399,'','Jaipur National University','',0,'0','',2007),(249,51,'','Jamia Hamdard University','',0,'0','',1989),(250,399,'','Jawaharlal Nehru Architecture and Fine Arts University - JNAFAU','',0,'0','',2008),(251,51,'','Jawaharlal Nehru Centre for Advanced Scientific Research - JNCASR','',0,'0','',1989),(252,50,'','Jawaharlal Nehru Krishi Vishwavidyalaya - JNKVV','',0,'0','',1964),(253,399,'','Jawaharlal Nehru Technological University','',0,'0','http://jntuh.ac.in/',1972),(254,4,'','Jawaharlal Nehru University - JNU','',0,'0','http://www.jnu.ac.in/',1969),(255,399,'','Jayoti Vidyapeeth Women\'s University','',0,'0','',2008),(256,51,'','Jaypee Institute of Information Technology University','',0,'0','',2001),(257,399,'','Jaypee University of Engineering and Technology - JUET','',0,'0','',2003),(258,399,'','Jaypee University of Information Technology - JUIT','',0,'0','',2002),(259,53,'','JECRC University','',0,'0','',2012),(260,53,'','Jharkhand Rai University - JRU','',0,'0','',2011),(261,53,'','JIS University','',0,'0','',2014),(262,399,'','Jiwaji University','',0,'0','',1964),(263,51,'','JK Lakshmipat University - JKLU','',0,'0','',2011),(264,51,'','Jodhpur National University - JNU','',0,'0','',2008),(265,51,'','JRN Rajasthan Vidyapeeth University','',0,'0','',1937),(266,399,'','Junagadh Agricultural University','',0,'0','',1972),(267,399,'','Kachchh University','',0,'0','',2003),(268,399,'','Kadi Sarva Vishwavidyalaya - KSV','',0,'0','',2007),(269,399,'','Kakatiya University','',0,'0','',1976),(270,51,'','Kalasalingam University','',0,'0','',1984),(271,51,'','Kalinga Institute of Industrial Technology - KIIT University','',0,'0','',1992),(272,53,'','Kaloji Narayana Rao University of Health Sciences','',0,'0','',0),(273,399,'','Kameshwar Singh Darbhanga Sanskrit University','',0,'0','',1961),(274,399,'','Kannada University','',0,'0','',1991),(275,399,'','Kannur University','',0,'0','',1996),(276,399,'','Karnatak University','',0,'0','',1949),(277,399,'','Karnataka State Law University - KSLU','',0,'0','',2009),(278,399,'','Karnataka State Open University - KSOU','',0,'0','',1996),(279,399,'','Karnataka State Women\'s University - KSWU','',0,'0','',2003),(280,399,'','Karnataka Veterinary Animal and Fisheries Sciences University','',0,'0','',2004),(281,51,'','Karpagam University','',0,'0','',2008),(282,51,'','Karunya University','',0,'0','',1986),(283,399,'','Kavikulguru Kalidas Sanskrit University','',0,'0','',1997),(284,53,'','Kaziranga University - KU','',0,'0','',2012),(285,53,'','Kendriya Vishwavidyalaya','',0,'0','',0),(286,399,'','Kerala Agricultural University - KAU','',0,'0','',1971),(287,51,'','Kerala Kalamandalam','',0,'0','',1930),(288,399,'','Kerala University of Fisheries and Ocean Studies - KUFOS','',0,'0','',2010),(289,53,'','Kerala University of Health Sciences - KUHS','',0,'0','',2010),(290,399,'','Kerala Veterinary and Animal Sciences University - KVASU','',0,'0','',2010),(291,399,'','Khwaja Moinuddin Chishti Urdu, Arabi, Farsi University','',0,'0','',2009),(292,399,'','King George\'s Medical University','',0,'0','',1911),(293,51,'','KLE University','',0,'0','',2006),(294,53,'','Kolhan University','',0,'0','',2009),(295,51,'','Koneru Lakshmaiah University - KL University','',0,'0','',2008),(296,53,'','KR Mangalam University - KRMU','',0,'0','',2013),(297,51,'','Krishna Institute of Medical Sciences University','',0,'0','',1984),(298,399,'','Krishna Kanta Handique State Open University - KKHSOU','',0,'0','',1995),(299,399,'','Krishna University','',0,'0','',2008),(300,399,'','KSGH Music and Performing Arts University','',0,'0','',2008),(301,399,'','Kumaun University','',0,'0','',0),(302,399,'','Kurukshetra University','',0,'0','',1957),(303,399,'','Kushabhau Thakre Patrakarita Avam Jansanchar University','',0,'0','',2004),(304,399,'','Kuvempu University','',0,'0','',1987),(305,51,'','Lakshmibai National University of Physical Education','',0,'0','',1957),(306,399,'','Lala Lajpat Rai University of Veterinary and Animal Sciences - LLRUVAS','',0,'0','',2010),(307,399,'','Lalit Narayan Mithila University','',0,'0','',1972),(308,51,'','Lingaya’s University','',0,'0','',1998),(309,53,'','LNCT University','',0,'0','',0),(310,399,'','Lovely Professional University - LPU','',0,'0','',2005),(311,53,'','M.S. Ramaiah University of Applied Sciences - MSRUAS','',0,'0','',2013),(312,53,'','Madan Mohan Malaviya University of Technology - MMMUT','',0,'0','',2013),(313,399,'','Madhya Pradesh Bhoj Open University','',0,'0','',1991),(314,399,'','Madhya Pradesh Medical Science University - MPMSU','',0,'0','',2011),(315,50,'','Madhya Pradesh Pashu Chikitsa Vigyan Vishwavidyalaya','',0,'0','',2009),(316,399,'','Madurai Kamaraj University','',0,'0','',1966),(317,399,'','Magadh University','',0,'0','',1962),(318,399,'','Mahamaya Technical University - MTU','',0,'0','',2010),(319,53,'','Maharaj Vinayak Global University','',0,'0','',2012),(320,399,'','Maharaja Ganga Singh University - MGSU','',0,'0','',2003),(321,399,'','Maharaja Ranjit Singh Punjab Technical University - MRSPTU','',0,'0','',2014),(322,399,'','Maharaja Sayajirao University of Baroda','',0,'0','',1949),(323,399,'','Maharana Pratap University of Agricultural and Technology - MPUAT','',0,'0','',1999),(324,399,'','Maharashtra Animal and Fisheries Sciences University','',0,'0','',2000),(325,399,'','Maharashtra University of Health Sciences','',0,'0','',1998),(326,53,'','Maharishi Arvind University','',0,'0','',2015),(327,399,'','Maharishi Mahesh Yogi Vedic Vishwavidyalaya - MMYVV','',0,'0','',1995),(328,51,'','Maharishi Markandeshwar University','',0,'0','',2007),(329,53,'','Maharishi University of Information Technology - MUIT','',0,'0','',0),(330,53,'','Maharishi University of Management and Technology - Bilaspur Campus','',0,'0','',2002),(331,399,'','Maharshi Dayanand Saraswati University','',0,'0','',1987),(332,399,'','Maharshi Dayanand University - MDU','',0,'0','',1976),(333,399,'','Maharshi Panini Sanskrit Evam Vedic Vishwavidyalaya','',0,'0','',2008),(334,53,'','Mahatma Gandhi University','',0,'0','',0),(335,53,'','Mahatma Jyoti Rao Phoole University','',0,'0','',2009),(336,399,'','Mahatma Phule Agriculture University / Vidyapeeth','',0,'0','',1968),(337,399,'','Makhanlal Chaturvedi National University of Journalism','',0,'0','',1990),(338,51,'','Malaviya National Institute of Technology - MNIT Jaipur','',0,'0','',1963),(339,53,'','Manav Bharti University','',0,'0','',2009),(340,399,'','Mangalayatan University','',0,'0','',2006),(341,399,'','Mangalore University','',0,'0','',1980),(342,51,'','Manipal University','',0,'0','',1953),(343,400,'','Manipur University','',0,'0','',1980),(344,399,'','Manonmaniam Sundaranar University','',0,'0','',1990),(345,399,'','Marathwada Agriculture University','',0,'0','',1972),(346,53,'','Martin Luther Christian University - MLCU','',0,'0','',2006),(347,53,'','Marwadi University - MU','',0,'0','',0),(348,53,'','MATS University','',0,'0','',2006),(349,51,'','Maulana Azad National Institute of Technology','',0,'0','',1960),(350,53,'','Medi-Caps University','',0,'0','',0),(351,50,'','Medvarsity Online Limited','',0,'0','',0),(352,51,'','Meenakshi Academy of Higher Education and Research','',0,'0','',2004),(353,53,'','Mewar University - MU','',0,'0','',2009),(354,53,'','MIT ADT University','',0,'0','',0),(355,400,'','Mizoram University','',0,'0','',2001),(356,399,'','MJP Rohilkhand University','',0,'0','',1975),(357,51,'','Mody University of Science and Technology - MUST','',0,'0','',1989),(358,53,'','Mohammad Ali Jauhar University','',0,'0','',2006),(359,399,'','Mohanlal Sukhadia University','',0,'0','',1962),(360,53,'','Monad University','',0,'0','',2010),(361,399,'','Mother Teresa Women\'s University','',0,'0','',1984),(362,53,'','Motherhood University','',0,'0','',2015),(363,51,'','Motilal Nehru National Institute of Technology - NIT Allahabad','',0,'0','',1961),(364,53,'','MVN University','',0,'0','',2012),(365,399,'','Mysore University','',0,'0','',1916),(366,400,'','Nagaland University','',0,'0','',1994),(367,400,'','Nalanda University','',0,'0','',2010),(368,399,'','Narendra Dev University of Agriculture and Technology - NDUAT','',0,'0','',1975),(369,51,'','Narsee Monjee Institute of Management Studies - NMIMS University','',0,'0','',1981),(370,399,'','National Academy of Legal Studies and Research University of Law - NALSAR Hyderabad','',0,'0','',1998),(371,50,'','National Backward Krushi Vidyapeet Solapur','',0,'0','',0),(372,51,'','National Brain Research Centre - NBRC','',0,'0','',1997),(373,51,'','National Dairy Research Institute - NDRI','',0,'0','',1923),(374,51,'','National Institute of Abiotic Stress Management','',0,'0','',2009),(375,51,'','National Museum Institute of History of Art, Conservation and Museology','',0,'0','',1988),(376,399,'','National University of Advanced Legal Studies - NUALS Ernakulam','',0,'0','',2005),(377,51,'','National University of Educational Planning and Administration - NUEPA','',0,'0','',1962),(378,399,'','National University of Study and Research in Law - NUSRL Ranchi','',0,'0','',2010),(379,51,'','Nava Nalanda Mahavihara','',0,'0','',1951),(380,399,'','Navrachana University','',0,'0','',2009),(381,50,'','Navsari Agricultural University','',0,'0','',2004),(382,51,'','Nehru Gram Bharati University','',0,'0','',1996),(383,399,'','Netaji Subhas Open University','',0,'0','',1997),(384,399,'','NIILM University','',0,'0','',0),(385,51,'','NIIT University','',0,'0','',1981),(386,399,'','Nilamber Pitamber University','',0,'0','',2009),(387,53,'','NIMS University','',0,'0','',2008),(388,399,'','Nirma University','',0,'0','',2003),(389,51,'','Nitte University','',0,'0','',1979),(390,399,'','Nizam\'s Institute of Medical Sciences - NIMS Hyderabad','',0,'0','',1989),(391,53,'','Noida International University - NIU','',0,'0','',2010),(392,51,'','Noorul Islam University','',0,'0','',1989),(393,399,'','North East Frontier Technical University - NEFTU','',0,'0','',2014),(394,400,'','North Eastern Hill University','',0,'0','',1973),(395,51,'','North Eastern Regional Institute of Science and Technology - NERIST','',0,'0','',1986),(396,399,'','North Maharashtra University','',0,'0','',1990),(397,399,'','North Orissa University','',0,'0','',1998),(398,399,'','O.P. Jindal Global University - JGU','',0,'0','',2009),(399,53,'','O.P. Jindal University - OPJU','',0,'0','',2014),(400,53,'','Oriental University','',0,'0','',1956),(401,399,'','Orissa University of Agriculture and Technology - OUAT','',0,'0','',1962),(402,399,'OU','Osmania University','',0,'0','http://www.osmania.ac.in/',1918),(403,399,'','Pacific University','',0,'0','',0),(404,51,'','Padmashree Dr.D.Y. Patil Medical College','',0,'0','',1996),(405,399,'','Palamuru University','',0,'0','',2008),(406,399,'','Pandit Bhagwat Dayal Sharma University of Health Sciences','',0,'0','',2008),(407,50,'','Pandit Deen Dayal Upadhyaya Pashu Chikitsa Vigyan Vishwavidyalaya Evam Go Anusandhan Sansthan','',0,'0','',2001),(408,399,'','Pandit Deendayal Petroleum University - PDPU','',0,'0','',2007),(409,399,'','Pandit Ravishankar Shukla University','',0,'0','',1964),(410,399,'','Pandit Sundarlal Sharma Open University','',0,'0','',2005),(411,399,'','Panjab University - PU','',0,'0','',1882),(412,53,'','Parul Universiy','',0,'0','',0),(413,399,'','Patna University','',0,'0','',1917),(414,53,'','PDM University','',0,'0','',0),(415,53,'','People\'s University','',0,'0','',2007),(416,51,'','Periyar Maniammai University','',0,'0','',1988),(417,399,'','Periyar University','',0,'0','',1997),(418,399,'','PES University','',0,'0','',1972),(419,400,'','Pondicherry University','',0,'0','',1985),(420,51,'','Ponnaiyah Ramajayam Institute of Science and Technology - PRIST University','',0,'0','',1994),(421,53,'','Poornima University - PU','',0,'0','',0),(422,399,'','Potti Sreeramulu Telugu University - PSTU','',0,'0','',1985),(423,53,'','Pragati School of Management','',0,'0','',0),(424,53,'','Pratap University','',0,'0','',1995),(425,51,'','Pravara Institute of Medical Sciences','',0,'0','',2003),(426,399,'','Presidency University - Presidency College','',0,'0','',1817),(427,51,'','PRIST University, East Campus','',0,'0','',0),(428,399,'','Punjab Technical University - PTU','',0,'0','',1997),(429,399,'','Punjabi University','',0,'0','',1961),(430,53,'','R.K. University','',0,'0','',2011),(431,399,'','Rabindra Bharati University','',0,'0','',1962),(432,53,'','Raffles University','',0,'0','',0),(433,53,'','Rai Technology University - RTU','',0,'0','',2013),(434,53,'','Rai University - RU','',0,'0','',2005),(435,53,'','Raja Mansingh Tomar Music and Arts University','',0,'0','',2008),(436,399,'','Rajasthan Technical University - RTU','',0,'0','',2006),(437,51,'','Rajiv Gandhi National Institute of Youth Development','',0,'0','',1993),(438,399,'','Rajiv Gandhi Proudyogiki Vishwavidyalaya - RGPV','',0,'0','',1998),(439,400,'','Rajiv Gandhi University','',0,'0','',1984),(440,399,'','Rajmata Vijayaraje Scindia Krishi Vishwa Vidyalaya','',0,'0','',2008),(441,399,'','Raksha Shakti University','',0,'0','',2009),(442,53,'','Rama University','',0,'0','',0),(443,51,'','Ramakrishna Mission Vivekananda University','',0,'0','',2005),(444,399,'','Ranchi University - RU','',0,'0','',1960),(445,399,'','Rani Channamma University','',0,'0','',2010),(446,399,'','Rani Durgavati University','',0,'0','',1956),(447,399,'','Rashtrasant Tukadoji Maharaj Nagpur University - RTMNU','',0,'0','',1923),(448,51,'','Rashtriya Sanskrit Sansthan','',0,'0','',1970),(449,51,'','Rashtriya Sanskrit Vidyapeeth','',0,'0','',1956),(450,399,'','Ravenshaw University','',0,'0','',2006),(451,399,'','Rayalaseema University','',0,'0','',2008),(452,53,'','REVA University','',0,'0','',0),(453,399,'','RIMT University','',0,'0','',2002),(454,53,'','RKDF University','',0,'0','',2011),(455,53,'','RNB Global University','',0,'0','',2015),(456,53,'','S.N. Bose National Centre for Basic Sciences - SNBNCBS','',0,'0','',1986),(457,53,'','Sai Nath University','',0,'0','',0),(458,51,'','Sam Higginbottom Institute of Agriculture Technology and Sciences - SHIATS','',0,'0','',1910),(459,399,'','Sambalpur University','',0,'0','',1966),(460,399,'','Sampurnanand Sanskrit University','',0,'0','',1791),(461,53,'','Sanchi University of Buddhist - Indic Studies','',0,'0','',2012),(462,53,'','Sangam University','',0,'0','',2012),(463,399,'','Sant Gadge Baba Amravati University','',0,'0','',1983),(464,51,'','Sant Longowal Institute of Engineering and Technology - SLIET','',0,'0','',1989),(465,51,'','Santosh University','',0,'0','',2007),(466,399,'','Sardar Patel University - SPU','',0,'0','',1955),(467,399,'','Sardar Vallabh Bhai Patel University of Agriculture and Technology','',0,'0','',2000),(468,399,'','Sardarkrushinagar Dantiwada Agricultural University','',0,'0','',1972),(469,399,'','Sarguja University','',0,'0','',2008),(470,53,'','Sarvepalli Radhakrishna University - SRK University','',0,'0','',0),(471,51,'','SASTRA University','',0,'0','',1984),(472,399,'','Satavahana University','',0,'0','',2006),(473,51,'','Sathyabama University','',0,'0','',1987),(474,399,'','Saurashtra University','',0,'0','',1967),(475,51,'','Saveetha University','',0,'0','',2005),(476,399,'','Savitribai Phule Pune University','',0,'0','',1948),(477,51,'','School of Planning and Architecture - SPA','',0,'0','',1959),(478,53,'','Seacom Skills University - SSU','',0,'0','',2014),(479,399,'','Senate of Serampore College University','',0,'0','',1827),(480,53,'','SGT University','',0,'0','',2013),(481,53,'','Sharda University - SU','',0,'0','',2009),(482,399,'','Sher-e-Kashmir University of Agricultural Sciences and Technology','',0,'0','',1999),(483,53,'','Shiv Nadar University','',0,'0','',2011),(484,399,'','Shivaji University','',0,'0','',1962),(485,51,'','Shobhit University','',0,'0','',2006),(486,399,'','Shoolini University - SU','',0,'0','',0),(487,399,'','Shree Somnath Sanskrit University - SSSU','',0,'0','',2005),(488,399,'','Shreemati Nathibai Damodar Thackersey Women\'s University - Churchgate','',0,'0','',1916),(489,399,'','Shri Jagannath Sanskrit Vishvavidyalaya','',0,'0','',1981),(490,51,'','Shri Lal Bahadur Shastri Rashtriya Sanskrit Vidyapeetha','',0,'0','',1962),(491,399,'','Shri Mata Vaishno Devi University','',0,'0','',1999),(492,53,'','Shri Venkateshwara University - SVU','',0,'0','',2010),(493,399,'','Shridhar University','',0,'0','',2010),(494,399,'','Sidho Kanho Birsha University','',0,'0','',2010),(495,399,'','Sido Kanhu Murmu University - SKMU','',0,'0','',1992),(496,399,'','Sikkim Manipal University','',0,'0','',1995),(497,400,'','Sikkim University','',0,'0','',2007),(498,51,'','Siksha \'O\' Anusandhan University','',0,'0','',2007),(499,51,'','Singhania University','',0,'0','',2007),(500,399,'','Sir Padampat Singhania University','',0,'0','',2007),(501,399,'','Solapur University','',0,'0','',2004),(502,400,'','South Asian University - SAU','',0,'0','',2010),(503,399,'','Sree Sankaracharya University of Sanskrit - SSUS','',0,'0','',1993),(504,51,'','Sri Balaji Vidyapeeth University','',0,'0','',2001),(505,51,'','Sri Chandrasekharendra Saraswathi Viswa Mahavidyalaya - SCSVM','',0,'0','',1993),(506,399,'','Sri Dev Suman Uttarakhand University - SDSUU','',0,'0','',0),(507,51,'','Sri Devaraj URS University','',0,'0','',0),(508,399,'','Sri Guru Granth Sahib World University','',0,'0','',2008),(509,50,'','Sri Karan Narendra Agriculture University - SKNAU','',0,'0','',2013),(510,399,'','Sri Krishnadevaraya University','',0,'0','',1968),(511,399,'','Sri Padmavati Mahila Visvavidyalayam - Women\'s University','',0,'0','',1983),(512,51,'','Sri Ramachandra University','',0,'0','',1985),(513,399,'','Sri Sai University - SSU','',0,'0','',2010),(514,51,'','Sri Sathya Sai Institute of Higher Learning (Anantapur Campus for Women)','',0,'0','',1981),(515,51,'','Sri Siddhartha Academy of Higher Education','',0,'0','',2008),(516,53,'','Sri Sri University - SSU','',0,'0','',2009),(517,399,'','Sri Venkateswara Institute of Medical Sciences','',0,'0','',2009),(518,399,'','Sri Venkateswara University','',0,'0','',1954),(519,399,'','Sri Venkateswara Vedic University - SVVU','',0,'0','',2006),(520,399,'','Sri Venkateswara Veterinary University','',0,'0','',2005),(521,399,'','Srimanta Sankaradeva University of Health Sciences - SSUHS','',0,'0','',2009),(522,53,'','Srimati Techno Institution - Durgapur','',0,'0','',0),(523,399,'SRM University','SRM University','SRM University is one of the top ranking universities in India with over 38,000 students and more than 2600 faculty across all the campus, offering a wide range of undergraduate, postgraduate and doctoral programs in Engineering, Management, Medicine and Health sciences, and Science and Humanities.',0,'0','http://www.srmuniv.ac.in/',2013),(524,51,'','St. Peter\'s University','',0,'0','',2008),(525,53,'','State University of Performing and Visual Arts','',0,'0','',2014),(526,51,'','Sumandeep Vidyapeeth University','',0,'0','',1981),(527,399,'','SunRise University','',0,'0','',2011),(528,399,'','Suresh Gyan Vihar University','',0,'0','',2000),(529,399,'','Swami Keshwanand Rajasthan Agricultural University','',0,'0','',1987),(530,53,'','Swami Rama Himalayan University','',0,'0','',0),(531,399,'','Swami Ramanand Teerth Marathwada University','',0,'0','',1994),(532,399,'','Swami Vivekanand Subharti University','',0,'0','',2008),(533,53,'','Swami Vivekanand University','',0,'0','',2011),(534,51,'','Swami Vivekananda Yoga Anusandhana Samsthana - SVYASA','',0,'0','',0),(535,399,'','Swarnim Gujarat Sports University - SGSU','',0,'0','',0),(536,51,'','Symbiosis International University - SIU','',0,'0','',2002),(537,53,'','Symbiosis University of Applied Sciences - SUAS','',0,'0','',2016),(538,399,'','Tamil Nadu Agricultural University - TNAU','',0,'0','',1971),(539,399,'','Tamil Nadu Dr. Ambedkar Law University','',0,'0','',1997),(540,399,'','Tamil Nadu Dr. M.G.R. Medical University','',0,'0','',1987),(541,52,'','Tamil Nadu Open University','',0,'0','',2002),(542,399,'','Tamil Nadu Physical Education and Sports University - TNPESU','',0,'0','',2004),(543,399,'','Tamil Nadu Teachers Education University - TNTEU','',0,'0','',2008),(544,399,'','Tamil Nadu Veterinary and Animal Sciences University','',0,'0','',1989),(545,399,'','Tamil University','',0,'0','',1981),(546,53,'','Tantia University','',0,'0','',2013),(547,51,'','Tata Institute of Fundamental Research','',0,'0','',1945),(548,51,'','Tata Institute of Social Sciences - TISS','',0,'0','',1936),(549,53,'','TeamLease Skills University - TLSU','',0,'0','',0),(550,53,'','Techno Global University, Sironj','',0,'0','',2013),(551,53,'','Techno India University','',0,'0','',0),(552,53,'','Teerthanker Mahaveer University','',0,'0','',2008),(553,399,'','Telangana University','',0,'0','',2006),(554,51,'','Teri University','',0,'0','',1998),(555,400,'','Tezpur University','',0,'0','',1994),(556,51,'','Thapar University','',0,'0','',1956),(557,51,'','The Gandhigram Rural Institute','',0,'0','',1956),(558,53,'','The George Telegraph Training Institute - Budge Budge','',0,'0','',0),(559,52,'','The Global Open University','',0,'0','',2006),(560,53,'','The Glocal University','',0,'0','',2012),(561,53,'','The Indian Institute of Financial Planning','',0,'0','',0),(562,53,'','The Institute of Trans Disciplinary Health Sciences and Technology','',0,'0','',2013),(563,51,'','The LNM Institute of Information Technology - LNMIIT','',0,'0','',0),(564,399,'','The National Law Institute University - NLIU Bhopal','',0,'0','',1997),(565,53,'','The Neotia University - TNU','',0,'0','',2015),(566,53,'','The NorthCap University - NCU','',0,'0','',0),(567,53,'','The West Bengal University of Teachers\' Training, Education Planning and Administration - WBUTTEPA','',0,'0','',0),(568,399,'','Thiruvalluvar University','',0,'0','',2002),(569,51,'','Tilak Maharastra Vidyapeeth','',0,'0','',1921),(570,399,'','Tilka Manjhi Bhagalpur University','',0,'0','',1960),(571,400,'','Tripura University','',0,'0','',1987),(572,399,'','Tumkur University','',0,'0','',2004),(573,53,'','Uka Tarsadia University','',0,'0','',2011),(574,53,'','University 18','',0,'0','',0),(575,399,'','University of Agricultural Sciences','',0,'0','',1964),(576,399,'','University of Burdwan','',0,'0','',1960),(577,399,'','University of Calcutta','',0,'0','',1857),(578,399,'','University of Calicut','',0,'0','',1968),(579,400,'Univ. Of Delhi','University of Delhi ','The University of Delhi is the premier university of the country and is known for its high  standards in teaching and research and attracts  eminent scholars to  its faculty. It  was  established in  1922 as a unitary, teaching and residential  university by an Act of the then Central Legislative  Assembly.   The President of India is the Visitor,  the Vice  President is  the Chancellor  and  the  Chief Justice  of the Supreme Court of India is the Pro-Chancellor of the University.',0,'0','http://www.du.ac.in/du/',1922),(580,53,'','University of Engineering and Management - UEM','',0,'0','',2014),(581,399,'','University of Engineering and Management - UEM','',0,'0','',2011),(582,399,'','University of Gour Banga','',0,'0','',2007),(583,50,'','University of Horticultural Sciences - UHS','',0,'0','',1986),(584,400,'','University of Hyderabad','',0,'0','',1974),(585,399,'','University of Jammu','',0,'0','',1969),(586,399,'','University of Kalyani','',0,'0','',1960),(587,399,'','University of Kashmir','',0,'0','',1956),(588,399,'','University of Kerala','',0,'0','',1937),(589,399,'','University of Kota','',0,'0','',2003),(590,399,'','University of Lucknow','',0,'0','',1920),(591,399,'','University of Madras','',0,'0','',1857),(592,399,'','University of Mumbai','',0,'0','',1857),(593,399,'','University of North Bengal','',0,'0','',1962),(594,399,'','University of Patanjali','',0,'0','',2007),(595,399,'','University of Petroleum and Energy Studies - UPES','',0,'0','',2003),(596,399,'','University of Rajasthan','',0,'0','',1947),(597,399,'','University of Science and Technology - USTM','',0,'0','',2008),(598,53,'','University of Technology and Management - UTM','',0,'0','',2010),(599,399,'','Utkal University','',0,'0','',1943),(600,399,'','Uttar Banga Krishi Vishwavidyalaya','',0,'0','',2000),(601,399,'','Uttar Pradesh Rajarshi Tandon Open University - UPRTOU','',0,'0','',1999),(602,399,'','Uttarakhand Ayurved University','',0,'0','',2009),(603,399,'','Uttarakhand Open University','',0,'0','',2005),(604,399,'','Uttarakhand Residential University','',0,'0','',2016),(605,399,'','Uttarakhand Sanskrit University','',0,'0','',2005),(606,399,'','Uttarakhand Technical University','',0,'0','',2005),(607,53,'','Uttaranchal University','',0,'0','',0),(608,399,'','Vardhaman Mahaveer Open University','',0,'0','',1987),(609,51,'','Vedanta University','',0,'0','',2006),(610,399,'','Veer Bahadur Singh Purvanchal University','',0,'0','',1987),(611,399,'','Veer Kunwar Singh University - VKSU','',0,'0','',1994),(612,399,'','Veer Narmad South Gujarat University','',0,'0','',1965),(613,399,'','Veer Surendra Sai University of Technology - University College of Engineering','',0,'0','',1956),(614,51,'','Vel Tech Dr. RR and Dr. SR Technical University - Vel Tech University','',0,'0','',1990),(615,51,'','Vellore Institute of Technology - VIT University','',0,'0','',1984),(616,51,'','VELS University','',0,'0','',1992),(617,399,'','Venkateshwara Open University - VOU','',0,'0','',2012),(618,399,'','Vidyasagar University','',0,'0','',1981),(619,51,'','Vignan University - VU','',0,'0','',1997),(620,399,'','Vijayanagara Sri Krishnadevaraya University - VSKU','',0,'0','',2010),(621,399,'','Vikram University','',0,'0','',1957),(622,399,'','Vikrama Simhapuri University - VSU','',0,'0','',2008),(623,53,'','Vinayaka Missions Sikkim University','',0,'0','',2008),(624,51,'','Vinayaka Missions University - VMU','',0,'0','',2001),(625,399,'','Vinoba Bhave University','',0,'0','',1992),(626,400,'','Visva Bharati University','',0,'0','',1863),(627,51,'','Visvesaraya National Institute of Technology - VNIT Nagpur','',0,'0','',2002),(628,399,'','Visvesvaraya Technological University - VTU','',0,'0','',1998),(629,399,'','West Bengal National University of Juridical Sciences - NUJS Kolkata','',0,'0','',1999),(630,399,'','West Bengal State University','',0,'0','',2008),(631,399,'','West Bengal University of Technology - WBUT','',0,'0','',2000),(632,53,'','William Carey University - WCU','',0,'0','',2007),(633,53,'','Xavier University Bhubaneswar - XUB','',0,'0','',2013),(634,399,'','Yashwantrao Chavan Maharashtra Open University','',0,'0','',1989),(635,51,'','Yenepoya University','',0,'0','',2009),(636,399,'','YMCA University of Science and Technology - YMCA UST','',0,'0','',1969),(637,399,'','Yogi Vemana University','',0,'0','',2006);
/*!40000 ALTER TABLE `UNIVERSITY` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `UNREGISTER_CAMPUS_STUDENT`
--

DROP TABLE IF EXISTS `UNREGISTER_CAMPUS_STUDENT`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `UNREGISTER_CAMPUS_STUDENT` (
  `unreg_student_id` int(15) NOT NULL AUTO_INCREMENT,
  `emp_event_id` int(15) NOT NULL,
  `search_name` varchar(100) NOT NULL,
  `first_name` varchar(50) NOT NULL,
  `middle_name` varchar(50) DEFAULT NULL,
  `last_name` varchar(50) NOT NULL,
  `gender` varchar(15) DEFAULT NULL,
  `date_of_birth` date DEFAULT NULL,
  `highlights` varchar(2000) DEFAULT NULL,
  `create_datetime` datetime NOT NULL,
  `update_datetime` datetime NOT NULL,
  `create_user_id` int(15) NOT NULL,
  `update_user_id` int(15) NOT NULL,
  `email_id` varchar(512) DEFAULT NULL,
  `phone_no` bigint(12) unsigned zerofill DEFAULT NULL,
  `program_name` varchar(50) DEFAULT NULL,
  `skills` varchar(200) DEFAULT NULL,
  `interest` varchar(200) DEFAULT NULL,
  `score` int(3) DEFAULT NULL,
  PRIMARY KEY (`unreg_student_id`),
  KEY `emp_event_id` (`emp_event_id`),
  CONSTRAINT `UNREGISTER_CAMPUS_STUDENT_ibfk_1` FOREIGN KEY (`emp_event_id`) REFERENCES `EMPLOYER_EVENT` (`emp_event_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `UNREGISTER_CAMPUS_STUDENT`
--

LOCK TABLES `UNREGISTER_CAMPUS_STUDENT` WRITE;
/*!40000 ALTER TABLE `UNREGISTER_CAMPUS_STUDENT` DISABLE KEYS */;
/*!40000 ALTER TABLE `UNREGISTER_CAMPUS_STUDENT` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `UNREGISTER_CAMPUS_STUDENT_WORK`
--

DROP TABLE IF EXISTS `UNREGISTER_CAMPUS_STUDENT_WORK`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `UNREGISTER_CAMPUS_STUDENT_WORK` (
  `company_upload_log_id` int(15) NOT NULL,
  `first_name` varchar(50) DEFAULT NULL,
  `middle_name` varchar(50) DEFAULT NULL,
  `last_name` varchar(50) DEFAULT NULL,
  `gender` varchar(20) DEFAULT NULL,
  `date_of_birth` varchar(20) DEFAULT NULL,
  `highlights` varchar(2000) DEFAULT NULL,
  `email_id` varchar(512) DEFAULT NULL,
  `phone_no` varchar(20) DEFAULT NULL,
  `error` varchar(100) DEFAULT NULL,
  `program_name` varchar(50) DEFAULT NULL,
  `skills` varchar(200) DEFAULT NULL,
  `interest` varchar(200) DEFAULT NULL,
  `score` varchar(10) DEFAULT NULL,
  KEY `IDX_UNREGISTER_CAMPUS_STUDENT_WORK` (`company_upload_log_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `UNREGISTER_CAMPUS_STUDENT_WORK`
--

LOCK TABLES `UNREGISTER_CAMPUS_STUDENT_WORK` WRITE;
/*!40000 ALTER TABLE `UNREGISTER_CAMPUS_STUDENT_WORK` DISABLE KEYS */;
/*!40000 ALTER TABLE `UNREGISTER_CAMPUS_STUDENT_WORK` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `USER_LOGIN_ATTRIBUTES`
--

DROP TABLE IF EXISTS `USER_LOGIN_ATTRIBUTES`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `USER_LOGIN_ATTRIBUTES` (
  `user_login_attr_id` int(15) NOT NULL AUTO_INCREMENT,
  `user_id` int(15) NOT NULL,
  `login_type_value_id` int(15) NOT NULL,
  `login_attr_key` varchar(50) NOT NULL,
  `login_attr_value` varchar(50) NOT NULL,
  PRIMARY KEY (`user_login_attr_id`),
  KEY `user_id` (`user_id`),
  KEY `login_type` (`login_type_value_id`),
  CONSTRAINT `USER_LOGIN_ATTRIBUTES_ibfk_2` FOREIGN KEY (`login_type_value_id`) REFERENCES `LOOKUP_VALUE` (`lookup_value_id`),
  CONSTRAINT `USER_LOGIN_ATTRIBUTES_ibfk_3` FOREIGN KEY (`user_id`) REFERENCES `SCORA_USER` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `USER_LOGIN_ATTRIBUTES`
--

LOCK TABLES `USER_LOGIN_ATTRIBUTES` WRITE;
/*!40000 ALTER TABLE `USER_LOGIN_ATTRIBUTES` DISABLE KEYS */;
/*!40000 ALTER TABLE `USER_LOGIN_ATTRIBUTES` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping events for database 'prodscora'
--

--
-- Dumping routines for database 'prodscora'
--

--
-- Final view structure for view `CAMPUS_EVENT_STUDENT_SEARCH_VW`
--

/*!50001 DROP VIEW IF EXISTS `CAMPUS_EVENT_STUDENT_SEARCH_VW`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`%` SQL SECURITY DEFINER */
/*!50001 VIEW `CAMPUS_EVENT_STUDENT_SEARCH_VW` AS select 1 AS `first_name`,1 AS `last_name`,1 AS `name`,1 AS `campus_id`,1 AS `campus_name`,1 AS `program_name`,1 AS `highlights`,1 AS `student_id`,1 AS `off_campus_ind`,1 AS `profile_activation_ind`,1 AS `student_status_value_id`,1 AS `department_id`,1 AS `program_id`,1 AS `program_type_value_id`,1 AS `program_class_value_id`,1 AS `program_cat_value_id`,1 AS `program_major_value_id`,1 AS `cgpa_score`,1 AS `planed_completion_date` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `CAMPUS_PLACEMENT_AGGREGATES_VW`
--

/*!50001 DROP VIEW IF EXISTS `CAMPUS_PLACEMENT_AGGREGATES_VW`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`%` SQL SECURITY DEFINER */
/*!50001 VIEW `CAMPUS_PLACEMENT_AGGREGATES_VW` AS select 1 AS `university_id`,1 AS `campus_id`,1 AS `academic_year`,1 AS `no_of_offers`,1 AS `total_offers`,1 AS `average_salary` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `CAMPUS_SEARCH_VW`
--

/*!50001 DROP VIEW IF EXISTS `CAMPUS_SEARCH_VW`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`%` SQL SECURITY DEFINER */
/*!50001 VIEW `CAMPUS_SEARCH_VW` AS select 1 AS `University_Name`,1 AS `Campus_Name`,1 AS `city_id`,1 AS `state_code`,1 AS `university_id`,1 AS `campus_id`,1 AS `city_name`,1 AS `rank`,1 AS `established_date`,1 AS `region_flag`,1 AS `tier_value_id`,1 AS `tier`,1 AS `search_name`,1 AS `search_short_name`,1 AS `campus_status_value_id`,1 AS `campus_status`,1 AS `average_salary` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `CAMPUS_SRCH_PROG_SKILL_INT_VW`
--

/*!50001 DROP VIEW IF EXISTS `CAMPUS_SRCH_PROG_SKILL_INT_VW`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`%` SQL SECURITY DEFINER */
/*!50001 VIEW `CAMPUS_SRCH_PROG_SKILL_INT_VW` AS select 1 AS `campus_id`,1 AS `program_id`,1 AS `program_name`,1 AS `program_type_value_id`,1 AS `program_class_value_id`,1 AS `program_cat_value_id`,1 AS `program_major_value_id`,1 AS `student_id`,1 AS `skill_type_value_id`,1 AS `interest_type_value_id` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `COMPANY_HIRING_AGGREGATES_VW`
--

/*!50001 DROP VIEW IF EXISTS `COMPANY_HIRING_AGGREGATES_VW`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`%` SQL SECURITY DEFINER */
/*!50001 VIEW `COMPANY_HIRING_AGGREGATES_VW` AS select 1 AS `company_id`,1 AS `calendar_year`,1 AS `avg_salary`,1 AS `total_no_of_offers` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `COMPANY_SEARCH_VW`
--

/*!50001 DROP VIEW IF EXISTS `COMPANY_SEARCH_VW`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`%` SQL SECURITY DEFINER */
/*!50001 VIEW `COMPANY_SEARCH_VW` AS select 1 AS `name`,1 AS `search_name`,1 AS `short_name`,1 AS `search_short_name`,1 AS `company_id`,1 AS `city_id`,1 AS `city_name`,1 AS `state_code`,1 AS `region_flag`,1 AS `company_type_value_id`,1 AS `industry_type_value_id`,1 AS `company_size_value_id`,1 AS `internship_ind`,1 AS `rating`,1 AS `company_status_value_id`,1 AS `company_type`,1 AS `company_size`,1 AS `industry_type`,1 AS `company_status` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `JOB_SRCH_OFF_CAMPUS_STUDENTS_VW`
--

/*!50001 DROP VIEW IF EXISTS `JOB_SRCH_OFF_CAMPUS_STUDENTS_VW`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`%` SQL SECURITY DEFINER */
/*!50001 VIEW `JOB_SRCH_OFF_CAMPUS_STUDENTS_VW` AS select 1 AS `company_id`,1 AS `name`,1 AS `industry_type_value_id`,1 AS `company_size_value_id`,1 AS `company_type_value_id`,1 AS `rating`,1 AS `emp_event_id`,1 AS `search_name`,1 AS `search_short_name`,1 AS `emp_drive_id`,1 AS `job_role_id`,1 AS `job_role_name`,1 AS `skill_set`,1 AS `summary` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `OFF_CAMPUS_STUDENT_SEARCH_VW`
--

/*!50001 DROP VIEW IF EXISTS `OFF_CAMPUS_STUDENT_SEARCH_VW`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`%` SQL SECURITY DEFINER */
/*!50001 VIEW `OFF_CAMPUS_STUDENT_SEARCH_VW` AS select 1 AS `first_name`,1 AS `last_name`,1 AS `expected_salary`,1 AS `university_name`,1 AS `university_id`,1 AS `campus_name`,1 AS `campus_id`,1 AS `program_name`,1 AS `highlights`,1 AS `student_id`,1 AS `program_id`,1 AS `program_type_value_id`,1 AS `program_class_value_id`,1 AS `program_cat_value_id`,1 AS `program_major_value_id`,1 AS `city_name`,1 AS `city_id`,1 AS `state_code`,1 AS `state_name`,1 AS `region`,1 AS `cgpa_score`,1 AS `planed_completion_date`,1 AS `actual_completion_date` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-04-16 18:46:58
