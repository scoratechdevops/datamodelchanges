CREATE OR REPLACE
VIEW `COMPANY_SEARCH_VW` AS
SELECT 
        `COMPANY`.`name` AS `name`,
        `COMPANY`.`search_name` AS `search_name`,
        `COMPANY`.`short_name` AS `short_name`,
        `COMPANY`.`search_short_name` AS `search_short_name`,
        `COMPANY`.`company_id` AS `company_id`,
        `ADDRESS`.`city_name` AS `city_name`,
        `STATE`.`state_code` AS `state_code`,
        `STATE`.`region_flag` AS `region_flag`,
        `COMPANY`.`company_type_value_id` AS `company_type_value_id`,
        `COMPANY`.`industry_type_value_id` AS `industry_type_value_id`,
        `COMPANY`.`company_size_value_id` AS `company_size_value_id`,
        `COMPANY`.`internship_ind` AS `internship_ind`,
        `COMPANY`.`rating` AS `rating`,
        `COMPANY`.`company_status_value_id` AS `company_status_value_id`,
        `L1`.`lookup_value` AS `company_type`,
        `L2`.`lookup_value` AS `company_size`,
        `L3`.`lookup_value` AS `industry_type`,
        `L4`.`lookup_value` AS `company_status`
    FROM       `COMPANY`
        LEFT JOIN `LOOKUP_VALUE` `L1` ON `COMPANY`.`company_type_value_id` = `L1`.`lookup_value_id`
        LEFT JOIN `LOOKUP_VALUE` `L2` ON `COMPANY`.`company_size_value_id` = `L2`.`lookup_value_id`
        LEFT JOIN `LOOKUP_VALUE` `L3` ON `COMPANY`.`industry_type_value_id` = `L3`.`lookup_value_id`
        LEFT JOIN `LOOKUP_VALUE` `L4` ON `COMPANY`.`company_status_value_id` = `L4`.`lookup_value_id`
        LEFT JOIN `COMPANY_ADDRESS` ON (`COMPANY_ADDRESS`.`company_id` = `COMPANY`.`company_id` AND IFNULL(`COMPANY_ADDRESS`.`primary_ind`, 'Y') = 'Y')
        LEFT JOIN `ADDRESS` ON `ADDRESS`.`address_id` = `COMPANY_ADDRESS`.`address_id`
        LEFT JOIN `STATE` ON (`STATE`.`state_code` = `ADDRESS`.`state_code` AND `STATE`.`country_code` = `ADDRESS`.`country_code`)