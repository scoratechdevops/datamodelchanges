CREATE OR REPLACE
VIEW `CAMPUS_SEARCH_VW` AS
  (SELECT 
        `UNIVERSITY`.`name` AS `University_Name`,
        `CAMPUS`.`name` AS `Campus_Name`,
        `STATE`.`state_code` AS `state_code`,
        `CAMPUS`.`university_id` AS `university_id`,
        `CAMPUS`.`campus_id` AS `campus_id`,
        `ADDRESS`.`city_name` AS `city_name`,
        `CAMPUS`.`rank` AS `rank`,
        `CAMPUS`.`established_date` AS `established_date`,
        `STATE`.`region_flag` AS `region_flag`,
        `CAMPUS`.`tier_value_id` AS `tier_value_id`,
        `L2`.`lookup_value` AS `tier`,
        `CAMPUS`.`search_name` AS `search_name`,
        `CAMPUS`.`search_short_name` AS `search_short_name`,
        `CAMPUS`.`campus_status_value_id` AS `campus_status_value_id`,
        `L1`.`lookup_value` AS `campus_status`,
        `CPA`.`average_salary` AS `average_salary`,
        `CAMPUS`.`number_of_students` AS `number_of_students`
    FROM
        `CAMPUS`
        JOIN `LOOKUP_VALUE` `L1` ON `CAMPUS`.`campus_status_value_id` = `L1`.`lookup_value_id`
        LEFT JOIN `LOOKUP_VALUE` `L2` ON `CAMPUS`.`tier_value_id` = `L2`.`lookup_value_id`
        JOIN `UNIVERSITY` ON `CAMPUS`.`university_id` = `UNIVERSITY`.`university_id`
        LEFT JOIN `CAMPUS_ADDRESS` ON (`CAMPUS_ADDRESS`.`campus_id` = `CAMPUS`.`campus_id` AND (IFNULL(`CAMPUS_ADDRESS`.`primary_ind`, 'Y') = 'Y'))
		LEFT JOIN `ADDRESS` ON `ADDRESS`.`address_id` = `CAMPUS_ADDRESS`.`address_id`
		LEFT JOIN `STATE` ON (`STATE`.`state_code` = `ADDRESS`.`state_code` AND `STATE`.`country_code` = `ADDRESS`.`country_code`)
		LEFT JOIN `CAMPUS_PLACEMENT_AGGREGATES_VW` `CPA` ON `CPA`.`campus_id` = `CAMPUS`.`campus_id`
            AND `CPA`.`academic_year` IN (SELECT 
                MAX(`CPAV`.`academic_year`)
            FROM
                `CAMPUS_PLACEMENT_AGGREGATES_VW` `CPAV`
            WHERE
                `CPAV`.`campus_id` = `CAMPUS`.`campus_id`
				))