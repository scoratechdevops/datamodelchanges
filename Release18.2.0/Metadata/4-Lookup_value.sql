UPDATE `LOOKUP_VALUE` SET `lookup_value`='Institute' WHERE `lookup_value_id`='27';

INSERT INTO `LOOKUP_VALUE` (`lookup_value_id`, `lookup_type_id`, `lookup_value`) VALUES ('515', '45', 'Unregistered Employers');
INSERT INTO `LOOKUP_VALUE` (`lookup_value_id`, `lookup_type_id`, `lookup_value`) VALUES ('516', '45', 'Unregistered Institutes');
INSERT INTO `LOOKUP_VALUE` (`lookup_value_id`,`lookup_type_id`,`lookup_value`) VALUES (517,46,'Pending Approval');
INSERT INTO `LOOKUP_VALUE` (`lookup_value_id`,`lookup_type_id`,`lookup_value`) VALUES (518,46,'Accept');
INSERT INTO `LOOKUP_VALUE` (`lookup_value_id`,`lookup_type_id`,`lookup_value`) VALUES (519,46,'Reject');
INSERT INTO `LOOKUP_VALUE` (`lookup_value_id`,`lookup_type_id`,`lookup_value`) VALUES (520,46,'Initiated');
INSERT INTO `LOOKUP_VALUE` (`lookup_value_id`, `lookup_type_id`, `lookup_value`) VALUES ('524', '39', 'Campus Unregistered Students');

INSERT INTO `LOOKUP_VALUE` (`lookup_value_id`, `lookup_type_id`, `lookup_value`) VALUES ('521', '20', 'Accepted');
INSERT INTO `LOOKUP_VALUE` (`lookup_value_id`, `lookup_type_id`, `lookup_value`) VALUES ('522', '20', 'Rejected');
INSERT INTO `LOOKUP_VALUE` (`lookup_value_id`, `lookup_type_id`, `lookup_value`) VALUES ('523', '20', 'Share the Event');

INSERT INTO `LOOKUP_VALUE` (`lookup_value_id`,`lookup_type_id`, `lookup_value`) VALUES ('533','40 ', 'Unregistered Institues');
INSERT INTO `LOOKUP_VALUE` (`lookup_value_id`,`lookup_type_id`, `lookup_value`) VALUES ('534','39', 'Unregistered Company');

UPDATE `LOOKUP_VALUE` SET `lookup_value`='Event Shared' WHERE `lookup_value_id`='523';
UPDATE `LOOKUP_VALUE` SET `lookup_value`='Event Unregistered Students' WHERE `lookup_value_id`='524';

UPDATE `LOOKUP_VALUE` SET `lookup_value`='Unregistered Employers' WHERE `lookup_value_id`='534';
UPDATE `LOOKUP_VALUE` SET `lookup_value`='Unregistered Employers' WHERE `lookup_value_id`='515';

UPDATE `LOOKUP_VALUE` SET `lookup_value`='Unregistered Institutes' WHERE `lookup_value_id`='533';






