CREATE OR REPLACE
    ALGORITHM = UNDEFINED 
    DEFINER = `devTm`@`%` 
    SQL SECURITY DEFINER
VIEW `OFF_CAMPUS_STUDENT_SEARCH_VW` AS
    (SELECT 
        `STUDENT`.`student_id` AS `student_id`,
        `STUDENT`.`first_name` AS `first_name`,
        `STUDENT`.`last_name` AS `last_name`,
        `STUDENT`.`expected_salary` AS `expected_salary`,
        `STUDENT`.`off_campus_ind` AS `off_campus_ind`,
        `STUDENT`.`highlights` AS `highlights`,
        if(isnull(`UNIVERSITY`.`name`),`STUDENT_MISSING_PROGRAM_WORK`.`university_name`,`UNIVERSITY`.`name`) AS `university_name`,
        `UNIVERSITY`.`university_id` AS `university_id`,
        if(isnull(`CAMPUS`.`name`),`STUDENT_MISSING_PROGRAM_WORK`.`campus_name`,`CAMPUS`.`name`)  AS `campus_name`,
        `CAMPUS`.`campus_id` AS `campus_id`,
        if(isnull(`PROGRAM`.`program_name`),`STUDENT_MISSING_PROGRAM_WORK`.`program_name`,`PROGRAM`.`program_name`)  AS `program_name`,
        `PROGRAM`.`program_id` AS `program_id`,
        if(isnull(`PROGRAM`.`program_type_value_id`), `STUDENT_MISSING_PROGRAM_WORK`.`program_type_value_id`,`PROGRAM`.`program_type_value_id`) AS `program_type_value_id`,
        if(isnull(`PROGRAM`.`program_class_value_id`), `STUDENT_MISSING_PROGRAM_WORK`.`program_class_value_id`,`PROGRAM`.`program_class_value_id`) AS `program_class_value_id`,
        if(isnull(`PROGRAM`.`program_cat_value_id`), `STUDENT_MISSING_PROGRAM_WORK`.`program_cat_value_id`,`PROGRAM`.`program_cat_value_id`) AS `program_cat_value_id`,
        if(isnull(`PROGRAM`.`program_major_value_id`), `STUDENT_MISSING_PROGRAM_WORK`.`program_major_value_id`,`PROGRAM`.`program_major_value_id`) AS `program_major_value_id`,
        `ADDRESS`.`city_name` AS `city_name`,
        `ADDRESS`.`country_code` AS `country_code`,
        `ADDRESS`.`state_code` AS `state_code`,
        `STATE`.`state_name` AS `state_name`,
        `ENROLLMENT`.`cgpa_score` AS `cgpa_score`,
        `ENROLLMENT`.`planed_completion_date` AS `planed_completion_date`,
        `ENROLLMENT`.`actual_completion_date` AS `actual_completion_date`,
		`ENROLLMENT`.`enrollment_id` AS `enrollment_id`,
		if(isnull(`PROGRAM`.`program_id`), 'N','Y') AS reg_campus_ind,
		SADI_10.score_grade AS 10th_score,
		SADI_12.score_grade AS 12th_score,
		SA.attach_location resume_location	
	FROM
        ((((((((`STUDENT`
        JOIN `ENROLLMENT` ON ((`ENROLLMENT`.`student_id` = `STUDENT`.`student_id`)))
        LEFT JOIN `PROGRAM` ON ((`PROGRAM`.`program_id` = `ENROLLMENT`.`program_id`)))
        LEFT JOIN `STUDENT_MISSING_PROGRAM_WORK` ON ((`STUDENT_MISSING_PROGRAM_WORK`.`enrollment_id` = `ENROLLMENT`.`enrollment_id`)))
        LEFT JOIN `CAMPUS` ON ((`CAMPUS`.`campus_id` = `ENROLLMENT`.`campus_id`)))
        LEFT JOIN `UNIVERSITY` ON ((`UNIVERSITY`.`university_id` = `CAMPUS`.`university_id`)))
        LEFT JOIN `STUDENT_ADDRESS` ON ((`STUDENT_ADDRESS`.`student_id` = `STUDENT`.`student_id`) AND (`STUDENT_ADDRESS`.`primary_ind` = 'Y')))
        LEFT JOIN `ADDRESS` ON ((`ADDRESS`.`address_id` = `STUDENT_ADDRESS`.`address_id`)))
        LEFT JOIN `STATE` ON ((`STATE`.`state_code` = `ADDRESS`.`state_code`)))
		LEFT JOIN STUDENT_ADDITIONAL_INFO SADI_10 ON (SADI_10.student_id =  ENROLLMENT.student_id AND SADI_10.attach_cat_code_value_id = 359)
		LEFT JOIN STUDENT_ADDITIONAL_INFO SADI_12 ON (SADI_12.student_id =  ENROLLMENT.student_id AND SADI_12.attach_cat_code_value_id = 360)
		LEFT JOIN STUDENT_ATTRIBUTES SA ON (SA.student_id = ENROLLMENT.student_id AND attach_cat_value_id = 41)
    WHERE `STUDENT`.`off_campus_ind` = 'Y')