INSERT INTO `SCORA_SERVICES` (`service_id`,`service_name`,`description`,`method_name`) VALUES (225,'/api/EmployerCampusListDtls/deleteCampuses',NULL,'POST');
INSERT INTO `SCORA_SERVICES` (`service_id`,`service_name`,`description`,`method_name`) VALUES (226,'/api/NotificationDetails/updateAllNotifications',NULL,'PUT');


INSERT INTO `SCORA_SERVICES` (`service_id`,`service_name`,`description`,`method_name`) VALUES (227,'/api/EmployerInstituteRelations/campusRelationWithEmpoyer',NULL,'PUT');
INSERT INTO `SCORA_SERVICES` (`service_id`,`service_name`,`description`,`method_name`) VALUES (228,'/api/CompanyContacts/requestContact',NULL,'PUT');

INSERT INTO  `SCORA_SERVICES` (`service_id`, `service_name`, `method_name`) VALUES ('230', '/api/CampusContacts/requestContact', 'PUT');

INSERT INTO `SCORA_SERVICES` (`service_id`, `service_name`, `method_name`) VALUES ('232', '/api/Assessments/getStudentAssessments', 'GET');

INSERT INTO `SCORA_SERVICES` (`service_id`, `service_name`, `description`, `method_name`) VALUES ('233', '/api/MobileAppVersions/getMobileAppVersion', 'API to check mobile app version', 'GET');

INSERT INTO `SCORA_SERVICES` (`service_id`,`service_name`, `description`, `method_name`) VALUES ('231','/api/UnregisteredEmployerUploads/unregisteredEmployerUpload', 'api for unregistered bulk upload', 'POST');
INSERT INTO `SCORA_SERVICES` (`service_id`,`service_name`, `description`, `method_name`) VALUES ('229','/SystemUploadLogs/getAllSysUploads', 'Api to get the contents from system log table', 'GET');

INSERT INTO `SCORA_SERVICES` (`service_id`, `service_name`, `description`, `method_name`) VALUES ('234', '/api/Assessments/', 'API to delete assessment record', 'DELETE');

INSERT INTO `SCORA_SERVICES` (`service_id`, `service_name`, `method_name`) VALUES ('235', '/api/EmployerEvents/getEmployerContact', 'GET');
INSERT INTO `SCORA_SERVICES` (`service_id`, `service_name`, `method_name`) VALUES ('236', '/api/CampusEvents/getCampusContact', 'GET');

INSERT INTO `SCORA_SERVICES` (`service_id`, `service_name`, `method_name`) VALUES ('237', '/api/ScoraInterests', 'POST');
INSERT INTO `SCORA_SERVICES` (`service_id`, `service_name`, `method_name`) VALUES ('238', '/api/ScoraInterests/getScoraInterest', 'GET');
INSERT INTO `SCORA_SERVICES` (`service_id`, `service_name`, `method_name`) VALUES ('239', '/api/ScoraHobbies', 'POST');
INSERT INTO `SCORA_SERVICES` (`service_id`, `service_name`, `method_name`) VALUES ('240', '/api/ScoraHobbies/getScoraHobbies', 'GET');
INSERT INTO `SCORA_SERVICES` (`service_id`, `service_name`, `method_name`) VALUES ('241', '/api/ScoraSkills', 'POST');
INSERT INTO `SCORA_SERVICES` (`service_id`, `service_name`, `method_name`) VALUES ('242', '/api/ScoraSkills/getAllSkills', 'GET');
INSERT INTO `SCORA_SERVICES` (`service_id`, `service_name`, `method_name`) VALUES ('243', '/api/StudentHobbies/deleteStudentHobby', 'DELETE');
INSERT INTO `SCORA_SERVICES` (`service_id`, `service_name`, `method_name`) VALUES ('244', '/api/StudentHobbies/getStudentHobbies', 'GET');
INSERT INTO `SCORA_SERVICES` (`service_id`, `service_name`, `method_name`) VALUES ('245', '/api/StudentHobbies', 'POST');

INSERT INTO `SCORA_SERVICES` (`service_id`, `service_name`, `method_name`) VALUES ('246', '/api/EmployerDriveCampuses/updateHostForPooled', 'PUT');

INSERT INTO `SCORA_SERVICES` (`service_id`, `service_name`, `method_name`) VALUES ('247', '/api/CampusEvents/unRegCampusCompanyEventUpdate', 'PUT');


INSERT INTO `SCORA_SERVICES` (`service_id`, `service_name`, `method_name`) VALUES ('248', '/api/NotificationDetails/getUnreadNotifications', 'GET');

UPDATE  SCORA_SERVICES SET `service_name`='/api/SystemUploadLogs/getAllSysUploads' WHERE `service_id`='229';




