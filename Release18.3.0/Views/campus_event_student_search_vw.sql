CREATE OR REPLACE
VIEW CAMPUS_EVENT_STUDENT_SEARCH_VW AS
    (SELECT 
		ENROLLMENT.enrollment_id AS enrollment_id,
        STUDENT.first_name AS first_name,
        STUDENT.last_name AS last_name,
        DEPARTMENT.name AS name,
        CAMPUS.campus_id AS campus_id,
        CAMPUS.name AS campus_name,
        PROGRAM.program_name AS program_name,
        STUDENT.highlights AS highlights,
        STUDENT.student_id AS student_id,
        STUDENT.off_campus_ind AS off_campus_ind,
        STUDENT.profilr_activation_ind AS profile_activation_ind,
        STUDENT.student_status_value_id AS student_status_value_id,
        DEPARTMENT.department_id AS department_id,
        PROGRAM.program_id AS program_id,
        PROGRAM.program_type_value_id AS program_type_value_id,
        PROGRAM.program_class_value_id AS program_class_value_id,
        PROGRAM.program_cat_value_id AS program_cat_value_id,
        PROGRAM.program_major_value_id AS program_major_value_id,
        ENROLLMENT.cgpa_score AS cgpa_score,
        ENROLLMENT.planed_completion_date AS planed_completion_date,
		ENROLLMENT.live_backlog_count AS live_backlog_count,
		SADI_10.score_grade AS 10th_score,
		SADI_12.score_grade AS 12th_score,
		SA.attach_location
    FROM ENROLLMENT
        JOIN PROGRAM ON (PROGRAM.program_id = ENROLLMENT.program_id)
        JOIN DEPARTMENT ON (DEPARTMENT.department_id = PROGRAM.department_id)
        JOIN STUDENT ON (STUDENT.student_id = ENROLLMENT.student_id AND STUDENT.off_campus_ind = 'N')
        JOIN CAMPUS ON (CAMPUS.campus_id = ENROLLMENT.campus_id)
		LEFT JOIN STUDENT_ADDITIONAL_INFO SADI_10 ON (SADI_10.student_id =  ENROLLMENT.student_id AND SADI_10.attach_cat_code_value_id = 359)
		LEFT JOIN STUDENT_ADDITIONAL_INFO SADI_12 ON (SADI_12.student_id =  ENROLLMENT.student_id AND SADI_12.attach_cat_code_value_id = 360)
		LEFT JOIN STUDENT_ATTRIBUTES SA ON (SA.student_id = ENROLLMENT.student_id AND attach_cat_value_id = 41)
    WHERE
        (ENROLLMENT.data_verified_ind = 'Y'));